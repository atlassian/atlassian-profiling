package com.atlassian.profiling.metrics.api.context;

import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.annotations.nullability.ReturnValuesAreNonnullByDefault;

/**
 * {@link com.atlassian.profiling.metrics.api.tags.OptionalTag OptionalTag}s can live within a ContextFragment, which
 * after it's closed will mean that any {@link com.atlassian.profiling.metrics.api.tags.OptionalTag OptionalTag}s will
 * no longer be a part of the {@link MetricContext}.
 * <p>
 * @see <a href="https://developer.atlassian.com/server/profiling/">Profiling usage guidance</a>
 * @since 4.6.0
 */
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public interface ContextFragment extends AutoCloseable {
    @Override
    void close();
}
