package com.atlassian.profiling.metrics.api.tags;

import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.annotations.nullability.ReturnValuesAreNonnullByDefault;

/**
 * Creates {@link TagFactory}s
 * <p>
 * @see <a href="https://developer.atlassian.com/server/profiling/">Profiling usage guidance</a>
 * @since 4.6.0
 */
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public interface TagFactoryFactory {
    /**
     * @param keyPrefix The prefix used for tag keys. This should be something unique to the App; like the name
     *                  of the App (e.g <code>team-calendars-</code>) to help metrics users understand where this tag
     *                  comes from, and to avoid collisions with other Apps.
     * @return a {@link TagFactory} that will prefix tags key with the provided prefix.
     * @since 4.6.0
     */
    TagFactory prefixedTagFactory(final String keyPrefix);
}
