package com.atlassian.profiling.metrics.api.tags;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;

import com.atlassian.annotations.nullability.ReturnValuesAreNonnullByDefault;

/**
 * A tag with no guarantee of ending up on the metric. Principally because it may not be enabled to end up in
 * the output.
 * <p>
 * A tag is additional information to explain what is being measured, where the key is unique in explaining what the
 * information is (e.g <code>"http-method"</code>) and the value is the additional information
 * (e.g <code>"GET"</code>).
 * <p>
 * @see <a href="https://developer.atlassian.com/server/profiling/">Profiling usage guidance</a>
 * @since 4.6.0
 */
@Immutable
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public interface OptionalTag {
    String getKey();

    String getValue();
}
