package com.atlassian.profiling.metrics.api.context;

import java.util.Set;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.annotations.nullability.ReturnValuesAreNonnullByDefault;
import com.atlassian.profiling.metrics.api.tags.OptionalTag;

/**
 * @see <a href="https://developer.atlassian.com/server/profiling/">Profiling usage guidance</a>
 * @since 4.6.0
 */
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public interface MetricContext {
    /**
     * Creates a {@link ContextFragment} within which the {@link OptionalTag}s will exist.
     * <p>
     * <strong>Note:</strong> There can only be one tag with the same key, the last one wins.
     * @since 4.6.0
     */
    ContextFragment put(final OptionalTag... tags);

    /**
     * @return all tags that already exist in context.
     * @since 4.6.0
     */
    Set<OptionalTag> getAll();
}
