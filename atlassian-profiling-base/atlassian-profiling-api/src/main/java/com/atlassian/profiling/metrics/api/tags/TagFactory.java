package com.atlassian.profiling.metrics.api.tags;

import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.annotations.nullability.ReturnValuesAreNonnullByDefault;

/**
 * Creates tags to be used with metrics.
 * <p>
 * @see <a href="https://developer.atlassian.com/server/profiling/">Profiling usage guidance</a>
 * @since 4.6.0
 */
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public interface TagFactory {
    /**
     * @param key Should be unique for the 'same information' (tags with the same key are not supported) and descriptive
     *            enough for metrics users to understand what the information is. e.g user
     * @param value Should be the piece of information to convey to the metrics users. e.g username123
     * @since 4.6.0
     */
    OptionalTag createOptionalTag(final String key, final String value);
}
