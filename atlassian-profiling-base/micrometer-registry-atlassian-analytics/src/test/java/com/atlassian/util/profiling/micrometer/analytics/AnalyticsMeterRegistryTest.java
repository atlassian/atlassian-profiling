package com.atlassian.util.profiling.micrometer.analytics;

import java.time.Duration;
import java.util.concurrent.CountDownLatch;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.DistributionSummary;
import io.micrometer.core.instrument.LongTaskTimer;
import io.micrometer.core.instrument.Measurement;
import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.MockClock;
import io.micrometer.core.instrument.Statistic;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.Timer;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.util.profiling.micrometer.analytics.events.AbstractMeterEvent;
import com.atlassian.util.profiling.micrometer.analytics.events.CounterEvent;
import com.atlassian.util.profiling.micrometer.analytics.events.GaugeEvent;
import com.atlassian.util.profiling.micrometer.analytics.events.LongTaskTimerEvent;
import com.atlassian.util.profiling.micrometer.analytics.events.MeterEvent;
import com.atlassian.util.profiling.micrometer.analytics.events.SummaryEvent;
import com.atlassian.util.profiling.micrometer.analytics.events.TimerEvent;

import static java.time.Duration.ofMillis;
import static java.util.Arrays.asList;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.apache.commons.lang3.Functions.run;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.doAnswer;

import static com.atlassian.util.profiling.micrometer.analytics.AnalyticsMeterRegistry.DEFAULT_THREAD_FACTORY;
import static com.atlassian.util.profiling.micrometer.analytics.AnalyticsMeterRegistry.SEND_ANALYTICS_TAG;

@RunWith(MockitoJUnitRunner.class)
public class AnalyticsMeterRegistryTest {
    private AnalyticsRegistryConfig config = new AnalyticsRegistryConfig() {
        @Override
        public String get(String key) {
            return null;
        }

        @Override
        public Duration step() {
            return ofMillis(100);
        }
    };

    @Mock
    private EventPublisher eventPublisher;

    @Captor
    private ArgumentCaptor<AbstractMeterEvent> eventCaptor;

    private MockClock clock = new MockClock();
    private AnalyticsMeterRegistry registry;

    @Before
    public void setup() {
        registry = new AnalyticsMeterRegistry(config, eventPublisher, clock, DEFAULT_THREAD_FACTORY);
    }

    @After
    public void cleanup() {
        registry.close();
    }

    @Test
    public void shouldIgnoreMetersWithNoAnalyticsTag() throws InterruptedException {
        registry.gauge("a", Tags.of("foo", "bar"), 10);
        registry.gauge("b", Tags.of("foo", "bar", SEND_ANALYTICS_TAG, "true"), 10);
        registry.gauge("c", Tags.of("foo", "bar"), 10);

        GaugeEvent e = awaitEvent();
        assertThat(e.getName(), equalTo("b"));
    }

    @Test
    public void testGauge() throws InterruptedException {
        registry.gauge("my.metric", Tags.of("foo", "bar", SEND_ANALYTICS_TAG, "true"), 10);

        GaugeEvent e = awaitEvent();
        assertThat(e.getName(), equalTo("my.metric"));
        assertThat(e.getTags(), hasEntry("foo", "bar"));
        assertThat(e.getValue(), equalTo(10D));
    }

    @Test
    public void testCounter() throws InterruptedException {
        final Counter counter = registry.counter("my.metric", Tags.of("foo", "bar", SEND_ANALYTICS_TAG, "true"));
        counter.increment(5);
        counter.increment(10);

        CounterEvent e = awaitEvent();
        assertThat(e.getName(), equalTo("my.metric"));
        assertThat(e.getTags(), hasEntry("foo", "bar"));
        assertThat(e.getCount(), equalTo(15d));
    }

    @Test
    public void testTimer() throws InterruptedException {
        final Timer timer = registry.timer("my.timer", Tags.of("foo", "bar", SEND_ANALYTICS_TAG, "true"));
        timer.record(ofMillis(10));
        timer.record(ofMillis(20));

        TimerEvent e = awaitEvent();
        assertThat(e.getName(), equalTo("my.timer"));
        assertThat(e.getTags(), hasEntry("foo", "bar"));
        assertThat(e.getCount(), equalTo(2L));
        assertThat(e.getMean(), equalTo(15d));
        assertThat(e.getMax(), equalTo(20d));
    }

    @Test
    public void testSummary() throws InterruptedException {
        final DistributionSummary summary =
                registry.summary("my.metric", Tags.of("foo", "bar", SEND_ANALYTICS_TAG, "true"));
        summary.record(10);
        summary.record(20);

        SummaryEvent e = awaitEvent();
        assertThat(e.getName(), equalTo("my.metric"));
        assertThat(e.getTags(), hasEntry("foo", "bar"));
        assertThat(e.getCount(), equalTo(2L));
        assertThat(e.getTotal(), equalTo(30d));
        assertThat(e.getMax(), equalTo(20d));
        assertThat(e.getMean(), equalTo(15d));
    }

    @Test
    public void testLongTimer() throws InterruptedException {
        CountDownLatch[] latches = {new CountDownLatch(1), new CountDownLatch(1)};

        final LongTaskTimer timer =
                registry.more().longTaskTimer("my.metric", Tags.of("foo", "bar", SEND_ANALYTICS_TAG, "true"));
        new Thread(() -> timer.record(() -> run(() -> {
                    clock.add(15, MILLISECONDS);
                    latches[0].await();
                    latches[1].await();
                })))
                .start();

        LongTaskTimerEvent e1 = awaitEvent();
        latches[0].countDown();
        assertThat(e1.getName(), equalTo("my.metric"));
        assertThat(e1.getTags(), hasEntry("foo", "bar"));
        assertThat(e1.getActiveTasks(), equalTo(1));
        assertThat(e1.getDuration(), equalTo(15d));
        assertThat(e1.getMax(), equalTo(15d));

        LongTaskTimerEvent e2 = awaitEvent();
        latches[1].countDown();
        assertThat(e2.getName(), equalTo("my.metric"));
        assertThat(e2.getTags(), hasEntry("foo", "bar"));
        assertThat(e2.getActiveTasks(), equalTo(1));
        assertThat(e2.getDuration(), equalTo(15d + config.step().toMillis()));
        assertThat(e2.getMax(), equalTo(15d + config.step().toMillis()));
    }

    @Test
    public void testMaxAttributeChangesAsNewMetricsArePublished() throws InterruptedException {
        CountDownLatch[] latches = {new CountDownLatch(1), new CountDownLatch(1)};

        final LongTaskTimer timer = registry.more().longTaskTimer("my.metric", Tags.of(SEND_ANALYTICS_TAG, "true"));
        new Thread(() -> timer.record(() -> run(() -> {
                    clock.add(15, MILLISECONDS);
                    latches[0].await();
                    clock.add(30, MILLISECONDS);
                    latches[1].await();
                })))
                .start();

        LongTaskTimerEvent e1 = awaitEvent();
        latches[0].countDown();
        assertThat(e1.getMax(), equalTo(15d));

        LongTaskTimerEvent e2 = awaitEvent();
        latches[1].countDown();
        assertThat(e2.getMax(), equalTo(45d + config.step().toMillis()));
    }

    @Test
    public void testOtherMeter() {
        Measurement m1 = new Measurement(() -> 10d, Statistic.COUNT);
        Measurement m2 = new Measurement(() -> (double) 8, Statistic.VALUE);
        Meter meter = Meter.builder("my.meter", Meter.Type.OTHER, asList(m1, m2))
                .tag("foo", "bar")
                .tag(SEND_ANALYTICS_TAG, "true")
                .register(registry);

        final MeterEvent e = new MeterEvent(meter);
        assertThat(e.getName(), equalTo("my.meter"));
        assertThat(e.getTags(), hasEntry("foo", "bar"));
        assertThat(e.getCount(), equalTo(10d));
        assertThat(e.getValue(), equalTo(8d));
        assertThat(e.getTotal(), nullValue());
        assertThat(e.getDuration(), nullValue());
        assertThat(e.getActiveTasks(), nullValue());
    }

    private <T> T awaitEvent() throws InterruptedException {
        clock.add(config.step());

        final CountDownLatch latch = new CountDownLatch(1);
        doAnswer(i -> {
                    latch.countDown();
                    return null;
                })
                .when(eventPublisher)
                .publish(eventCaptor.capture());
        latch.await(1, SECONDS); // timeout

        return (T) eventCaptor.getValue();
    }
}
