package com.atlassian.util.profiling.micrometer.analytics.events;

import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import io.micrometer.core.instrument.FunctionTimer;

import static java.util.Objects.requireNonNull;

/**
 * Analytics event for a {@link FunctionTimer} meter
 */
public class FunctionTimerEvent extends AbstractMeterEvent {

    private final double total;
    private final double count;
    private final double mean;

    public FunctionTimerEvent(@Nonnull FunctionTimer timer, @Nonnull TimeUnit unit) {
        super(timer);
        total = timer.totalTime(requireNonNull(unit));
        count = timer.count();
        mean = timer.mean(unit);
    }

    public double getTotal() {
        return total;
    }

    public double getCount() {
        return count;
    }

    public double getMean() {
        return mean;
    }

    @Override
    public String getType() {
        return "functionTimer";
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("total", total)
                .append("count", count)
                .append("mean", mean)
                .toString();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FunctionTimerEvent that = (FunctionTimerEvent) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(total, that.total)
                .append(count, that.count)
                .append(mean, that.mean)
                .isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(total)
                .append(count)
                .append(mean)
                .toHashCode();
    }
}
