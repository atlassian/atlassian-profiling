package com.atlassian.util.profiling.micrometer.analytics;

import io.micrometer.core.instrument.step.StepRegistryConfig;

import com.atlassian.annotations.Internal;

/**
 * Configuration for {@link AnalyticsMeterRegistry}
 *
 * @since 3.5
 */
@Internal
public interface AnalyticsRegistryConfig extends StepRegistryConfig {
    AnalyticsRegistryConfig DEFAULT = System::getProperty;

    @Override
    default String prefix() {
        return "profiling.analytics";
    }

    /**
     * @return Whether counters and timers that have no activity in an interval are still sent to analytics.
     */
    default boolean logInactive() {
        String v = get(prefix() + ".sendInactive");
        return Boolean.parseBoolean(v);
    }
}
