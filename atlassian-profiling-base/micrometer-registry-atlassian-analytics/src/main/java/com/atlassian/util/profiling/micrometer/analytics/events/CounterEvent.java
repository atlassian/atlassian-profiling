package com.atlassian.util.profiling.micrometer.analytics.events;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.FunctionCounter;

/**
 * Analytics event for a {@link Counter} meter
 */
public class CounterEvent extends AbstractMeterEvent {

    private final double count;

    public CounterEvent(@Nonnull Counter counter) {
        super(counter);
        count = counter.count();
    }

    public CounterEvent(FunctionCounter counter) {
        super(counter);
        count = counter.count();
    }

    public double getCount() {
        return count;
    }

    @Override
    public String getType() {
        return "counter";
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("count", count)
                .toString();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CounterEvent that = (CounterEvent) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(count, that.count)
                .isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(count)
                .toHashCode();
    }
}
