package com.atlassian.util.profiling.micrometer.analytics.events;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import io.micrometer.core.instrument.Meter;

/**
 * Analytics event for other types of {@link Meter}
 */
public class MeterEvent extends AbstractMeterEvent {

    private Double activeTasks;
    private Double count;
    private Double duration;
    private Double max;
    private Double total;
    private Double unknown;
    private Double value;

    public MeterEvent(@Nonnull Meter meter) {
        super(meter);

        meter.measure().forEach(m -> {
            switch (m.getStatistic()) {
                case MAX:
                    max = m.getValue();
                    break;
                case TOTAL:
                case TOTAL_TIME:
                    total = m.getValue();
                    break;
                case COUNT:
                    count = m.getValue();
                    break;
                case VALUE:
                    value = m.getValue();
                    break;
                case DURATION:
                    duration = m.getValue();
                    break;
                case ACTIVE_TASKS:
                    activeTasks = m.getValue();
                    break;
                case UNKNOWN:
                default:
                    unknown = m.getValue();
            }
        });
    }

    public Double getActiveTasks() {
        return activeTasks;
    }

    public Double getCount() {
        return count;
    }

    public Double getDuration() {
        return duration;
    }

    public Double getMax() {
        return max;
    }

    public Double getTotal() {
        return total;
    }

    public Double getUnknown() {
        return unknown;
    }

    public Double getValue() {
        return value;
    }

    @Override
    public String getType() {
        return "meter";
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("activeTasks", activeTasks)
                .append("count", count)
                .append("duration", duration)
                .append("max", max)
                .append("total", total)
                .append("unknown", unknown)
                .append("value", value)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MeterEvent that = (MeterEvent) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(activeTasks, that.activeTasks)
                .append(count, that.count)
                .append(duration, that.duration)
                .append(max, that.max)
                .append(total, that.total)
                .append(unknown, that.unknown)
                .append(value, that.value)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(activeTasks)
                .append(count)
                .append(duration)
                .append(max)
                .append(total)
                .append(unknown)
                .append(value)
                .toHashCode();
    }
}
