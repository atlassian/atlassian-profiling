package com.atlassian.util.profiling.micrometer.analytics;

import java.util.Optional;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;

import io.micrometer.core.instrument.Clock;
import io.micrometer.core.instrument.step.StepMeterRegistry;
import io.micrometer.core.instrument.util.NamedThreadFactory;

import com.atlassian.annotations.Internal;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.util.profiling.micrometer.analytics.events.CounterEvent;
import com.atlassian.util.profiling.micrometer.analytics.events.FunctionTimerEvent;
import com.atlassian.util.profiling.micrometer.analytics.events.GaugeEvent;
import com.atlassian.util.profiling.micrometer.analytics.events.LongTaskTimerEvent;
import com.atlassian.util.profiling.micrometer.analytics.events.MeterEvent;
import com.atlassian.util.profiling.micrometer.analytics.events.SummaryEvent;
import com.atlassian.util.profiling.micrometer.analytics.events.TimerEvent;

import static io.micrometer.core.instrument.config.MeterFilter.denyUnless;
import static java.lang.Boolean.parseBoolean;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.of;

/**
 * Micrometer {@link io.micrometer.core.instrument.MeterRegistry} that sends to Atlassian Analytics
 * @since 3.5
 */
@Internal
public class AnalyticsMeterRegistry extends StepMeterRegistry {
    public static final ThreadFactory DEFAULT_THREAD_FACTORY =
            new NamedThreadFactory("atlassian-analytics-metrics-publisher");
    public static final String SEND_ANALYTICS_TAG = "atl-analytics";

    private final EventPublisher eventPublisher;

    public AnalyticsMeterRegistry(@Nonnull AnalyticsRegistryConfig config, @Nonnull EventPublisher eventPublisher) {
        this(config, eventPublisher, Clock.SYSTEM, DEFAULT_THREAD_FACTORY);
    }

    public AnalyticsMeterRegistry(
            @Nonnull AnalyticsRegistryConfig config,
            @Nonnull EventPublisher eventPublisher,
            @Nonnull Clock clock,
            @Nonnull ThreadFactory threadFactory) {
        super(config, clock);
        this.eventPublisher = requireNonNull(eventPublisher);

        start(requireNonNull(threadFactory));
        config().meterFilter(denyUnless(id -> parseBoolean(id.getTag(SEND_ANALYTICS_TAG))));
    }

    @Override
    protected void publish() {
        forEachMeter(m -> {
            final Optional<?> event = m.match(
                    gauge -> of(new GaugeEvent(gauge)),
                    counter -> of(new CounterEvent(counter)).filter(e -> e.getCount() != 0D),
                    timer -> of(new TimerEvent(timer, getBaseTimeUnit())).filter(e -> e.getCount() != 0L),
                    summary -> of(new SummaryEvent(summary)).filter(e -> e.getCount() != 0L),
                    longTaskTimer -> of(new LongTaskTimerEvent(longTaskTimer, getBaseTimeUnit()))
                            .filter(e -> e.getActiveTasks() != 0),
                    timeGauge ->
                            of(new GaugeEvent(timeGauge, getBaseTimeUnit())).filter(e -> e.getValue() != 0),
                    counter -> of(new CounterEvent(counter)).filter(e -> e.getCount() != 0D),
                    functionTimer -> of(new FunctionTimerEvent(functionTimer, getBaseTimeUnit()))
                            .filter(e -> e.getCount() != 0D),
                    other -> of(new MeterEvent(other)));
            event.ifPresent(eventPublisher::publish);
        });
    }

    @Override
    protected TimeUnit getBaseTimeUnit() {
        return TimeUnit.MILLISECONDS;
    }
}
