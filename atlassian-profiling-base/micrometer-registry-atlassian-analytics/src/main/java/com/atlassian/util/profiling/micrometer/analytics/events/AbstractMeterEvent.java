package com.atlassian.util.profiling.micrometer.analytics.events;

import java.util.Map;
import java.util.Objects;
import javax.annotation.Nonnull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import io.micrometer.core.instrument.Meter;

import com.atlassian.analytics.api.annotations.EventName;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toMap;

import static com.atlassian.util.profiling.micrometer.analytics.AnalyticsMeterRegistry.SEND_ANALYTICS_TAG;

/**
 * Base class for Atlassian Analytics events emited by {@link com.atlassian.util.profiling.micrometer.analytics.AnalyticsMeterRegistry}
 */
public abstract class AbstractMeterEvent {

    private final String name;
    private final Map<String, String> tags;

    public AbstractMeterEvent(@Nonnull Meter meter) {
        name = requireNonNull(meter).getId().getName();
        tags = meter.getId().getTags().stream()
                .filter(t -> !Objects.equals(SEND_ANALYTICS_TAG, t.getKey()))
                .collect(toMap(t -> t.getKey(), t -> t.getValue()));
    }

    public String getName() {
        return name;
    }

    @Nonnull
    public Map<String, String> getTags() {
        return tags;
    }

    @Nonnull
    public abstract String getType();

    /*
       NOTE: Don't move @EventName as a class annotation even though we're returning a constant.
       We can't do that because @EventName annotation is not declared with @Inherited.
    */
    @EventName
    @Nonnull
    public String getAnalyticsName() {
        return "profiling.metric";
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", name)
                .append("tags", tags)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AbstractMeterEvent that = (AbstractMeterEvent) o;

        return new EqualsBuilder()
                .append(name, that.name)
                .append(tags, that.tags)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(name).append(tags).toHashCode();
    }
}
