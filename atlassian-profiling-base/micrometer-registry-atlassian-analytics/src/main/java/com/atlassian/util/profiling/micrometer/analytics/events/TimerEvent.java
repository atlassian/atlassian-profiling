package com.atlassian.util.profiling.micrometer.analytics.events;

import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import io.micrometer.core.instrument.Timer;

import static java.util.Objects.requireNonNull;

/**
 * Analytics event for a {@link Timer} meter
 */
public class TimerEvent extends AbstractMeterEvent {

    private final long count;
    private final double max;
    private final double mean;
    private final double total;

    public TimerEvent(@Nonnull Timer timer, @Nonnull TimeUnit unit) {
        super(timer);
        total = timer.totalTime(requireNonNull(unit));
        count = timer.count();
        mean = timer.mean(unit);
        max = timer.max(unit);
    }

    public long getCount() {
        return count;
    }

    public double getMax() {
        return max;
    }

    public double getMean() {
        return mean;
    }

    public double getTotal() {
        return total;
    }

    @Override
    public String getType() {
        return "timer";
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("count", count)
                .append("max", max)
                .append("mean", mean)
                .append("total", total)
                .toString();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TimerEvent that = (TimerEvent) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(count, that.count)
                .append(max, that.max)
                .append(mean, that.mean)
                .append(total, that.total)
                .isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(count)
                .append(max)
                .append(mean)
                .append(total)
                .toHashCode();
    }
}
