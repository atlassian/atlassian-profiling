package com.atlassian.util.profiling.micrometer.analytics.events;

import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import io.micrometer.core.instrument.LongTaskTimer;

import static java.util.Objects.requireNonNull;

/**
 * Analytics event for a {@link LongTaskTimer} meter
 */
public class LongTaskTimerEvent extends AbstractMeterEvent {

    private final int activeTasks;
    private final double duration;
    private final double max;

    public LongTaskTimerEvent(@Nonnull LongTaskTimer timer, @Nonnull TimeUnit unit) {
        super(timer);
        requireNonNull(unit);
        activeTasks = timer.activeTasks();
        duration = timer.duration(unit);
        max = timer.max(unit);
    }

    public int getActiveTasks() {
        return activeTasks;
    }

    public double getDuration() {
        return duration;
    }

    public double getMax() {
        return max;
    }

    @Override
    public String getType() {
        return "longTaskTimer";
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("activeTasks", activeTasks)
                .append("duration", duration)
                .append("max", max)
                .toString();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LongTaskTimerEvent that = (LongTaskTimerEvent) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(activeTasks, that.activeTasks)
                .append(duration, that.duration)
                .append(max, that.max)
                .isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(activeTasks)
                .append(duration)
                .append(max)
                .toHashCode();
    }
}
