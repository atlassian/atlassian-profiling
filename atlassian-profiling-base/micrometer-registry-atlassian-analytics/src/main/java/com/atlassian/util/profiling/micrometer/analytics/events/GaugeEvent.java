package com.atlassian.util.profiling.micrometer.analytics.events;

import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.TimeGauge;

/**
 * Analytics event for a {@link Gauge} meter
 */
public class GaugeEvent extends AbstractMeterEvent {

    private final double value;

    public GaugeEvent(@Nonnull Gauge gauge) {
        super(gauge);
        value = gauge.value();
    }

    public GaugeEvent(TimeGauge gauge, TimeUnit unit) {
        super(gauge);
        value = gauge.value(unit);
    }

    public double getValue() {
        return value;
    }

    @Override
    public String getType() {
        return "gauge";
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("value", value)
                .toString();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GaugeEvent that = (GaugeEvent) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(value, that.value)
                .isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(value)
                .toHashCode();
    }
}
