package com.atlassian.util.profiling;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.profile.GCProfiler;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import com.codahale.metrics.MetricRegistry;

import com.atlassian.util.profiling.dropwizard.DropwizardMetricStrategy;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
// @Threads(4)
public class DropwizardMetricsBenchmark {

    private final Timer preDefinedTimer = Timers.timerWithMetric("This is the predefined timer");

    @Param({"true", "false"})
    public boolean metricsEnabled;

    @Setup(Level.Trial)
    public void configureProfiling() {
        MetricRegistry registry = new MetricRegistry();
        DropwizardMetricStrategy strategy = new DropwizardMetricStrategy(registry);
        StrategiesRegistry.addMetricStrategy(strategy);
        Metrics.getConfiguration().setEnabled(metricsEnabled);
    }

    @Benchmark
    public Ticker levelOneTimer() {
        try (Ticker ignored = Timers.startWithMetric("this is a simple metric")) {
            return ignored;
        }
    }

    @Benchmark
    public Ticker levelOnePredefinedTimer() {
        try (Ticker ignored = preDefinedTimer.start("this is a simple metric")) {
            return ignored;
        }
    }

    @Benchmark
    public void levelTenTimer(Blackhole blackhole) {
        try (Ticker ignored = Timers.startWithMetric("this is a simple metric - level 1")) {
            try (Ticker ignored2 = Timers.startWithMetric("this is a simple metric - level 2")) {
                try (Ticker ignored3 = Timers.startWithMetric("this is a simple metric - level 3")) {
                    try (Ticker ignored4 = Timers.startWithMetric("this is a simple metric - level 4")) {
                        try (Ticker ignored5 = Timers.startWithMetric("this is a simple metric - level 5")) {
                            try (Ticker ignored6 = Timers.startWithMetric("this is a simple metric - level 6")) {
                                try (Ticker ignored7 = Timers.startWithMetric("this is a simple metric - level 7")) {
                                    try (Ticker ignored8 =
                                            Timers.startWithMetric("this is a simple metric - level 8")) {
                                        try (Ticker ignored9 =
                                                Timers.startWithMetric("this is a simple metric - level 9")) {
                                            try (Ticker ignored10 =
                                                    Timers.startWithMetric("this is a simple metric - level 10")) {
                                                blackhole.consume(ignored);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /*
     * To run this benchmark, execute:
     *
     * mvn clean install
     * java -jar target/benchmarks.jar DropwizardMetricsBenchmark -f 1 -prof gc
     */
    public static void main(String[] args) throws RunnerException {
        Timers.getConfiguration().setEnabled(false);

        Options opt = new OptionsBuilder()
                .include(DropwizardMetricsBenchmark.class.getSimpleName())
                .addProfiler(GCProfiler.class)
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}
