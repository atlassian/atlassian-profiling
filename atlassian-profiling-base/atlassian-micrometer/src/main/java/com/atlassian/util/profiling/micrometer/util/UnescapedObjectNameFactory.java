package com.atlassian.util.profiling.micrometer.util;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.codahale.metrics.jmx.ObjectNameFactory;

/**
 * Will not escape any metric names when creating an {@link ObjectName}
 * <p>
 * <strong>Note:</strong> should be used with {@link QualifiedCompatibleHierarchicalNameMapper}
 *
 * @since 3.5.0
 */
public class UnescapedObjectNameFactory implements ObjectNameFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(UnescapedObjectNameFactory.class);

    @Override
    public ObjectName createName(String type, String domain, String name) {
        try {
            return new ObjectName(domain + ":" + name);
        } catch (MalformedObjectNameException e) {
            LOGGER.warn("Unable to register {} {}", type, name, e);
            throw new RuntimeException(e);
        }
    }
}
