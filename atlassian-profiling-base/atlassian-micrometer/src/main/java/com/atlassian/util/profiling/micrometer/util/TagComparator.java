package com.atlassian.util.profiling.micrometer.util;

import java.util.Comparator;
import java.util.List;

import io.micrometer.core.instrument.Tag;

import com.atlassian.annotations.Internal;

import static java.lang.Integer.MAX_VALUE;
import static java.util.Arrays.asList;
import static java.util.Comparator.comparing;

import static com.atlassian.util.profiling.MetricTag.FROM_PLUGIN_KEY_TAG_KEY;
import static com.atlassian.util.profiling.MetricTag.INVOKER_PLUGIN_KEY_TAG_KEY;
import static com.atlassian.util.profiling.MetricTag.SUBCATEGORY;

@Internal
abstract class TagComparator {
    private static final List<String> orderedPrefixes = asList(FROM_PLUGIN_KEY_TAG_KEY, INVOKER_PLUGIN_KEY_TAG_KEY);
    private static final List<String> orderedSuffixes = asList(
            SUBCATEGORY, "statistic" // created by Micrometer, e.g. for longRunningTasks in JMX registry
            );

    private static final Comparator<String> tagKeyComparator =
            comparing(TagComparator::rankByPrefix).thenComparing(TagComparator::rankBySuffix);

    private static int rankByPrefix(String key) {
        return orderedPrefixes.contains(key) ? orderedPrefixes.indexOf(key) : MAX_VALUE;
    }

    private static int rankBySuffix(String key) {
        return orderedSuffixes.indexOf(key);
    }

    /**
     * Tag comparator that provides logical hierarchy to JMX ObjectName so that metrics are grouped more intuitively,
     * e.g. they will be grouped by their pluginKeys first instead of by conditionName.
     */
    static final Comparator<Tag> tagComparator = comparing(Tag::getKey, tagKeyComparator);
}
