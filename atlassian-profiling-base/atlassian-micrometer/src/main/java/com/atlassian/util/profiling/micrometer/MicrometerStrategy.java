package com.atlassian.util.profiling.micrometer;

import java.time.Duration;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import io.micrometer.core.instrument.LongTaskTimer;
import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.Timer;
import io.micrometer.core.instrument.search.MeterNotFoundException;

import com.atlassian.annotations.Internal;
import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.util.profiling.MetricKey;
import com.atlassian.util.profiling.MetricTag.RequiredMetricTag;
import com.atlassian.util.profiling.MetricsFilter;
import com.atlassian.util.profiling.Ticker;
import com.atlassian.util.profiling.strategy.MetricStrategy;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

import static com.atlassian.util.profiling.MetricKey.metricKey;
import static com.atlassian.util.profiling.MetricTag.SUBCATEGORY;

/**
 * Simple {@link MetricStrategy} that delegates to matching {@link Timer timers} from a Micrometer {@link MeterRegistry}.
 */
@ParametersAreNonnullByDefault
@Internal
public class MicrometerStrategy implements MetricStrategy {

    private static final long INITIAL_GAUGE_VALUE = 0L;

    @VisibleForTesting
    final Map<MetricKey, AtomicLong> gauges;

    private final MeterRegistry registry;

    public MicrometerStrategy(MeterRegistry registry) {
        gauges = new ConcurrentHashMap();
        this.registry = requireNonNull(registry, "registry");
        registry.config().onMeterRemoved(this::removeRelatedMetric);
    }

    @Override
    public void cleanupMetrics(MetricsFilter filter) {
        for (Meter meter : registry.getMeters()) {
            final String name = meter.getId().getName();
            if (!filter.accepts(name)) {
                registry.remove(meter);
                meter.close();
            }
        }
    }

    @Override
    public void resetMetric(MetricKey metricKey) {
        for (Meter meter : registry.getMeters()) {

            if (!meter.getId().getName().equals(metricKey.getMetricName())
                    || meter.getId().getTags().size() != metricKey.getTags().size()) {
                continue;
            }

            final Set<Tag> meterTags = new HashSet<>(meter.getId().getTags());
            final List<Tag> tagsToMatch = metricKey.getTags().stream()
                    .map(profilingTag -> Tag.of(profilingTag.getKey(), profilingTag.getValue()))
                    .collect(toList());
            if (meterTags.containsAll(tagsToMatch)) {
                registry.remove(meter);
                meter.close();
            }
        }
    }

    @Nonnull
    @Override
    public Ticker startTimer(String metricName) {
        return startTimer(registry.timer(metricName));
    }

    @Nonnull
    @Override
    public Ticker startTimer(MetricKey metricKey) {
        return startTimer(registry.timer(metricKey.getMetricName(), getTags(metricKey)));
    }

    /**
     * @since 3.5.0
     */
    @Nonnull
    @Override
    public Ticker startLongRunningTimer(String metricName) {
        return startLongRunningTimer(metricName, null);
    }

    /**
     * @since 3.5.0
     */
    @Nonnull
    @Override
    public Ticker startLongRunningTimer(@Nonnull final MetricKey metricKey) {
        requireNonNull(metricKey, "metricKey");
        return startLongRunningTimer(metricKey.getMetricName(), getTags(metricKey));
    }

    private Ticker startTimer(Timer timer) {
        final Timer.Sample sample = Timer.start(registry);
        return () -> sample.stop(timer);
    }

    private Ticker startLongRunningTimer(final String metricName, final Collection<Tag> metricTags) {
        final LongTaskTimer.Sample longTimerSample = registry.more()
                .longTaskTimer(metricName, Tags.of(metricTags).and(SUBCATEGORY, "current"))
                .start();
        final Timer timer = registry.timer(metricName, metricTags);
        final Timer.Sample timerSample = Timer.start(registry);
        return () -> {
            longTimerSample.stop();
            timerSample.stop(timer);
        };
    }

    @Override
    public void incrementCounter(MetricKey metricKey, long deltaValue) {
        registry.counter(metricKey.getMetricName(), getTags(metricKey)).increment(deltaValue);
    }

    @Override
    public void updateHistogram(String metricName, long value) {
        registry.summary(metricName).record(value);
    }

    @Override
    public void updateHistogram(MetricKey metricKey, long value) {
        registry.summary(metricKey.getMetricName(), getTags(metricKey)).record(value);
    }

    @Override
    public void updateTimer(MetricKey metricKey, Duration time) {
        registry.timer(metricKey.getMetricName(), getTags(metricKey)).record(time);
    }

    @Override
    public void updateTimer(String metricName, long time, TimeUnit timeUnit) {
        registry.timer(metricName).record(time, timeUnit);
    }

    @Override
    public void incrementGauge(MetricKey metricKey, long deltaValue) {
        AtomicLong gauge = getAndSyncGauge(metricKey);
        gauge.addAndGet(deltaValue);
    }

    @Override
    public void setGauge(MetricKey metricKey, long currentValue) {
        AtomicLong gauge = getAndSyncGauge(metricKey);
        gauge.set(currentValue);
    }

    private AtomicLong getAndSyncGauge(MetricKey metricKey) {
        AtomicLong gauge = gauges.get(metricKey);

        if (isNull(gauge) || isMissingInRegistry(metricKey)) {
            gauge = registry.gauge(metricKey.getMetricName(), getTags(metricKey), new AtomicLong(INITIAL_GAUGE_VALUE));
            gauges.put(metricKey, gauge);
        }
        return gauge;
    }

    private boolean isMissingInRegistry(MetricKey metricKey) {
        try {
            return registry.get(metricKey.getMetricName()).meters().isEmpty();
        } catch (MeterNotFoundException exception) {
            return true;
        }
    }

    private void removeRelatedMetric(Meter meter) {
        final String name = meter.getId().getName();
        final List<RequiredMetricTag> tags = getRequiredMetricTags(meter);
        gauges.remove(metricKey(name, tags));
    }

    /**
     * Build a list of {@link RequiredMetricTag}s from the given Micrometer {@link Meter}.
     */
    private static List<RequiredMetricTag> getRequiredMetricTags(Meter meter) {
        return meter.getId().getTags().stream()
                .map(tag -> RequiredMetricTag.of(tag.getKey(), tag.getValue()))
                .collect(toList());
    }

    /**
     * Build a list of Micrometer {@link Tag}s from the given {@link MetricKey}.
     */
    private static List<Tag> getTags(MetricKey metricKey) {
        return metricKey.getTags().stream()
                .map(tag -> Tag.of(tag.getKey(), tag.getValue()))
                .collect(toList());
    }
}
