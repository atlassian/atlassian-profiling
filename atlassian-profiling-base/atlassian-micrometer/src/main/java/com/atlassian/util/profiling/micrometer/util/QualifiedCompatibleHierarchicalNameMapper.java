package com.atlassian.util.profiling.micrometer.util;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import javax.management.ObjectName;

import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.config.NamingConvention;
import io.micrometer.core.instrument.util.HierarchicalNameMapper;

import com.atlassian.annotations.VisibleForTesting;

import static java.lang.String.format;
import static java.util.Arrays.stream;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import static com.atlassian.util.profiling.micrometer.util.TagComparator.tagComparator;

/**
 * Compatible with Java qualified names in metric tag keys and values. Will split a metric name into categories on '.'
 * <p>
 * e.g.
 * <pre>{@code
 * try (Ticker ignored = Metrics.metric("db.ao.executeInTransaction").tag("pluginKey", "com.atlassian.audit").timer() {
 *          // measured code here
 *  }
 *  }</pre>
 * would output this ObjectName to JMX:
 * <pre>
 * com.atlassian.refapp:type=metrics,category00=db,category01=ao,name=executeInTransaction,tag.pluginKey=com.atlassian.audit
 * </pre>
 * and appear as follows in JConsole:
 * <pre>{@code
 * + com.atlassian.refapp
 *   + metrics
 *     + db
 *       + ao
 *         + executeInTransaction
 *           + com.atlassian.audit
 * }</pre>
 * <p>
 * <strong>Note:</strong> should be used with {@link UnescapedObjectNameFactory}
 * <p>
 * <strong>Note:</strong> should only be used with {@link io.micrometer.jmx.JmxMeterRegistry}
 *
 * @since 3.5.0
 */
public class QualifiedCompatibleHierarchicalNameMapper implements HierarchicalNameMapper {
    @VisibleForTesting
    static final String NAME_KEY = "name";

    @VisibleForTesting
    static final String TYPE_KEY = "type";

    @VisibleForTesting
    static final String TAG_KEY_PREFIX = "tag.";

    @VisibleForTesting
    static final String METRICS_PROPERTY = "metrics";

    @VisibleForTesting
    static final String KEY_PROPERTY_PAIR_DELIMITER = ",";

    @VisibleForTesting
    static final String NAME_GROUPING_DELIMITER = ".";

    private static final String CATEGORY_BASE_KEY = "category";
    private static final String KEY_PROPERTY_SEPARATOR = "=";

    @VisibleForTesting
    static final int STARTING_COUNT = 0;

    @Nonnull
    @Override
    public String toHierarchicalName(
            @Nonnull final Meter.Id meterId, @Nonnull final NamingConvention namingConvention) {
        requireNonNull(meterId, "meterId");
        requireNonNull(namingConvention, "namingConvention");

        final Map<String, String> properties = new LinkedHashMap<>();
        properties.put(TYPE_KEY, METRICS_PROPERTY);
        putCategoriesAndName(meterId, properties);
        putTags(meterId, namingConvention, properties);

        return buildHierarchicalName(properties);
    }

    private String buildHierarchicalName(Map<String, String> properties) {
        return properties.entrySet().stream()
                .map(e -> buildKeyProperty(e.getKey(), e.getValue()))
                .collect(joining(","));
    }

    /**
     * Characters that will cause {@link ObjectName} to throw a {@link javax.management.MalformedObjectNameException}
     */
    private static final Pattern SPECIAL_CHARACTERS = Pattern.compile("["
            + Stream.of(" ", "\\", "\"", "*", "?", ":", "\n", "=", ",")
                    .map(Pattern::quote)
                    .collect(joining())
            + "]");

    private static String sanitize(String hierarchicalName) {
        return SPECIAL_CHARACTERS.matcher(hierarchicalName).replaceAll("_");
    }

    private static void putCategoriesAndName(Meter.Id id, Map<String, String> properties) {
        final String metricName = id.getConventionName(NamingConvention.identity);
        final List<String> categories =
                stream(metricName.split("\\" + NAME_GROUPING_DELIMITER)).collect(toList());
        final String jmxName = categories.remove(categories.size() - 1);

        int categoriesCount = STARTING_COUNT;
        for (String category : categories) {
            properties.put(buildNumberedCategory(categoriesCount), category);
            categoriesCount++;
        }
        properties.put(NAME_KEY, jmxName);
    }

    private static void putTags(Meter.Id id, NamingConvention namingConvention, Map<String, String> properties) {
        id.getConventionTags(namingConvention).stream()
                .sorted(tagComparator)
                .forEach(tag -> properties.put(TAG_KEY_PREFIX + tag.getKey(), tag.getValue()));
    }

    /**
     * Pads an integer with a zero, so it's at least a two character string.
     * <p>
     * Saves a dependency on apache commons
     *
     * @param integerToPad integer to be padded
     * @return a string with the padding
     */
    private static String twoDigitMinimumLeftPad(int integerToPad) {
        return format("%02d", integerToPad);
    }

    @VisibleForTesting
    static String buildNumberedCategory(final int number) {
        return CATEGORY_BASE_KEY + twoDigitMinimumLeftPad(number);
    }

    @VisibleForTesting
    static String buildKeyProperty(final String key, final String value) {
        return sanitize(key) + KEY_PROPERTY_SEPARATOR + sanitize(value);
    }
}
