package com.atlassian.util.profiling.micrometer.util;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import org.junit.Test;
import io.micrometer.core.instrument.Meter.Id;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.config.NamingConvention;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;

import static java.lang.String.join;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;

import static com.atlassian.util.profiling.micrometer.util.QualifiedCompatibleHierarchicalNameMapper.KEY_PROPERTY_PAIR_DELIMITER;
import static com.atlassian.util.profiling.micrometer.util.QualifiedCompatibleHierarchicalNameMapper.METRICS_PROPERTY;
import static com.atlassian.util.profiling.micrometer.util.QualifiedCompatibleHierarchicalNameMapper.NAME_GROUPING_DELIMITER;
import static com.atlassian.util.profiling.micrometer.util.QualifiedCompatibleHierarchicalNameMapper.NAME_KEY;
import static com.atlassian.util.profiling.micrometer.util.QualifiedCompatibleHierarchicalNameMapper.STARTING_COUNT;
import static com.atlassian.util.profiling.micrometer.util.QualifiedCompatibleHierarchicalNameMapper.TAG_KEY_PREFIX;
import static com.atlassian.util.profiling.micrometer.util.QualifiedCompatibleHierarchicalNameMapper.TYPE_KEY;
import static com.atlassian.util.profiling.micrometer.util.QualifiedCompatibleHierarchicalNameMapper.buildKeyProperty;
import static com.atlassian.util.profiling.micrometer.util.QualifiedCompatibleHierarchicalNameMapper.buildNumberedCategory;

public class QualifiedCompatibleHierarchicalNameMapperTest {
    private final QualifiedCompatibleHierarchicalNameMapper mapper = new QualifiedCompatibleHierarchicalNameMapper();
    private final SimpleMeterRegistry registry = new SimpleMeterRegistry();

    @Test
    public void shouldSplitTags() {
        final String metricName = "servlet";
        final String firstTagKey = "method";
        final String firstTagValue = "GET";

        final String hierarchicalName =
                mapper.toHierarchicalName(id(metricName, firstTagKey, firstTagValue), NamingConvention.camelCase);

        assertThat(hierarchicalName, containsString(buildKeyProperty(firstTagKey, firstTagValue)));
    }

    @Test
    public void shouldSwapOutSpecialCharacters() throws MalformedObjectNameException {
        final String name = mapper.toHierarchicalName(
                id(
                        "servlet",
                        "log files",
                        "C:\\logs\\**\\*.log",
                        "properties",
                        "role=\"admin\"",
                        "question",
                        "Hello, is it me you're looking for?"),
                NamingConvention.camelCase);

        // Verify does not throw MalformedObjectNameException
        ObjectName o = new ObjectName("domain:" + name);

        // Should swap out space, =, *, and :
        assertThat(o.getKeyProperty("tag.log_files"), equalTo("C__logs_____.log"));
        // Should swap out = and quote
        assertThat(o.getKeyProperty("tag.properties"), equalTo("role__admin_"));
        // Should swap out comma, space, and ?
        assertThat(o.getKeyProperty("tag.question"), equalTo("Hello__is_it_me_you're_looking_for_"));
    }

    @Test
    public void shouldSplitMetricNameIntoCategories() {
        final String firstCategory = "db";
        final String baseMetricName = "execute";
        final String metricName = firstCategory + NAME_GROUPING_DELIMITER + baseMetricName;

        final String hierarchicalName = mapper.toHierarchicalName(id(metricName), NamingConvention.camelCase);

        assertThat(
                hierarchicalName,
                containsString(buildKeyProperty(buildNumberedCategory(STARTING_COUNT), firstCategory)));
        assertThat(hierarchicalName, containsString(buildKeyProperty(NAME_KEY, baseMetricName)));
    }

    @Test
    public void shouldNotEndWithJmxKeyPropertyDelimiter() {
        final String hierarchicalName = mapper.toHierarchicalName(id("simpleName"), NamingConvention.camelCase);

        assertThat(hierarchicalName, not(endsWith(KEY_PROPERTY_PAIR_DELIMITER)));
    }

    /**
     * When using the Prometheus JMX exporter with its default configuration, this will consistently keep the metric
     * tags and names as metric labels thus making them easier to query in PromQL. It does not affect how JConsole
     * displays the MBeans in folders (the "metrics" "folder" will continue to be the parent).
     */
    @Test
    public void shouldBePrefixedWithTypeMetrics() {
        final String hierarchicalName =
                mapper.toHierarchicalName(id("metricName", "tagKey", "tagValue"), NamingConvention.camelCase);

        assertThat(hierarchicalName, startsWith(buildKeyProperty(TYPE_KEY, METRICS_PROPERTY)));
    }

    @Test
    public void shouldPrefixTagKeysToPreventCollisions() {
        final String tagKey = "name";

        final String hierarchicalName =
                mapper.toHierarchicalName(id("metricName", tagKey, "value"), NamingConvention.camelCase);

        final String prefixedTagKey = TAG_KEY_PREFIX + tagKey;
        assertThat(hierarchicalName, containsString(prefixedTagKey));
    }

    @Test
    public void testEndToEnd() {
        Id id = id("my-metric")
                .withTags(Tags.of(
                        "a", "apple",
                        "z", "zebra",
                        "fromPluginKey", "com.atlassian.upm",
                        "statistic", "active"));

        final String output = mapper.toHierarchicalName(id, NamingConvention.camelCase);

        final String expected = join(
                ",",
                "type=metrics",
                "name=my-metric",
                "tag.fromPluginKey=com.atlassian.upm",
                "tag.a=apple",
                "tag.z=zebra",
                "tag.statistic=active");

        assertThat(output, equalTo(expected));
    }

    private Id id(final String name, final String... tags) {
        return registry.counter(name, tags).getId();
    }
}
