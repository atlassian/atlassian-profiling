package com.atlassian.util.profiling.micrometer.util;

import org.junit.Test;
import com.codahale.metrics.jmx.ObjectNameFactory;

import static org.junit.Assert.assertEquals;

public class UnescapedObjectNameFactoryTest {
    @Test
    public void createName_shouldNot_escapeTheMetricName() {
        final String domain = "aDomain";
        final String name = "name=aName";

        ObjectNameFactory underTest = new UnescapedObjectNameFactory();

        assertEquals(
                "aDomain:name=aName", underTest.createName("", domain, name).getCanonicalName());
    }
}
