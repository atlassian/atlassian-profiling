package com.atlassian.util.profiling.micrometer.util;

import java.util.List;

import org.junit.Test;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

import static com.atlassian.util.profiling.MetricTag.FROM_PLUGIN_KEY_TAG_KEY;
import static com.atlassian.util.profiling.MetricTag.INVOKER_PLUGIN_KEY_TAG_KEY;
import static com.atlassian.util.profiling.MetricTag.SUBCATEGORY;
import static com.atlassian.util.profiling.micrometer.util.TagComparator.tagComparator;

public class TagComparatorTest {

    @Test
    public void should_force_priority_ordering() {
        Tags chaoticOrdered = Tags.of(
                "entityType",
                "Page",
                "taskName",
                "IndexTask",
                INVOKER_PLUGIN_KEY_TAG_KEY,
                "com.atlassian.upm",
                FROM_PLUGIN_KEY_TAG_KEY,
                "com.acme",
                "statistic",
                "active",
                "templateName",
                "admin.soy",
                SUBCATEGORY,
                "current");

        List<Tag> sorted = chaoticOrdered.stream().sorted(tagComparator).collect(toList());
        assertThat(
                sorted,
                contains(
                        // Prefix tags
                        Tag.of(FROM_PLUGIN_KEY_TAG_KEY, "com.acme"),
                        Tag.of(INVOKER_PLUGIN_KEY_TAG_KEY, "com.atlassian.upm"),

                        // Natural ordering
                        Tag.of("entityType", "Page"),
                        Tag.of("taskName", "IndexTask"),
                        Tag.of("templateName", "admin.soy"),

                        // Suffix tags
                        Tag.of(SUBCATEGORY, "current"),
                        Tag.of("statistic", "active")));
    }
}
