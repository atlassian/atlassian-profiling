package com.atlassian.util.profiling.micrometer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import io.micrometer.core.instrument.DistributionSummary;
import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.search.Search;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;

import com.atlassian.util.profiling.Histogram;
import com.atlassian.util.profiling.LongRunningMetricTimer;
import com.atlassian.util.profiling.MetricKey;
import com.atlassian.util.profiling.MetricTag.RequiredMetricTag;
import com.atlassian.util.profiling.MetricTimer;
import com.atlassian.util.profiling.Metrics;
import com.atlassian.util.profiling.MetricsConfiguration;
import com.atlassian.util.profiling.StrategiesRegistry;
import com.atlassian.util.profiling.Ticker;
import com.atlassian.util.profiling.Timer;
import com.atlassian.util.profiling.Timers;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import static com.atlassian.util.profiling.Metrics.metric;
import static com.atlassian.util.profiling.MetricsFilter.ACCEPT_ALL;
import static com.atlassian.util.profiling.MetricsFilter.deny;

public class MicrometerStrategyTest {

    private static final double EQUIVALENCE_DELTA = 0d;
    private static final long SAMPLE_VALUE = 30L;

    private MetricsConfiguration configuration;
    private MeterRegistry registry;
    private MicrometerStrategy strategy;
    private boolean wasEnabled;

    //              BEGIN adapted from the Dropwizard Strategy tests

    @Before
    public void setup() {
        registry = Mockito.spy(new SimpleMeterRegistry());
        strategy = new MicrometerStrategy(registry);

        configuration = Metrics.getConfiguration();
        wasEnabled = configuration.isEnabled();
        configuration.setEnabled(true);
        StrategiesRegistry.addMetricStrategy(strategy);
    }

    @After
    public void tearDown() {
        StrategiesRegistry.removeMetricStrategy(strategy);
        configuration.setEnabled(wasEnabled);
        Metrics.getConfiguration().setFilter(ACCEPT_ALL);
    }

    @Test
    public void testDisabledMetricWorksWhenEnabled() {
        configuration.setEnabled(false);

        MetricTimer metric = Metrics.timer("my.metric");
        io.micrometer.core.instrument.Timer mmTimer = registry.timer("my.metric");

        assertEquals(0L, mmTimer.count());

        metric.start().close();

        assertEquals(0L, mmTimer.count());

        configuration.setEnabled(true);
        metric.start().close();

        assertEquals(1L, mmTimer.count());
    }

    @Test
    public void testEnabledMetricNoopsWhenDisabled() {
        MetricTimer metric = Metrics.timer("my.metric");
        io.micrometer.core.instrument.Timer mmTimer = registry.timer("my.metric");

        assertEquals(0L, mmTimer.count());

        // verify that the metric is enabled
        metric.start().close();
        assertEquals(1L, mmTimer.count());

        // disable the strategy and verify that interacting with the metric does nothing
        configuration.setEnabled(false);
        metric.start().close();

        assertEquals(1L, mmTimer.count());

        // re-enable and verify that the metric is enabled too
        configuration.setEnabled(true);
        metric.start().close();

        assertEquals(2L, mmTimer.count());
    }

    @Test
    public void testCounter() {
        final String metricName = "counter";
        final long zero = 0;
        final long firstValue = 1;
        final long secondValue = 2;
        final double exactlyEqual = 0;

        metric(metricName).incrementCounter(zero);
        assertEquals("Should accept zero", zero, registry.counter(metricName).count(), exactlyEqual);
        metric(metricName).incrementCounter(firstValue);
        assertEquals(
                "Should start counting from 0",
                firstValue,
                registry.counter(metricName).count(),
                exactlyEqual);
        metric(metricName).incrementCounter(secondValue);
        assertEquals(
                "Should increment the counter",
                firstValue + secondValue,
                registry.counter(metricName).count(),
                exactlyEqual);
    }

    @Test
    public void testHistogram() {
        Histogram histogram = Metrics.histogram("histo");
        DistributionSummary mmDistSummary = registry.summary("histo");

        assertEquals(0, mmDistSummary.count());

        histogram.update(1L);
        assertEquals(1, mmDistSummary.count());

        histogram.update(2L);
        assertEquals(2, mmDistSummary.count());

        histogram.update(3L);
        assertEquals(3, mmDistSummary.count());
    }

    @Test
    public void testMetric() {
        MetricTimer metric = Metrics.timer("test.metric");
        io.micrometer.core.instrument.Timer mmTimer = registry.timer("test.metric");

        assertEquals(0L, mmTimer.count());

        metric.start().close();
        assertEquals(1L, mmTimer.count());

        metric.start().close();
        assertEquals(2L, mmTimer.count());
    }

    @Test
    public void testTimerWithMetric() {
        Timer timer = Timers.timerWithMetric("test.metric");
        io.micrometer.core.instrument.Timer mmTimer = registry.timer("test.metric");

        assertEquals(0L, mmTimer.count());

        timer.start().close();
        assertEquals(1L, mmTimer.count());
    }

    @Test
    public void testTimerWithMetricDifferentMetricName() {
        Timer timer = Timers.timerWithMetric("something something", "test.metric");
        io.micrometer.core.instrument.Timer mmTimer = registry.timer("test.metric");

        assertEquals(0L, mmTimer.count());

        timer.start().close();
        assertEquals(1L, mmTimer.count());
    }

    //              END adapted from the Dropwizard Strategy tests

    @Test
    public void testDeniedMetricNoopsWhenDisabled() {
        String metricName = "my.denied.metric";
        MetricTimer metric = metric(metricName).timer();
        metric.start().close();

        // verify that the metric is enabled
        assertEquals(1L, getTimerCount(metricName));

        // disable the metric and verify that timer meter is removed and interacting with the metric does nothing
        Metrics.getConfiguration().setFilter(deny(metricName));

        metric.start().close();
        assertEquals(0, registry.getMeters().size());
        assertEquals(0L, getTimerCount(metricName));

        // re-enable and verify that the metric is enabled too
        Metrics.getConfiguration().setFilter(ACCEPT_ALL);
        metric.start().close();
        metric.start().close();
        assertEquals(1, registry.getMeters().size());
        assertEquals(2L, getTimerCount(metricName));
    }

    @Test
    public void testGaugeRemoval() {
        final String metricName = "my.metric.gauge";
        final long sampleValue = 20L;
        metric(metricName).setGauge(sampleValue);

        assertThat("Gauge should be present", getRegisteredMeters(), hasItem(metricName));

        Metrics.getConfiguration().setFilter(deny(metricName));

        assertThat("Gauge should be removed", getRegisteredMeters(), not(hasItem(metricName)));

        Metrics.getConfiguration().setFilter(ACCEPT_ALL);

        metric(metricName).setGauge(sampleValue);
        assertThat("Gauge should be present", getRegisteredMeters(), hasItem(metricName));
    }

    @Test
    public void testLongRunningTimer() {
        final String timerName = "test.long.running";
        LongRunningMetricTimer metric = Metrics.longRunningTimer(timerName);

        // There should be nothing in the registry yet (since we haven't started the timer)
        assertEquals(0L, registry.getMeters().size());

        Ticker ticker = metric.start();
        // The underlying long-task timer and regular timer should now both be in the registry
        assertEquals(2L, registry.getMeters().size());
        assertThat(getRegisteredMeters(), hasItems(timerName, timerName));

        ticker.close();
        // The underlying long-task timer and regular timer should remain in the registry
        assertEquals(2L, registry.getMeters().size());
        assertThat(getRegisteredMeters(), hasItems(timerName, timerName));
    }

    @Test
    public void testUpdateGauge() {
        final MetricKey metricKey = MetricKey.metricKey("gauge");
        final long zero = 0;
        final long firstValue = 50;
        final long secondValue = -20;
        final long expectedFinalValue = firstValue + secondValue;
        final double equivalenceDelta = 0d; // Should be exactly the same

        strategy.incrementGauge(metricKey, zero);
        assertEquals("should start from 0", zero, getFirstMeterValue(), equivalenceDelta);

        strategy.incrementGauge(metricKey, firstValue);
        assertEquals("should return updated value", firstValue, getFirstMeterValue(), equivalenceDelta);

        strategy.incrementGauge(metricKey, secondValue);
        assertEquals("should update as a delta value", expectedFinalValue, getFirstMeterValue(), equivalenceDelta);

        assertThat("meter is registered", getRegisteredMeters(), hasItem(metricKey.getMetricName()));
    }

    @Test
    public void testSetGauge() {
        final MetricKey metricKey = MetricKey.metricKey("gauge");
        final long zero = 0;
        final long firstValue = 50;
        final long secondValue = 20;
        final long expectedFinalValue = secondValue;

        strategy.setGauge(metricKey, zero);
        assertEquals("should start from 0", zero, getFirstMeterValue(), EQUIVALENCE_DELTA);

        strategy.setGauge(metricKey, firstValue);
        assertEquals("should return set value", firstValue, getFirstMeterValue(), EQUIVALENCE_DELTA);

        strategy.setGauge(metricKey, secondValue);
        assertEquals("should set as a second value", expectedFinalValue, getFirstMeterValue(), EQUIVALENCE_DELTA);

        assertThat("meter is registered", getRegisteredMeters(), hasItem(metricKey.getMetricName()));
    }

    @Test
    public void testGaugeRemovedFromRegistry() {
        final MetricKey metricKey = MetricKey.metricKey("gauge");
        final MetricKey metricKeyWithTag = MetricKey.metricKey("gauge", RequiredMetricTag.of("key", "value"));
        final Long otherValue = SAMPLE_VALUE * 2;

        strategy.setGauge(metricKey, SAMPLE_VALUE);
        strategy.setGauge(metricKeyWithTag, otherValue);

        assertEquals("Should contain sample value", SAMPLE_VALUE, getSecondMeterValue(), EQUIVALENCE_DELTA);
        assertEquals("Should contain other value", otherValue, getFirstMeterValue(), EQUIVALENCE_DELTA);

        registry.clear();
        verify(registry, times(2)).remove(any(Meter.Id.class));

        strategy.incrementGauge(metricKey, SAMPLE_VALUE);
        strategy.incrementGauge(metricKeyWithTag, otherValue);

        assertEquals(
                "Should contain incremented sample value equals to initial value",
                SAMPLE_VALUE,
                getSecondMeterValue(),
                EQUIVALENCE_DELTA);
        assertEquals(
                "Should contain incremented other value equals to initial value",
                otherValue,
                getFirstMeterValue(),
                EQUIVALENCE_DELTA);

        assertThat(
                "Meter is registered",
                getRegisteredMeters(),
                hasItems(metricKey.getMetricName(), metricKeyWithTag.getMetricName()));
    }

    @Test
    public void testMetricRemovalBasedOnMetricKey() {
        final String defaultMetricName = "gauge";
        final RequiredMetricTag defaultFirstTag = RequiredMetricTag.of("key", "value");
        final RequiredMetricTag defaultSecondTag = RequiredMetricTag.of("another_key", "another_value");
        final MetricKey metricKey = MetricKey.metricKey(defaultMetricName, defaultFirstTag, defaultSecondTag);
        final List<MetricKey> metrics = new ArrayList<>(Arrays.asList(
                metricKey,
                MetricKey.metricKey(defaultMetricName),
                MetricKey.metricKey(defaultMetricName, RequiredMetricTag.of("key", "differentValue"), defaultSecondTag),
                MetricKey.metricKey("diffGauge"),
                MetricKey.metricKey(defaultMetricName, defaultFirstTag),
                MetricKey.metricKey(defaultMetricName, defaultSecondTag),
                MetricKey.metricKey(
                        defaultMetricName,
                        defaultFirstTag,
                        defaultSecondTag,
                        RequiredMetricTag.of("superset_key", "superset_value")),
                MetricKey.metricKey("diffGauge", defaultFirstTag, defaultSecondTag)));

        metrics.forEach(metric -> strategy.setGauge(metric, SAMPLE_VALUE));

        List<MetricKey> registryMetrics = getMetricKeysFromRegistry();

        assertEquals(metrics.size(), registry.getMeters().size());
        assertThat(registryMetrics, Matchers.containsInAnyOrder(metrics.toArray()));

        strategy.resetMetric(metricKey);
        metrics.remove(metricKey);
        registryMetrics = getMetricKeysFromRegistry();

        assertEquals(metrics.size(), registry.getMeters().size());
        assertThat("Registry should not contain removed metric.", registryMetrics, not(hasItem(metricKey)));
        assertThat(registryMetrics, Matchers.containsInAnyOrder(metrics.toArray()));
    }

    private List<MetricKey> getMetricKeysFromRegistry() {
        return registry.getMeters().stream()
                .map(Meter::getId)
                .map(id -> MetricKey.metricKey(
                        id.getName(),
                        id.getTags().stream()
                                .map(tag -> RequiredMetricTag.of(tag.getKey(), tag.getValue()))
                                .collect(Collectors.toList())))
                .collect(Collectors.toList());
    }

    private double getFirstMeterValue() {
        return registry.getMeters().get(0).measure().iterator().next().getValue();
    }

    private double getSecondMeterValue() {
        return registry.getMeters().get(1).measure().iterator().next().getValue();
    }

    private List<String> getRegisteredMeters() {
        return registry.getMeters().stream()
                .map(Meter::getId)
                .map(Meter.Id::getName)
                .collect(Collectors.toList());
    }

    private long getTimerCount(String name) {
        Meter meter = Search.in(registry).name(name).meter();

        if (meter instanceof io.micrometer.core.instrument.Timer) {
            return ((io.micrometer.core.instrument.Timer) meter).count();
        }
        return 0L;
    }
}
