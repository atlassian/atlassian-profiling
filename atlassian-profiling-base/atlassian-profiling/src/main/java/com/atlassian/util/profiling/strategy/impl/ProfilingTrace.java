package com.atlassian.util.profiling.strategy.impl;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.annotations.Internal;
import com.atlassian.util.profiling.ProfilerConfiguration;

import static java.util.Objects.requireNonNull;

/**
 * Represents a profiling trace, which consists of the top-level profiled code block, and all profiled code blocks
 * in the call tree from that top-level block.
 *
 * @since 3.0
 */
@Internal
class ProfilingTrace {

    private final StackProfilerStrategy strategy;

    // the currently active frame
    private ProfilingFrame current;
    // total number of frames in trace
    private int frameCount;

    ProfilingTrace(@Nonnull StackProfilerStrategy strategy) {
        this.strategy = requireNonNull(strategy, "strategy");
    }

    void closeAbnormally() {
        while (current != null) {
            current.closeAbnormally();
        }
    }

    @Nullable
    ProfilingFrame getCurrentFrame() {
        return current;
    }

    int getFrameCount() {
        return frameCount;
    }

    ProfilerConfiguration getConfiguration() {
        return strategy.getConfiguration();
    }

    boolean isClosed() {
        return current == null;
    }

    void onClose(@Nonnull ProfilingFrame frame) {
        if (current != requireNonNull(frame, "frame")) {
            // a frame is being closed that is not the current frame. Check whether frame is one of the current frame's
            // parents. If so, close all frames up to the parent
            ProfilingFrame f = current;
            while (f != null && f != frame) {
                f = f.getParent();
            }
            if (f == null) {
                // frame was not part of the current trace at all, ignore it and just return
                return;
            }
            // found the frame in the current trace, close all frames that might have been skipped
            f = current;
            while (f != null && f != frame) {
                f.closeAbnormally();
                f = f.getParent();
            }
        }

        if (current == frame) {
            current = frame.getParent();
            if (frame.isPruned()) {
                // the frame that was just closed has been merged with its sibling
                frameCount -= frame.size();
            }
            if (current == null) {
                strategy.onClose(this, frame);
            }
        }
    }

    @Nonnull
    ProfilingFrame startFrame(@Nonnull String frameName, boolean profileMemory) {
        ProfilingFrame frame = new ProfilingFrame(this, frameName, profileMemory);
        if (current != null) {
            current.addChild(frame);
        }
        current = frame;
        ++frameCount;
        return frame;
    }
}
