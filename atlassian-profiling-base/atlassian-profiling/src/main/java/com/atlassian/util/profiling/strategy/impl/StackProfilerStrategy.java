package com.atlassian.util.profiling.strategy.impl;

import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.annotations.Internal;
import com.atlassian.util.profiling.ProfilerConfiguration;
import com.atlassian.util.profiling.Ticker;
import com.atlassian.util.profiling.Timers;
import com.atlassian.util.profiling.strategy.ProfilerStrategy;

import static java.util.Objects.requireNonNull;

/**
 * Default profiling strategy, which is included into {@link Timers} by default.
 *
 * @since 3.0
 */
@Internal
public class StackProfilerStrategy implements ProfilerStrategy {

    private static final Logger log = LoggerFactory.getLogger(Timers.class);

    private final ThreadLocal<ProfilingTrace> current;

    private ProfilerConfiguration config;
    private Logger logger;

    public StackProfilerStrategy() {
        current = new ThreadLocal<>();
        logger = log;
    }

    public Logger getLogger() {
        return logger;
    }

    @Override
    public void onRequestEnd() {
        try {
            ProfilingTrace trace = current.get();
            if (trace != null) {
                trace.closeAbnormally();
            }
        } finally {
            current.remove();
        }
    }

    @Override
    public void setConfiguration(@Nonnull ProfilerConfiguration configuration) {
        config = requireNonNull(configuration, "configuration");
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Nonnull
    @Override
    public Ticker start(@Nonnull String name) {
        requireNonNull(name, "name");
        ProfilingTrace trace = current.get();
        if (trace == null || trace.isClosed()) {
            trace = new ProfilingTrace(this);
            current.set(trace);
        } else if (trace.getFrameCount() >= config.getMaxFramesPerTrace()) {
            // skip the frame
            return Ticker.NO_OP;
        }

        return trace.startFrame(sanitizeName(name), config.isMemoryProfilingEnabled());
    }

    ProfilerConfiguration getConfiguration() {
        return config;
    }

    /**
     * Retrieves the {@link Ticker} for an active profiling frame in the trace for the current thread.
     *
     * @param name the name of the ticker
     * @return the {@link Ticker}, or {@code null} if no active frame was found matching the provided name
     * @deprecated in 3.0 for removal in 5.0. This method exists only for backwards compatibility to allow
     *             {@link StackProfilingStrategy} to delegate to the new implementation (this class).
     */
    @Deprecated
    @Nullable
    Ticker getTicker(String name) {
        ProfilingTrace trace = current.get();
        if (trace == null) {
            return null;
        }

        ProfilingFrame frame = trace.getCurrentFrame();
        if (frame == null) {
            return null;
        }

        String sanitedName = sanitizeName(name);
        while (frame != null && !sanitedName.equals(frame.getName())) {
            frame = frame.getParent();
        }

        return frame;
    }

    void onClose(ProfilingTrace trace, ProfilingFrame rootFrame) {
        if (logger.isDebugEnabled()
                && !rootFrame.isPruned()
                && rootFrame.getDurationNanos() >= config.getMinTraceTime(TimeUnit.NANOSECONDS)) {
            StringBuilder builder = new StringBuilder();
            rootFrame.append("", builder);
            if (builder.length() > 0) {
                logger.debug(builder.toString());
            }
        }
        if (current.get() == trace) {
            current.remove();
        }
    }

    private String sanitizeName(String name) {
        int max = config.getMaxFrameNameLength();
        if (name == null || name.length() <= max) {
            return name;
        }
        return name.substring(0, max) + "...";
    }
}
