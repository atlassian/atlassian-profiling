package com.atlassian.util.profiling;

import java.util.Collection;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.annotations.Internal;
import com.atlassian.util.profiling.strategy.MetricStrategy;
import com.atlassian.util.profiling.strategy.ProfilerStrategy;

import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static java.util.Objects.requireNonNull;

/**
 * Factory and utility methods for create {@link Timer} instances
 *
 * <pre>
 *     try (Ticker ignored = Timers.start("my-timer")) {
 *         // monitored code block here
 *     }
 * </pre>
 *
 * @since 3.0.0
 */
@Internal
@ParametersAreNonnullByDefault
public class Timers {
    private static final ProfilerConfiguration CONFIGURATION = new ProfilerConfiguration();
    private static final Logger log = LoggerFactory.getLogger(Timers.class);

    private Timers() {
        throw new UnsupportedOperationException("Timers is an utility class and should not be instantiated");
    }

    /**
     * Combine multiple {@link Timer} objects into one, so they can be started and stopped together.
     * @param timers to combine
     * @return a composite {@link Timer}
     * @since 3.0.0
     */
    @Nonnull
    public static Timer concat(Timer... timers) {
        return new CompositeTimer(timers);
    }

    /**
     * Get the current {@link ProfilerConfiguration}.
     * @return the current configuration.
     * @since 3.0.0
     */
    @Nonnull
    public static ProfilerConfiguration getConfiguration() {
        return CONFIGURATION;
    }

    /**
     * Call this method to signal that the request has completed to trigger all configured
     * {@link ProfilerStrategy profiler strategies} and {@link MetricStrategy metric strategies} to clean up any
     * request or thread scoped resources associated with profiling state, including {@link Ticker#close() closing}
     * any {@link Ticker tickers} that have not yet been closed.
     * @since 3.0.0
     */
    public static void onRequestEnd() {
        for (ProfilerStrategy strategy : StrategiesRegistry.getProfilerStrategies()) {
            try {
                strategy.onRequestEnd();
            } catch (Exception e) {
                log.warn(
                        "Error cleaning up profiler state for {}",
                        strategy.getClass().getName(),
                        e);
            }
        }

        for (MetricStrategy strategy : StrategiesRegistry.getMetricStrategies()) {
            try {
                strategy.onRequestEnd();
            } catch (Exception e) {
                log.warn(
                        "Error cleaning up metrics state for {}",
                        strategy.getClass().getName(),
                        e);
            }
        }
    }

    /**
     * Starts a timer with the given name.
     * @return a {@link Ticker} to stop the created and started timer.
     * @since 3.0.0
     */
    @Nonnull
    public static Ticker start(String name) {
        if (CONFIGURATION.isEnabled()) {
            return timer(name).start();
        }
        return Ticker.NO_OP;
    }

    /**
     * Starts a timer with the given name and metrics.
     * @return a {@link Ticker} to stop the created and started timer.
     * @since 3.0.0
     */
    @Nonnull
    public static Ticker startWithMetric(String timerName) {
        if (CONFIGURATION.isEnabled()) {
            return timerWithMetric(timerName).start();
        }
        return Metrics.startTimer(timerName);
    }

    /**
     * Starts a timer with the given name and metrics with the given metric name.
     * @deprecated since 4.3.0, for removal in 5.0.0, use {@link Timers#timerWithMetric(String, Metrics.Builder)} with
     * the {@link Metrics#metric(String)} builder instead.
     * @return a {@link Ticker} to stop the created and started timer.
     * @since 3.0.0
     */
    @Deprecated
    @Nonnull
    public static Ticker startWithMetric(String timerName, String metricName) {
        if (CONFIGURATION.isEnabled()) {
            return timerWithMetric(timerName, metricName).start();
        }
        return Metrics.startTimer(metricName);
    }

    /**
     * Starts a timer with the given name, and tagged metrics.
     * <p>
     * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
     * @deprecated since 4.3.0, for removal in 5.0.0, use {@link Timers#timerWithMetric(String, Metrics.Builder)} with
     * the {@link Metrics#metric(String)} builder instead.
     * @return a {@link Ticker} to stop the created and started timer.
     * @since 3.4.2
     */
    @Deprecated
    @Nonnull
    public static Ticker startWithMetric(String timerName, MetricTag.RequiredMetricTag... metricTags) {
        if (CONFIGURATION.isEnabled()) {
            return timerWithMetric(timerName, metricTags).start();
        }
        return Metrics.startTimer(timerName, metricTags);
    }

    /**
     * Starts a timer with the given name, and tagged metrics with the given metric name.
     * <p>
     * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
     * @deprecated since 4.3.0, for removal in 5.0.0, use {@link Timers#timerWithMetric(String, Metrics.Builder)} with
     * the {@link Metrics#metric(String)} builder instead.
     * @return a {@link Ticker} to stop the created and started timer.
     * @since 3.4.1
     */
    @Deprecated
    @Nonnull
    public static Ticker startWithMetric(
            String timerName, String metricName, MetricTag.RequiredMetricTag... metricTags) {
        if (CONFIGURATION.isEnabled()) {
            return timerWithMetric(timerName, metricName, metricTags).start();
        }
        return Metrics.startTimer(metricName, metricTags);
    }

    /**
     * Creates a {@link Timer} with the given name.
     * @return the created {@link Timer}.
     * @since 3.0.0
     */
    @Nonnull
    public static Timer timer(String name) {
        return new DefaultTimer(requireNonNull(name, "name"));
    }

    /**
     * Creates a {@link Timer} with the given trace name, and metrics with given metric name.
     * @deprecated since 4.3.0, for removal in 5.0.0, use {@link Timers#timerWithMetric(String, Metrics.Builder)} with
     * the {@link Metrics#metric(String)} builder instead.
     * @return the created {@link Timer}.
     * @since 3.0.0
     */
    @Deprecated
    @Nonnull
    public static Timer timerWithMetric(String traceName, String metricName) {
        return timerWithMetric(traceName, metricName, emptySet());
    }

    /**
     * Creates a {@link Timer} with the given trace name, and tagged metrics with the given metric name.
     * <p>
     * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
     * @deprecated since 4.3.0, for removal in 5.0.0, use {@link Timers#timerWithMetric(String, Metrics.Builder)} with
     * the {@link Metrics#metric(String)} builder instead.
     * @return the created {@link Timer}.
     * @since 3.4.0
     */
    @Deprecated
    @Nonnull
    public static Timer timerWithMetric(String traceName, String metricName, MetricTag.RequiredMetricTag... tags) {
        return timerWithMetric(traceName, metricName, asList(tags));
    }

    /**
     * Creates a {@link Timer} with the given trace name, and tagged metrics with the given metric name.
     * <p>
     * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
     * @deprecated since 4.3.0, for removal in 5.0.0, use {@link Timers#timerWithMetric(String, Metrics.Builder)} with
     * the {@link Metrics#metric(String)} builder instead.
     * @return the created {@link Timer}.
     * @since 3.4.0
     */
    @Deprecated
    @Nonnull
    public static Timer timerWithMetric(
            String traceName, String metricName, Collection<MetricTag.RequiredMetricTag> tags) {
        return timerWithMetric(traceName, Metrics.timer(metricName, tags));
    }

    /**
     * Creates a {@link Timer} with the given trace name, and metrics based on the builder.
     * <p>
     * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
     * @return the created {@link Timer}.
     * @since 3.5.0
     */
    @Nonnull
    public static Timer timerWithMetric(String traceName, Metrics.Builder metricBuilder) {
        return timerWithMetric(traceName, metricBuilder.timer());
    }

    /**
     * Creates a {@link Timer} with the given trace name, and mixes in a {@link MetricTimer}.
     * @return the created {@link Timer}.
     * @since 3.5.0
     */
    @Nonnull
    public static Timer timerWithMetric(String traceName, MetricTimer metricTimer) {
        Timer timer = Timers.timer(traceName);

        return new Timer() {
            @Nonnull
            @Override
            public Ticker start(@Nullable Object... callParameters) {
                Ticker traceTicker = timer.start(callParameters);
                Ticker metricTicker = metricTimer.start();
                return Tickers.of(traceTicker, metricTicker);
            }
        };
    }

    /**
     * Creates a {@link Timer} with the given name, and metrics.
     * @return a {@link Ticker} to stop the created timer.
     * @since 3.0.0
     */
    @Nonnull
    public static Timer timerWithMetric(String name) {
        return timerWithMetric(name, name);
    }

    /**
     * Creates a {@link Timer} with the given name, and tagged metrics.
     * <p>
     * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
     * @deprecated since 4.3.0, for removal in 5.0.0, use {@link Timers#timerWithMetric(String, Metrics.Builder)} with
     * the {@link Metrics#metric(String)} builder instead.
     * @return the created {@link Timer}.
     * @since 3.4.0
     */
    @Deprecated
    @Nonnull
    public static Timer timerWithMetric(String name, Collection<MetricTag.RequiredMetricTag> tags) {
        return timerWithMetric(name, name, tags);
    }

    /**
     * Creates a {@link Timer} with the given name, and tagged metrics.
     * <p>
     * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
     * @deprecated since 4.3.0, for removal in 5.0.0, use {@link Timers#timerWithMetric(String, Metrics.Builder)} with
     * the {@link Metrics#metric(String)} builder instead.
     * @return the created {@link Timer}.
     * @since 3.4.0
     */
    @Deprecated
    @Nonnull
    public static Timer timerWithMetric(String name, MetricTag.RequiredMetricTag... tags) {
        return timerWithMetric(name, name, asList(tags));
    }

    /**
     * @since 3.0.0
     */
    @ParametersAreNonnullByDefault
    private static class CompositeTimer implements Timer {

        private final Timer[] timers;

        private CompositeTimer(Timer[] timers) {
            this.timers = requireNonNull(timers, "timers");
        }

        @Nonnull
        @Override
        public Ticker start(@Nullable Object... callParameters) {
            CompositeTicker ticker = null;
            for (Timer timer : timers) {
                if (timer != null) {
                    ticker = Tickers.addTicker(timer.start(callParameters), ticker);
                }
            }
            return ticker == null ? Ticker.NO_OP : ticker;
        }
    }

    /**
     * @since 3.0.0
     */
    private static class DefaultTimer implements Timer {
        private static final Logger log = LoggerFactory.getLogger(DefaultTimer.class);

        private final String name;

        DefaultTimer(String name) {
            this.name = name;
        }

        @Nonnull
        @Override
        public Ticker start(@Nullable Object... callParameters) {
            if (!CONFIGURATION.isEnabled()) {
                return Ticker.NO_OP;
            }

            String frameName;
            if (callParameters == null || callParameters.length == 0) {
                frameName = name;
            } else {
                StringBuilder builder = new StringBuilder(name);

                boolean added = false;
                for (Object p : callParameters) {
                    String param = p == null ? null : String.valueOf(p);
                    if (!(param == null || param.isEmpty())) {
                        if (added) {
                            builder.append(", ");
                        } else {
                            builder.append("(");
                            added = true;
                        }
                        builder.append(param);
                    }
                }
                if (added) {
                    builder.append(")");
                }

                frameName = builder.toString();
            }

            CompositeTicker compositeTicker = null;
            for (ProfilerStrategy strategy : StrategiesRegistry.getProfilerStrategies()) {
                try {
                    compositeTicker = Tickers.addTicker(strategy.start(frameName), compositeTicker);
                } catch (Exception e) {
                    log.warn("Failed to start profiling frame for {}", frameName, e);
                }
            }
            return compositeTicker == null ? Ticker.NO_OP : compositeTicker;
        }
    }
}
