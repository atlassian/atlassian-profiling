package com.atlassian.util.profiling;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;

import com.atlassian.annotations.Internal;

import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;

/**
 * A data class representing the identifier of a single metric.
 *
 * @since 3.4
 */
@ParametersAreNonnullByDefault
@Immutable
@Internal
public final class MetricKey {

    private final String metricName;
    private final Set<MetricTag.RequiredMetricTag> tags;

    private MetricKey(String metricName, Set<MetricTag.RequiredMetricTag> tags) {
        this.metricName = requireNonNull(metricName);
        this.tags = tags;
    }

    @Nonnull
    public String getMetricName() {
        return metricName;
    }

    @Nonnull
    public Collection<MetricTag.RequiredMetricTag> getTags() {
        return tags;
    }

    @Override
    public String toString() {
        return tags.isEmpty() ? metricName : metricName + getFormattedTags();
    }

    private String getFormattedTags() {
        return tags.stream().map(MetricTag::toString).collect(joining(",", "[", "]"));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MetricKey)) return false;
        MetricKey metricKey = (MetricKey) o;
        return metricName.equals(metricKey.metricName) && tags.equals(metricKey.tags);
    }

    @Override
    public int hashCode() {
        return Objects.hash(metricName, tags);
    }

    public static MetricKey metricKey(String metricName) {
        return metricKey(metricName, emptySet());
    }

    public static MetricKey metricKey(String metricName, Collection<MetricTag.RequiredMetricTag> tags) {
        return new MetricKey(
                metricName, new HashSet<>(tags) // we’re doing this so the equality check is based on the values
                );
    }

    public static MetricKey metricKey(String metricName, MetricTag.RequiredMetricTag... tags) {
        return metricKey(metricName, asList(tags));
    }
}
