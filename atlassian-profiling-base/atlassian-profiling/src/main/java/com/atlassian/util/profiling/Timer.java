package com.atlassian.util.profiling;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.annotations.Internal;

/**
 * Object that is used to add profiling around blocks of code
 *
 * <pre>
 *     Timer timer = Timers.timer("my-timer");
 *     try (Ticker ignored = timer.start("foo", "bar")) {
 *         // profiled code block
 *     }
 * </pre>
 *
 * @since 3.0
 */
@Internal
public interface Timer {

    /**
     * Starts a {@link Ticker ticker} for a profiled code block. The optional {@code callParameters} are
     * appended to the name that was provided when the timer was {@link Timers#timer(String) created}.
     * <p>
     * Call parameters are useful if you want to use a pre-defined timer, but include call parameters in the profiling
     * trace.
     *
     * @param callParameters the call parameters
     * @return the ticker
     */
    @Nonnull
    default Ticker start(@Nullable String... callParameters) {
        return start((Object[]) callParameters);
    }

    /**
     * Starts a {@link Ticker ticker} for a profiled code block. The optional {@code callParameters} are
     * appended to the name that was provided when the timer was {@link Timers#timer(String) created}.
     * <p>
     * Call parameters are useful if you want to use a pre-defined timer, but include call parameters in the profiling
     * trace.
     *
     * @param callParameters the call parameters
     * @return the ticker
     *
     * @since 3.1
     */
    @Nonnull
    Ticker start(@Nullable Object... callParameters);
}
