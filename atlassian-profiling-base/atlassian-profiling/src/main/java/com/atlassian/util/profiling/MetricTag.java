package com.atlassian.util.profiling;

import java.util.Objects;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;

import com.atlassian.annotations.Internal;
import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.profiling.metrics.api.tags.OptionalTag;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;

/**
 * A data class representing a single metric tag, as a key-value pair.
 *
 * @since 3.4
 */
@Internal
@Immutable
@ParametersAreNonnullByDefault
public abstract class MetricTag {
    public static final RequiredMetricTag SEND_ANALYTICS = new RequiredMetricTag("atl-analytics", "true");

    public static final String FROM_PLUGIN_KEY_TAG_KEY = "fromPluginKey";
    public static final String INVOKER_PLUGIN_KEY_TAG_KEY = "invokerPluginKey";
    public static final String SUBCATEGORY = "subCategory";

    @VisibleForTesting
    public static final String UNDEFINED_TAG_VALUE = "undefined";

    private final String key;
    private final String value;

    private MetricTag(String key, @Nullable String value) {
        this.key = requireNonNull(key);
        this.value = isNull(value) ? UNDEFINED_TAG_VALUE : value;
    }

    @Nonnull
    public String getKey() {
        return key;
    }

    @Nonnull
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return key + "=" + value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MetricTag)) return false;
        MetricTag metricTag = (MetricTag) o;
        return this.getClass() == o.getClass() && key.equals(metricTag.key) && value.equals(metricTag.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }

    /**
     * nullable value since 3.6.1
     * @since 3.4.2
     */
    @Nonnull
    public static RequiredMetricTag of(String key, @Nullable String value) {
        return new RequiredMetricTag(key, value);
    }

    /**
     * @since 3.4.2
     */
    @Nonnull
    public static RequiredMetricTag of(String key, int value) {
        return new RequiredMetricTag(key, String.valueOf(value));
    }

    /**
     * @since 3.4.2
     */
    @Nonnull
    public static RequiredMetricTag of(String key, boolean value) {
        return new RequiredMetricTag(key, String.valueOf(value));
    }

    /**
     * @since 4.0.0
     */
    @Nonnull
    public static OptionalMetricTag optionalOf(String key, @Nullable String value) {
        return new OptionalMetricTag(key, value);
    }

    /**
     * @since 4.0.0
     */
    @Nonnull
    public static OptionalMetricTag optionalOf(String key, int value) {
        return new OptionalMetricTag(key, String.valueOf(value));
    }

    /**
     * @since 4.0.0
     */
    @Nonnull
    public static OptionalMetricTag optionalOf(String key, boolean value) {
        return new OptionalMetricTag(key, String.valueOf(value));
    }

    /**
     * @since 4.0.0
     */
    public static final class RequiredMetricTag extends MetricTag {
        private RequiredMetricTag(String key, @Nullable String value) {
            super(key, value);
        }
    }

    /**
     * Represents a metric tag that may or may not be considered when generating a metric.
     * @since 4.0.0
     */
    public static final class OptionalMetricTag extends MetricTag implements OptionalTag {
        private OptionalMetricTag(String key, @Nullable String value) {
            super(key, value);
        }

        /**
         * @since 4.0.0
         */
        RequiredMetricTag convert() {
            return new RequiredMetricTag(getKey(), getValue());
        }
    }
}
