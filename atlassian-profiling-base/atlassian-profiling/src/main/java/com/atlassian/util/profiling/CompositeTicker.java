package com.atlassian.util.profiling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.annotations.Internal;

@Internal
class CompositeTicker implements Ticker {
    private static final Logger log = LoggerFactory.getLogger(Timers.class);

    private final List<Ticker> tickers;

    private volatile boolean closed;

    CompositeTicker() {
        tickers = new ArrayList<>(4);
    }

    CompositeTicker(Ticker... values) {
        tickers = new ArrayList<>(values.length);
        tickers.addAll(Arrays.asList(values));
    }

    @Override
    public void close() {
        if (closed) {
            return;
        }
        closed = true;
        for (Ticker ticker : tickers) {
            try {
                ticker.close();
            } catch (Exception e) {
                log.debug("Failure closing ticker", e);
            }
        }
    }

    void add(Ticker ticker) {
        tickers.add(ticker);
    }
}
