package com.atlassian.util.profiling;

import com.atlassian.annotations.Internal;

/**
 * A metric that calculates the distribution of a value
 *
 * <pre>
 *     Histogram metric = Metrics.histogram("my-metric");
 *     timer.update(3);
 * </pre>
 *
 * @since 3.0
 */
@Internal
public interface Histogram {

    /**
     * Records a new value
     *
     * @param value the value to record
     */
    void update(long value);
}
