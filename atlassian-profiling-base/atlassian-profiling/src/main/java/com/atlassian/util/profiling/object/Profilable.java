package com.atlassian.util.profiling.object;

import com.atlassian.annotations.Internal;

@Internal
public interface Profilable {
    Object profile() throws Exception;
}
