package com.atlassian.util.profiling;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.annotations.Internal;
import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.util.profiling.strategy.MetricStrategy;

import static java.lang.System.getProperties;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

import static com.atlassian.util.profiling.MetricsFilter.ACCEPT_ALL;
import static com.atlassian.util.profiling.ProfilingConstants.ACTIVATE_METRICS_PROPERTY;
import static com.atlassian.util.profiling.ProfilingConstants.DEFAULT_ACTIVATE_METRICS_PROPERTY;
import static com.atlassian.util.profiling.ProfilingConstants.OPTIONAL_TAG_PROPERTY_PREFIX;

/**
 * Configuration that applies to {@link com.atlassian.util.profiling.strategy.MetricStrategy}
 *
 * @since 3.0
 */
@Internal
public class MetricsConfiguration {

    private Map<String, List<String>> metricNameToRequiredTagNames;
    private boolean enabled;
    private MetricsFilter filter = ACCEPT_ALL;

    public MetricsConfiguration() {
        reloadConfigs();
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean value) {
        enabled = value;
    }

    /**
     * @return the {@link MetricsFilter} that's currently applied
     * @since 4.1
     */
    public MetricsFilter getFilter() {
        return filter;
    }

    /**
     * Applies {@link MetricsFilter} to filter metrics that will be accepted by atlassian-profiling.
     * This will also clean up existing metrics that no longer satisfy the new filter.
     *
     * @since 4.1
     */
    public void setFilter(@Nonnull MetricsFilter filter) {
        this.filter = requireNonNull(filter);
        cleanupMetrics(filter);
    }

    @VisibleForTesting
    public void reloadConfigs() {
        enabled =
                Boolean.parseBoolean(System.getProperty(ACTIVATE_METRICS_PROPERTY, DEFAULT_ACTIVATE_METRICS_PROPERTY));
        metricNameToRequiredTagNames = computeRequiredOptionalTags();
    }

    private void cleanupMetrics(MetricsFilter filter) {
        for (MetricStrategy strategy : StrategiesRegistry.getMetricStrategies()) {
            strategy.cleanupMetrics(filter);
        }
    }

    /**
     * Checks whether an optional tag for a specific metric is enabled or not.
     *
     * @param metricName target metric name
     * @param tagName    tag name belongs to the metric
     * @return true if the optional tag is enabled, false otherwise.
     */
    boolean isOptionalTagEnabled(@Nonnull final String metricName, @Nullable final String tagName) {
        List<String> tags = metricNameToRequiredTagNames.get(metricName);
        return tags != null && tags.contains(tagName);
    }

    /**
     * Look for any system property starting with {@link ProfilingConstants#OPTIONAL_TAG_PROPERTY_PREFIX}
     * and extract the trailing key characters as the metric name and extract the value as a delimited list of tags
     * to be enabled.
     *
     * @return a {@link Map} where key is the metric name & value is a list of tags
     */
    private Map<String, List<String>> computeRequiredOptionalTags() {
        Map<String, List<String>> requiredMetricOptionalTags = new HashMap<>();
        getProperties().forEach((key, val) -> {
            final String propKey = key.toString();
            if (propKey.startsWith(OPTIONAL_TAG_PROPERTY_PREFIX)) {
                String metricName = propKey.substring(OPTIONAL_TAG_PROPERTY_PREFIX.length());
                final List<String> tags = Arrays.stream(val.toString().split(","))
                        .map(String::trim)
                        .filter(string -> !string.isEmpty())
                        .collect(toList());
                if (!tags.isEmpty()) {
                    requiredMetricOptionalTags.put(metricName, tags);
                }
            }
        });

        return requiredMetricOptionalTags;
    }
}
