package com.atlassian.util.profiling.filters;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.FilterConfig;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.util.profiling.ProfilerConfiguration;

import static com.atlassian.util.profiling.Timers.getConfiguration;

/**
 * @deprecated in 3.0 for removal in 5.0. Enabling or disabling system-wide profiling from request parameters is
 *             deprecated. Use per-request profiler provided by {@link RequestProfilingFilter} instead.
 */
@Deprecated
public class ProfilingStatusUpdateViaRequestStrategy implements StatusUpdateStrategy, FilterConfigAware {

    private static final Logger log = LoggerFactory.getLogger(ProfilingStatusUpdateViaRequestStrategy.class);

    /**
     * Default parameter for turning the filter on and off. eg.
     * <pre>
     * http://mywebsite.com/a.jsp?profile.filter=on
     * http://mywebsite.com/a.jsp?profile.filter=off
     * </pre>
     * Most often you will want to change this default value by passing an init parameter to the filter.  This is
     * configurable as it could be a potential security hole if users can randomly start and stop the profiling.
     * <p>
     * Security through obscurity - yes I know its bad!
     */
    private static final String DEFAULT_ON_OFF_PARAM = "profile.filter";

    /**
     * This is static as there may be more than one instance of a servlet filter.
     */
    private static String onOffParameter = DEFAULT_ON_OFF_PARAM;

    private static Pattern onOffParameterPattern = Pattern.compile(onOffParameter + "=([\\w\\d]+)");

    /**
     * This is the parameter you pass to the init parameter &amp; specify in the web.xml file: eg.
     * <pre>
     * &lt;filter&gt;
     *   &lt;filter-name&gt;profile&lt;/filter-name&gt;
     *   &lt;filter-class&gt;com.atlassian.util.profiling.filters.ProfilingFilter&lt;/filter-class&gt;
     *  &lt;init-param&gt;
     *    &lt;param-name&gt;activate.param&lt;/param-name&gt;
     *    &lt;param-value&gt;filter.changestatus&lt;/param-value&gt;
     *  &lt;/init-param&gt;
     * &lt;/filter&gt;
     *  </pre>
     */
    static final String ON_OFF_INIT_PARAM = "activate.param";

    public void configure(FilterConfig filterConfig) {
        if (filterConfig.getInitParameter(ON_OFF_INIT_PARAM) != null) {
            log.debug(
                    "[Filter: {}] Using parameter [{}]",
                    filterConfig.getFilterName(),
                    filterConfig.getInitParameter(ON_OFF_INIT_PARAM));
            setOnOffParameter(filterConfig);
        }
    }

    public void setStateViaRequest(ServletRequest request) {
        String paramValue = null;

        // Let's try to get the profiling value from the query string instead of getParameter()
        // to allow for the request to later have its encoding set
        if (request instanceof HttpServletRequest) {
            String queryString = ((HttpServletRequest) request).getQueryString();
            if (queryString != null) {
                Matcher m = getOnOffParameterPattern().matcher(queryString);
                if (m.find()) {
                    paramValue = m.group(1);
                }
            }
        }

        if (paramValue != null) {
            setProfilingState(paramValue);
        }
    }

    protected void turnProfilingOn() {
        log.debug("Turning profiling on [{}=on]", onOffParameter);
        getConfiguration().setEnabled(true);
    }

    protected void turnProfilingOnAndSetThreshold(long minTotalTime) {
        log.debug("Turning profiling on [{}=on] with threshold {}ms", onOffParameter, minTotalTime);
        ProfilerConfiguration config = getConfiguration();
        config.setMinTraceTime(minTotalTime, TimeUnit.MILLISECONDS);
        config.setEnabled(true);
    }

    protected void turnProfilingOff() {
        log.debug("Turning profiling off [{}=off]", onOffParameter);
        ProfilerConfiguration config = getConfiguration();
        config.setMinTraceTime(0L, TimeUnit.MILLISECONDS);
        config.setEnabled(false);
    }

    static Pattern getOnOffParameterPattern() {
        return onOffParameterPattern;
    }

    private static void setOnOffParameter(FilterConfig filterConfig) {
        onOffParameter = filterConfig.getInitParameter(ON_OFF_INIT_PARAM);
        onOffParameterPattern = Pattern.compile(onOffParameter + "=([\\w\\d]+)");
    }

    private void setProfilingState(String paramValue) {
        if ("on".equals(paramValue) || "true".equals(paramValue)) {
            turnProfilingOn();
        } else if ("off".equals(paramValue) || "false".equals(paramValue)) {
            turnProfilingOff();
        } else if (paramValue.length() > 0 && Character.isDigit(paramValue.charAt(0))) {
            try {
                turnProfilingOnAndSetThreshold(Long.parseLong(paramValue));
            } catch (NumberFormatException e) {
                log.debug("Could not parse {} to Long value", paramValue);
            }
        }
    }
}
