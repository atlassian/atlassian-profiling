package com.atlassian.util.profiling;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.annotations.Internal;
import com.atlassian.plugin.util.PluginKeyStack;
import com.atlassian.util.profiling.strategy.MetricStrategy;

import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toMap;

import static com.atlassian.util.profiling.MetricKey.metricKey;
import static com.atlassian.util.profiling.MetricTag.FROM_PLUGIN_KEY_TAG_KEY;
import static com.atlassian.util.profiling.MetricTag.INVOKER_PLUGIN_KEY_TAG_KEY;
import static com.atlassian.util.profiling.MetricTag.SEND_ANALYTICS;

/**
 * Factory and utility methods for creating {@link MetricTimer}, {@link LongRunningMetricTimer}, and {@link Histogram}.
 *
 * <em>Please move to using the builder.</em>
 *
 * <pre>{@code
 *     try (Ticker autoclosed = Metrics.metric("my-timer").startTimer()) {
 *         // monitored code block here
 *     }
 * }</pre>
 *
 * @since 3.0.0
 */
@Internal
@ParametersAreNonnullByDefault
public class Metrics {
    private static final MetricsConfiguration CONFIGURATION = new MetricsConfiguration();

    private Metrics() {
        throw new UnsupportedOperationException("Metrics is an utility class and should not be instantiated");
    }

    /**
     * Resets (that may mean removal depending on the metric strategy) a specific metric that exactly matches a {@link MetricKey}.
     *
     * @param metricKey metric key to be reset.
     *
     * @since 4.9.0
     */
    public static void resetMetric(MetricKey metricKey) {
        for (MetricStrategy strategy : StrategiesRegistry.getMetricStrategies()) {
            strategy.resetMetric(metricKey);
        }
    }

    /**
     * Get the current {@link MetricsConfiguration}.
     * @return the current configuration.
     * @since 3.0.0
     */
    @Nonnull
    public static MetricsConfiguration getConfiguration() {
        return CONFIGURATION;
    }

    /**
     * Creates a {@link Histogram} with the given name.
     * @deprecated since 4.3.0, for removal in 5.0.0, use the {@link Metrics#metric(String)} builder instead.
     * @return the created {@link Histogram}
     * @since 3.4.0
     */
    @Deprecated
    @Nonnull
    public static Histogram histogram(String name) {
        return metric(name).histogram();
    }

    /**
     * Creates a {@link Histogram} with the given name, and tags it with the given tags.
     * <p>
     * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
     * @deprecated since 4.3.0, for removal in 5.0.0, use the {@link Metrics#metric(String)} builder instead.
     * @return the created {@link Histogram}
     * @since 3.4.0
     */
    @Deprecated
    @Nonnull
    public static Histogram histogram(String name, Collection<MetricTag.RequiredMetricTag> tags) {
        return metric(name).tags(tags).histogram();
    }

    /**
     * Creates a {@link Histogram} with the given name, and tags it with the given tags.
     * <p>
     * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
     * @deprecated since 4.3.0, for removal in 5.0.0, use the {@link Metrics#metric(String)} builder instead.
     * @return the created {@link Histogram}
     * @since 3.4.0
     */
    @Deprecated
    @Nonnull
    public static Histogram histogram(String name, MetricTag.RequiredMetricTag... tags) {
        return metric(name).tags(tags).histogram();
    }

    /**
     * Creates a {@link MetricTimer} with the given name.
     * @deprecated since 4.3.0, for removal in 5.0.0, use the {@link Metrics#metric(String)} builder instead.
     * @return the created {@link MetricTimer}
     * @since 3.4.0
     */
    @Deprecated
    @Nonnull
    public static MetricTimer timer(String name) {
        return metric(name).timer();
    }

    /**
     * Creates a {@link MetricTimer} with the given name, and tags it with the given tags.
     * <p>
     * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
     * @deprecated since 4.3.0, for removal in 5.0.0, use the {@link Metrics#metric(String)} builder instead.
     * @return the created {@link MetricTimer}
     * @since 3.4.0
     */
    @Deprecated
    @Nonnull
    public static MetricTimer timer(String name, Collection<MetricTag.RequiredMetricTag> tags) {
        return metric(name).tags(tags).timer();
    }

    /**
     * Creates a {@link MetricTimer} with the given name, and tags it with the given tags.
     * <p>
     * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
     * @deprecated since 4.3.0, for removal in 5.0.0, use the {@link Metrics#metric(String)} builder instead.
     * @return the created {@link MetricTimer}
     * @since 3.4.0
     */
    @Deprecated
    @Nonnull
    public static MetricTimer timer(String name, MetricTag.RequiredMetricTag... tags) {
        return metric(name).tags(tags).timer();
    }

    /**
     * Creates a {@link LongRunningMetricTimer} with the given name.
     * @deprecated since 4.3.0, for removal in 5.0.0, use the {@link Metrics#metric(String)} builder instead.
     * @return the created {@link LongRunningMetricTimer}
     * @since 3.5.0
     */
    @Deprecated
    @Nonnull
    public static LongRunningMetricTimer longRunningTimer(String name) {
        return metric(name).longRunningTimer();
    }

    /**
     * Creates a {@link LongRunningMetricTimer} with the given name, and tags it with the given tags.
     * <p>
     * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
     * @deprecated since 4.3.0, for removal in 5.0.0, use the {@link Metrics#metric(String)} builder instead.
     * @return the created {@link LongRunningMetricTimer}
     * @since 3.5.0
     */
    @Deprecated
    @Nonnull
    public static LongRunningMetricTimer longRunningTimer(String name, Collection<MetricTag.RequiredMetricTag> tags) {
        return metric(name).tags(tags).longRunningTimer();
    }

    /**
     * Creates a {@link LongRunningMetricTimer} with the given name, and tags it with the given tags.
     * <p>
     * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
     * @deprecated since 4.3.0, for removal in 5.0.0, use the {@link Metrics#metric(String)} builder instead.
     * @return the created {@link LongRunningMetricTimer}
     * @since 3.5.0
     */
    @Deprecated
    @Nonnull
    public static LongRunningMetricTimer longRunningTimer(String name, MetricTag.RequiredMetricTag... tags) {
        return longRunningTimer(name, Arrays.asList(tags));
    }

    /**
     * Starts a {@link MetricTimer} with the given name.
     * @deprecated since 4.3.0, for removal in 5.0.0, use the {@link Metrics#metric(String)} builder instead.
     * @return a {@link Ticker} to stop the created and started {@link MetricTimer}
     * @since 3.4.0
     */
    @Deprecated
    @Nonnull
    public static Ticker startTimer(String name) {
        return startTimer(name, emptySet());
    }

    /**
     * Starts a {@link MetricTimer} with the given name, and tags it with the given tags.
     * <p>
     * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
     * @deprecated since 4.3.0, for removal in 5.0.0, use the {@link Metrics#metric(String)} builder instead.
     * @return a {@link Ticker} to stop the created and started {@link MetricTimer}
     * @since 3.4.0
     */
    @Deprecated
    @Nonnull
    public static Ticker startTimer(String name, MetricTag.RequiredMetricTag... tags) {
        return startTimer(name, asList(tags));
    }

    /**
     * Starts a {@link MetricTimer} with the given name, and tags it with the given tags.
     * <p>
     * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
     * @deprecated since 4.3.0, for removal in 5.0.0, use the {@link Metrics#metric(String)} builder instead.
     * @return a {@link Ticker} to stop the created and started {@link MetricTimer}
     * @since 3.4.0
     */
    @Deprecated
    @Nonnull
    public static Ticker startTimer(String name, Collection<MetricTag.RequiredMetricTag> tags) {
        return metric(name).tags(tags).startTimer();
    }

    /**
     * Starts a {@link LongRunningMetricTimer} with the given name, and tags it with the given tags.
     * @deprecated since 4.3.0, for removal in 5.0.0, use the {@link Metrics#metric(String)} builder instead.
     * @return a {@link Ticker} to stop the created and started {@link LongRunningMetricTimer}
     * @since 3.5.0
     */
    @Deprecated
    @Nonnull
    public static Ticker startLongRunningTimer(String name) {
        return metric(name).startLongRunningTimer();
    }

    /**
     * Starts a {@link LongRunningMetricTimer} with the given name, and tags it with the given tags.
     * <p>
     * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
     * @deprecated since 4.3.0, for removal in 5.0.0, use the {@link Metrics#metric(String)} builder instead.
     * @return a {@link Ticker} to stop the created and started {@link LongRunningMetricTimer}
     * @since 3.5.0
     */
    @Deprecated
    @Nonnull
    public static Ticker startLongRunningTimer(String name, MetricTag.RequiredMetricTag... tags) {
        return metric(name).tags(tags).startLongRunningTimer();
    }

    /**
     * Starts a {@link LongRunningMetricTimer} with the given name, and tags it with the given tags.
     * <p>
     * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
     * @deprecated since 4.3.0, for removal in 5.0.0, use the {@link Metrics#metric(String)} builder instead.
     * @return a {@link Ticker} to stop the created and started {@link LongRunningMetricTimer}
     * @since 3.5.0
     */
    @Deprecated
    @Nonnull
    public static Ticker startLongRunningTimer(String name, Collection<MetricTag.RequiredMetricTag> tags) {
        return metric(name).tags(tags).startLongRunningTimer();
    }

    /**
     * Start creating a metric using the {@link Metrics.Builder}
     * @param name metric name
     * @return a metric builder
     * @since 3.5.0
     */
    public static Builder metric(String name) {
        return new Builder(name);
    }

    /**
     * A metric builder, to get started call {@link Metrics#metric(String)}.
     * @since 3.5.0
     */
    @ParametersAreNonnullByDefault
    public static class Builder {

        private final String name;
        private final Map<String, MetricTag.RequiredMetricTag> requiredTags = new HashMap<>();
        private final Map<String, MetricTag.OptionalMetricTag> optionalTags = new HashMap<>();

        Builder(String name) {
            this.name = name;
        }

        /**
         * Collects {@link MetricTag.OptionalMetricTag} that may exist in a {@link MetricTagContext}
         * @since 4.0.0
         */
        @Nonnull
        public Builder collect(final String... metricTagKeys) {
            return collect(asList(metricTagKeys));
        }

        /**
         * Collects {@link MetricTag.OptionalMetricTag} that may exist in a {@link MetricTagContext}
         * @since 4.0.0
         */
        @Nonnull
        public Builder collect(final Iterable<String> metricTagKeys) {
            final Set<String> metricTagKeysSet =
                    StreamSupport.stream(metricTagKeys.spliterator(), false).collect(Collectors.toSet());

            return tags(MetricTagContext.getAll().stream()
                    .filter(metricTag -> metricTagKeysSet.contains(metricTag.getKey()))
                    .map(MetricTag.OptionalMetricTag::convert)
                    .collect(Collectors.toList()));
        }

        /**
         * Adds a tag to the metric.
         * <p>
         * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
         * @since 4.4.0
         */
        @Nonnull
        public Builder tag(String key, boolean value) {
            return tags(MetricTag.of(key, value));
        }

        /**
         * Adds a tag to the metric.
         * <p>
         * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
         * @since 4.4.0
         */
        @Nonnull
        public Builder tag(String key, int value) {
            return tags(MetricTag.of(key, value));
        }

        /**
         * Adds a tag to the metric.
         * <p>
         * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
         * @since 3.5.0
         */
        @Nonnull
        public Builder tag(String key, @Nullable String value) {
            return tags(MetricTag.of(key, value));
        }

        /**
         * Adds tags to the metric.
         * <p>
         * See {@link Metrics.Builder#tags(Iterable)} for more information about tag behaviour.
         * @since 3.5.0
         */
        @Nonnull
        public Builder tags(MetricTag.RequiredMetricTag... tags) {
            return tags(asList(tags));
        }

        /**
         * Adds tags to the metric.
         * <p>
         * <strong>Note:</strong> If there is an existing tag with the same key, the new value will replace the old.
         * <p>
         * <strong>Note:</strong> If multiple tags with the same key are passed, the last one will win.
         * <p>
         * <strong>Note:</strong> There is no guarantee in the order of tags in the output, that is the responsibility
         * of each {@link MetricStrategy} strategy used.
         * @since 3.5.0
         */
        @Nonnull
        public Builder tags(Iterable<MetricTag.RequiredMetricTag> requiredMetricTags) {
            requiredMetricTags.forEach(
                    requiredMetricTag -> requiredTags.put(requiredMetricTag.getKey(), requiredMetricTag));
            return this;
        }

        /**
         * Adds an optional tag to the metric.
         * <p>
         * See {@link Metrics.Builder#optionalTags(Iterable)} for more information about optional tag behaviour.
         * @since 4.4.0
         */
        @Nonnull
        public Builder optionalTag(String key, boolean value) {
            return optionalTags(MetricTag.optionalOf(key, value));
        }

        /**
         * Adds an optional tag to the metric.
         * <p>
         * See {@link Metrics.Builder#optionalTags(Iterable)} for more information about optional tag behaviour.
         * @since 4.4.0
         */
        @Nonnull
        public Builder optionalTag(String key, int value) {
            return optionalTags(MetricTag.optionalOf(key, value));
        }

        /**
         * Adds an optional tag to the metric.
         * <p>
         * See {@link Metrics.Builder#optionalTags(Iterable)} for more information about optional tag behaviour.
         * @since 4.0.0
         */
        @Nonnull
        public Builder optionalTag(String key, @Nullable String value) {
            return optionalTags(MetricTag.optionalOf(key, value));
        }

        /**
         * Adds optional tags to the metric.
         * <p>
         * See {@link Metrics.Builder#optionalTags(Iterable)} for more information about optional tag behaviour.
         * @since 4.0.0
         */
        @Nonnull
        public Builder optionalTags(MetricTag.OptionalMetricTag... optionalMetricTags) {
            return optionalTags(asList(optionalMetricTags));
        }

        /**
         * Adds optional tags to the metric.
         * <p>
         * <strong>Note:</strong> If there is an existing {@link MetricTag.OptionalMetricTag} with the same key, the new value will replace the old.
         * <p>
         * <strong>Note:</strong> If there is a {@link MetricTag.RequiredMetricTag} with the same key it will win, regardless of order.
         * <p>
         * <strong>Note:</strong> If there is an {@link MetricTag.OptionalMetricTag} with same key in a {@link MetricTagContext}
         * then those provided to the builder will take priority
         * <p>
         * <strong>Note:</strong> There is no guarantee in the order of tags in the output, that is the responsibility
         * of each {@link MetricStrategy} strategy used.
         *
         * @since 4.0.0
         */
        @Nonnull
        public Builder optionalTags(Iterable<MetricTag.OptionalMetricTag> optionalMetricTags) {
            optionalMetricTags.forEach(
                    optionalMetricTag -> optionalTags.put(optionalMetricTag.getKey(), optionalMetricTag));
            return this;
        }

        private boolean isOptionalTagEnabled(MetricTag.OptionalMetricTag optionalMetricTag) {
            return CONFIGURATION.isOptionalTagEnabled(name, optionalMetricTag.getKey());
        }

        /**
         * Attribute a plugin for providing a module that is measured
         * @since 3.6.0
         */
        @Nonnull
        public Builder fromPluginKey(@Nullable String pluginKey) {
            if (isNull(pluginKey)) {
                requiredTags.remove(FROM_PLUGIN_KEY_TAG_KEY);
                return this;
            }

            return tag(FROM_PLUGIN_KEY_TAG_KEY, pluginKey);
        }

        /**
         * Attribute the plugin for invoking measured code
         * @since 3.6.0
         */
        @Nonnull
        public Builder invokerPluginKey(@Nullable String pluginKey) {
            if (isNull(pluginKey)) {
                requiredTags.remove(INVOKER_PLUGIN_KEY_TAG_KEY);
                return this;
            }

            return tag(INVOKER_PLUGIN_KEY_TAG_KEY, pluginKey);
        }

        /**
         * Attribute the plugin at the top of the call stack for invoking measured code
         * <p>
         * <strong>No guarantee</strong>, that the call stack will be able to be traversed, i.e if work is
         * delegated to another thread.
         * @since 3.6.0
         */
        @Nonnull
        public Builder withInvokerPluginKey() {
            return invokerPluginKey(PluginKeyStack.getFirstPluginKey());
        }

        /**
         * Mark any created metrics to be sent to analytics.
         * <p>
         * <strong>Reminder:</strong> add any tags to the appropriate analytics allow-list.
         * @since 3.5.0
         */
        @Nonnull
        public Builder withAnalytics() {
            return tags(SEND_ANALYTICS);
        }

        /**
         * Creates a {@link Histogram} with information in the builder.
         * @return the created {@link Histogram}
         * @since 3.5.0
         */
        @Nonnull
        public Histogram histogram() {
            return new DefaultHistogram(metricKey(name, getAllTags()));
        }

        /**
         * <strong>Does not guarantee</strong> safety from lost-updates, check implementations.
         * <p>
         * Called to increment to count the number of occurrences with information in the builder. The delta,
         * <strong>must be positive</strong>.
         * @param deltaValue the change in gauge that should be recorded
         * @since 3.7.0
         */
        public void incrementCounter(@Nullable Long deltaValue) {
            MetricKey metricKey = metricKey(name, getAllTags());

            if (!accepts(metricKey)) {
                return;
            }

            if (isNull(deltaValue)) {
                return;
            }

            if (deltaValue < 0) {
                throw new IllegalArgumentException("The delta value must be not be negative; received: " + deltaValue
                        + " for the metric: " + metricKey);
            }

            StrategiesRegistry.getMetricStrategies()
                    .forEach(metricStrategy -> metricStrategy.incrementCounter(metricKey, deltaValue));
        }

        /**
         * <strong>Does not guarantee</strong> safety from lost-updates, check implementations.
         * <p>
         * Called to increment the gauge by the value. Starts from 0
         * @since 4.8.0
         */
        public void incrementGauge(@Nullable Long deltaValue) {
            final MetricKey metricKey = metricKey(name, getAllTags());

            if (!accepts(metricKey)) {
                return;
            }

            if (isNull(deltaValue)) {
                return;
            }

            StrategiesRegistry.getMetricStrategies()
                    .forEach(metricStrategy -> metricStrategy.incrementGauge(metricKey, deltaValue));
        }

        /**
         * Called to set the gauge by the value
         *
         * @since 4.8.0
         */
        public void setGauge(@Nullable Long currentValue) {
            final MetricKey metricKey = metricKey(name, getAllTags());

            if (!accepts(metricKey)) {
                return;
            }

            if (isNull(currentValue)) {
                return;
            }

            StrategiesRegistry.getMetricStrategies()
                    .forEach(metricStrategy -> metricStrategy.setGauge(metricKey, currentValue));
        }

        /**
         * Creates a {@link MetricTimer} with information in the builder.
         * @return the created {@link MetricTimer}
         * @since 3.5.0
         */
        @Nonnull
        public MetricTimer timer() {
            return new DefaultMetricTimer(metricKey(name, getAllTags()));
        }

        /**
         * Returns a {@link Collection} of required and enabled optional tags including enabled tags
         * from {@link MetricTagContext}. Required tags takes precedence, thus if an optional
         * and a required tag exist with the same key then the required tag will be chosen instead.
         *
         * @return a {@link Collection} of required and enabled tags
         */
        private Collection<MetricTag.RequiredMetricTag> getAllTags() {
            Set<MetricTag.OptionalMetricTag> optionalContext = MetricTagContext.getAll();
            if (optionalContext.isEmpty() && optionalTags.isEmpty()) {
                return requiredTags.values();
            }
            final Stream<MetricTag.RequiredMetricTag> allEnabledOptionalTags = Stream.concat(
                            optionalContext.stream(), optionalTags.values().stream())
                    .filter(this::isOptionalTagEnabled)
                    .map(MetricTag.OptionalMetricTag::convert);

            return Stream.concat(allEnabledOptionalTags, requiredTags.values().stream())
                    .collect(toMap(
                            MetricTag::getKey, Function.identity(), (firstTag, secondTag) -> secondTag)) // last-wins
                    .values();
        }

        /**
         * Creates a {@link LongRunningMetricTimer} with information in the builder.
         * @return the created {@link LongRunningMetricTimer}
         * @since 3.6.0
         */
        @Nonnull
        public LongRunningMetricTimer longRunningTimer() {
            return new DefaultLongRunningMetricTimer(metricKey(name, getAllTags()));
        }

        /**
         * Starts a {@link MetricTimer} with information in the builder.
         * @return a {@link Ticker} to stop the created and started {@link MetricTimer}
         * @since 3.5.0
         */
        @Nonnull
        public Ticker startTimer() {
            return timer().start();
        }

        /**
         * Starts a {@link LongRunningMetricTimer} with information in the builder.
         * @return a {@link Ticker} to stop the created and started {@link LongRunningMetricTimer}
         * @since 3.6.0
         */
        @Nonnull
        public Ticker startLongRunningTimer() {
            return longRunningTimer().start();
        }
    }

    /**
     * @since 3.0.0
     */
    @ParametersAreNonnullByDefault
    private static class DefaultMetricTimer implements MetricTimer {
        private static final Logger log = LoggerFactory.getLogger(DefaultMetricTimer.class);

        private final MetricKey metricKey;

        DefaultMetricTimer(MetricKey name) {
            this.metricKey = requireNonNull(name, "metricKey");
        }

        @Nonnull
        @Override
        public Ticker start() {
            if (!accepts(metricKey)) {
                return Ticker.NO_OP;
            }

            Collection<MetricStrategy> metricStrategies = StrategiesRegistry.getMetricStrategies();
            if (metricStrategies.isEmpty()) {
                return Ticker.NO_OP;
            }

            CompositeTicker compositeTicker = null;
            for (MetricStrategy strategy : metricStrategies) {
                try {
                    compositeTicker = Tickers.addTicker(strategy.startTimer(metricKey), compositeTicker);
                } catch (RuntimeException e) {
                    log.warn("Failed to start metric trace for {}", metricKey, e);
                }
            }

            return compositeTicker == null ? Ticker.NO_OP : compositeTicker;
        }

        @Override
        public void update(Duration time) {
            requireNonNull(time, "time");
            if (!accepts(metricKey)) {
                return;
            }

            for (MetricStrategy strategy : StrategiesRegistry.getMetricStrategies()) {
                try {
                    strategy.updateTimer(metricKey, time);
                } catch (RuntimeException e) {
                    log.warn("Failed to update metric for {}", metricKey, e);
                }
            }
        }
    }

    /**
     * @since 3.5.0
     */
    @ParametersAreNonnullByDefault
    private static class DefaultLongRunningMetricTimer implements LongRunningMetricTimer {
        private static final Logger log = LoggerFactory.getLogger(DefaultLongRunningMetricTimer.class);

        private final MetricKey metricKey;

        DefaultLongRunningMetricTimer(MetricKey name) {
            this.metricKey = requireNonNull(name, "metricKey");
        }

        @Nonnull
        @Override
        public Ticker start() {
            if (!accepts(metricKey)) {
                return Ticker.NO_OP;
            }

            Collection<MetricStrategy> metricStrategies = StrategiesRegistry.getMetricStrategies();
            if (metricStrategies.isEmpty()) {
                return Ticker.NO_OP;
            }

            CompositeTicker compositeTicker = null;
            for (MetricStrategy strategy : metricStrategies) {
                try {
                    compositeTicker = Tickers.addTicker(strategy.startLongRunningTimer(metricKey), compositeTicker);
                } catch (RuntimeException e) {
                    log.warn("Failed to start metric trace for {}", metricKey, e);
                }
            }

            return compositeTicker == null ? Ticker.NO_OP : compositeTicker;
        }
    }

    /**
     * @since 3.0.0
     */
    @ParametersAreNonnullByDefault
    private static class DefaultHistogram implements Histogram {
        private static final Logger log = LoggerFactory.getLogger(DefaultHistogram.class);

        private final MetricKey metricKey;

        private DefaultHistogram(MetricKey metricKey) {
            this.metricKey = requireNonNull(metricKey, "metricKey");
        }

        @Override
        public void update(long value) {
            if (!accepts(metricKey)) {
                return;
            }

            for (MetricStrategy strategy : StrategiesRegistry.getMetricStrategies()) {
                try {
                    strategy.updateHistogram(metricKey, value);
                } catch (RuntimeException e) {
                    log.warn("Failed to update histogram for {}", metricKey, e);
                }
            }
        }
    }

    /**
     * A helper to determine if the metric should be created/updated.
     * @since 4.1.0
     */
    private static boolean accepts(MetricKey metricKey) {
        return getConfiguration().isEnabled() && getConfiguration().getFilter().accepts(metricKey.getMetricName());
    }
}
