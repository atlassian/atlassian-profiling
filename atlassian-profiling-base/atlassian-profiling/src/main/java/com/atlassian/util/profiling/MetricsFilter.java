package com.atlassian.util.profiling;

import java.util.Collection;

import com.atlassian.annotations.Internal;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toSet;

/**
 * Allows filtering of metrics that will be included or excluded from the published metrics.
 * @see com.atlassian.util.profiling.MetricsConfiguration
 * @since 4.1
 */
@FunctionalInterface
@Internal
public interface MetricsFilter {

    /**
     * A filter that guarantees the inclusion of all meters
     */
    MetricsFilter ACCEPT_ALL = name -> true;

    /**
     * A filter that guarantees the exclusion of all meters
     */
    MetricsFilter DENY_ALL = name -> false;

    /**
     * Metrics with the specified names should NOT be present in published metrics.
     * @param names When a metric name matches any of the specified values, it will be excluded from the published metrics.
     * @return A filter that guarantees the exclusion of meters with any of the specified names.
     */
    static MetricsFilter deny(String... names) {
        return deny(stream(names).collect(toSet()));
    }

    /**
     * Metrics with the specified names should NOT be present in published metrics.
     * @param names When a metric name matches any of the specified values, it will be excluded from the published metrics.
     * @return A filter that guarantees the exclusion of meters with any of the specified names.
     */
    static MetricsFilter deny(Collection<String> names) {
        return name -> !names.contains(name);
    }

    /**
     * @param name metrics name
     * @return true if metrics with the given name should be accepted
     */
    boolean accepts(String name);
}
