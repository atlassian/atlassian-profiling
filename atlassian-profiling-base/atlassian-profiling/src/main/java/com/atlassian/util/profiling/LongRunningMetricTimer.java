package com.atlassian.util.profiling;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.annotations.Internal;

/**
 * Object that is used to collect time metrics about the execution a block of code.
 *
 * <pre>
 *     LongRunningMetricTimer timer = Metrics.longRunningTimer("my-metric");
 *     try (Ticker ignored = timer.start()) {
 *         // monitored code block
 *     }
 * </pre>
 *
 * @since 3.5.0
 */
@Internal
@ParametersAreNonnullByDefault
public interface LongRunningMetricTimer {

    /**
     * Starts a {@link Ticker ticker} for a monitored code block
     *
     * @return the ticker
     */
    @Nonnull
    Ticker start();
}
