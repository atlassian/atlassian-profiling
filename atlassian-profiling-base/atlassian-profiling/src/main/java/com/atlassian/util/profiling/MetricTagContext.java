package com.atlassian.util.profiling;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;

import com.atlassian.annotations.Internal;
import com.atlassian.profiling.metrics.api.context.ContextFragment;
import com.atlassian.profiling.metrics.api.tags.OptionalTag;

import static java.util.Collections.emptySet;
import static java.util.Objects.requireNonNull;

/**
 * <strong>Internal only version</strong> of {@link com.atlassian.profiling.metrics.api.context.MetricContext}
 * <p>
 * Provides similar capability to <a href="http://logback.qos.ch/manual/mdc.html">MDC</a>,
 * i.e. for adding contextual tags onto all metrics generated during the enclosed execution context.
 * <p>
 * E.g.
 * <pre>{@code
 * try (Closeable t = MetricTagContext.put(MetricTag.optionalOf("projectKey", projectKey))) {
 *     reindexIssue(issueId);
 * }
 * }</pre>
 *
 * Only takes {@link MetricTag.OptionalMetricTag}s to prevent over collection.
 */
@Internal
public class MetricTagContext {
    private static final InheritableThreadLocal<Set<MetricTag.OptionalMetricTag>> threadLocal =
            new InheritableThreadLocal<Set<MetricTag.OptionalMetricTag>>() {
                @Override
                protected Set<MetricTag.OptionalMetricTag> childValue(Set<MetricTag.OptionalMetricTag> parentValue) {
                    return parentValue == null ? null : new HashSet<>(parentValue);
                }
            };

    /**
     * @since 4.6.0
     */
    @Nonnull
    public static ContextFragment put(@Nonnull final OptionalTag... tags) {
        requireNonNull(tags, "tags");
        // This could be of type List<ContextFragment> but this should be an extra guard against recursion
        List<Closeable> closeableList = Arrays.stream(tags)
                .map(tag -> MetricTag.optionalOf(tag.getKey(), tag.getValue()))
                .map(MetricTagContext::put)
                .collect(Collectors.toList());
        return () -> closeableList.forEach(ContextFragment::close);
    }

    /**
     * @deprecated Use {@link MetricTagContext#put(OptionalTag...)}, for removal in 5.0.0
     */
    public static Closeable put(MetricTag.OptionalMetricTag tag) {
        Set<MetricTag.OptionalMetricTag> set = getOrCreateLocalSet();

        if (set.add(tag)) {
            return () -> set.remove(tag);
        }
        return () -> {};
    }

    @Nonnull
    public static Set<MetricTag.OptionalMetricTag> getAll() {
        final Set<MetricTag.OptionalMetricTag> set = threadLocal.get();
        return set == null ? emptySet() : new HashSet<>(set);
    }

    private static Set<MetricTag.OptionalMetricTag> getOrCreateLocalSet() {
        Set<MetricTag.OptionalMetricTag> set = threadLocal.get();
        if (set == null) {
            threadLocal.set(set = new HashSet<>());
        }
        return set;
    }

    /**
     * @deprecated use {@link ContextFragment} instead, don't use this with new methods, leave existing ones alone for
     * now for backwards-compatibility. For removal in 5.0
     */
    public interface Closeable extends ContextFragment {}
}
