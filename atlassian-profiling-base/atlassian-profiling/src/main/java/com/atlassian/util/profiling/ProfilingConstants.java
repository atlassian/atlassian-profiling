package com.atlassian.util.profiling;

import com.atlassian.annotations.Internal;

/**
 * @since 3.0
 */
@Internal
public final class ProfilingConstants {
    /**
     * System property prefix for optional tags to be enabled. The value is a comma delimited list of optional tags to
     * be enabled.
     * <p>
     * Usage:
     * <pre>{@code atlassian.metrics.optional.tags.condition=url,location}</pre>
     * The above system property will enable `url` &amp; `location` optional tags on the condition metric
     */
    public static final String OPTIONAL_TAG_PROPERTY_PREFIX = "atlassian.metrics.optional.tags.";
    /**
     * System property that controls whether this memory usage details should be included in profiling or not.
     * Set to "true" to activate. Set to "false" to deactivate.
     */
    public static final String ACTIVATE_MEMORY_PROPERTY = "atlassian.profile.activate.memory";
    /**
     * System property that specifies by default whether metrics should be used or not. Set to "true" activates the
     * timer. Set to "false" to deactivate.
     */
    public static final String ACTIVATE_METRICS_PROPERTY = "atlassian.metrics.activate";
    /**
     * System property that specifies by default whether this timer should be used or not. Set to "true" activates the
     * timer. Set to "false" to deactivate.
     */
    public static final String ACTIVATE_PROPERTY = "atlassian.profile.activate";
    /**
     * Default value that is used if {@link #ACTIVATE_PROPERTY} is not set.
     */
    public static final String DEFAULT_ACTIVATE_PROPERTY = "false";
    /**
     * Default value that is used if {@link #ACTIVATE_METRICS_PROPERTY} is not set.
     */
    public static final String DEFAULT_ACTIVATE_METRICS_PROPERTY = "true";
    /**
     * Default value that is used if {@link #ACTIVATE_MEMORY_PROPERTY} is not set.
     */
    public static final String DEFAULT_ACTIVATE_MEMORY_PROPERTY = "false";
    /**
     * Default value that is used if {@link #MAX_FRAME_COUNT} is not set.
     */
    public static final int DEFAULT_MAX_FRAME_COUNT = 1000;
    /**
     * Default value that is used if {@link #MAX_FRAME_LENGTH} is not set.
     */
    public static final int DEFAULT_MAX_FRAME_LENGTH = 150;
    /**
     * Default value that is used if {@link #MIN_TIME} is not set.
     */
    public static final int DEFAULT_MIN_TIME = 0;
    /**
     * Default value that is used if {@link #MIN_TOTAL_TIME} is not set.
     */
    public static final int DEFAULT_MIN_TOTAL_TIME = 0;
    /**
     * System property that controls the default maximum number of timer frames that can be reported per timer stack.
     */
    public static final String MAX_FRAME_COUNT = "atlassian.profile.maxframecount";
    /**
     * System property that controls the default maximum length of timer frame names.
     */
    public static final String MAX_FRAME_LENGTH = "atlassian.profile.maxframelength";
    /**
     * System property that controls the default threshold time (in milliseconds) below which a profiled event
     * should not be reported.
     */
    public static final String MIN_TIME = "atlassian.profile.mintime";
    /**
     * System property that controls the default threshold time (in milliseconds) below which an entire stack of
     * profiled events should not be reported.
     */
    public static final String MIN_TOTAL_TIME = "atlassian.profile.mintotaltime";

    private ProfilingConstants() {
        throw new IllegalArgumentException("ProfilingConstants should not be instantiated");
    }
}
