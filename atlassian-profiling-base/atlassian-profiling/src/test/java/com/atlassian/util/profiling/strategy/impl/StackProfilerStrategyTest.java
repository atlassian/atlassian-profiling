package com.atlassian.util.profiling.strategy.impl;

import java.util.Arrays;
import java.util.concurrent.*;
import java.util.regex.Pattern;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.atlassian.util.profiling.ProfilerConfiguration;
import com.atlassian.util.profiling.Ticker;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StackProfilerStrategyTest {

    private ProfilerConfiguration configuration;

    @Captor
    private ArgumentCaptor<String> logCaptor;

    @Mock
    private Logger logger;

    private StackProfilerStrategy strategy;

    @Before
    public void setup() {
        when(logger.isDebugEnabled()).thenReturn(true);

        strategy = new StackProfilerStrategy();
        strategy.setLogger(logger);

        configuration = new ProfilerConfiguration();
        configuration.setEnabled(true);
        configuration.setMinFrameTime(0L, TimeUnit.SECONDS);
        configuration.setMinTraceTime(0L, TimeUnit.SECONDS);
        strategy.setConfiguration(configuration);
    }

    @Test
    public void testBasicUsage() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        try (Ticker ignored = strategy.start("top level")) {
            for (int i = 0; i < 2; ++i) {
                try (Ticker ignored2 = strategy.start("inner-" + i)) {
                    latch.await(100_000, TimeUnit.NANOSECONDS);
                    try (Ticker ignored3 = strategy.start("inner-" + i + "-inner")) {
                        latch.await(100_000, TimeUnit.NANOSECONDS);
                    }
                }
            }
        }

        verify(logger).debug(logCaptor.capture());
        String[] lines = logCaptor.getValue().split("\\n");
        assertEquals(5, lines.length);
        assertThat(lines[0], profilingFrame(0, "top level"));
        assertThat(lines[1], profilingFrame(1, "inner-0"));
        assertThat(lines[2], profilingFrame(2, "inner-0-inner"));
        assertThat(lines[3], profilingFrame(1, "inner-1"));
        assertThat(lines[4], profilingFrame(2, "inner-1-inner"));
    }

    @Test
    public void testFrameNameIsTruncated() {
        configuration.setMaxFrameNameLength(16);

        try (Ticker ignored = strategy.start("this is the outer frame that is a bit too long")) {
            strategy.start("this is the inner frame that is a bit too long").close();
        }

        verify(logger).debug(logCaptor.capture());
        String[] lines = logCaptor.getValue().split("\\n");
        assertEquals(2, lines.length);
        assertThat(lines[0], profilingFrame(0, "this is the oute..."));
        assertThat(lines[1], profilingFrame(1, "this is the inne..."));
    }

    @Test
    public void testMaxFramesPerTraceEnforced() {
        configuration.setMaxFramesPerTrace(4);

        try (Ticker ignored = strategy.start("outer")) {
            for (int i = 0; i < 10; ++i) {
                try (Ticker ignored2 = strategy.start("inner-" + i)) {
                    strategy.start("inner-" + i + "-inner").close();
                }
            }
        }

        verify(logger).debug(logCaptor.capture());
        String[] lines = logCaptor.getValue().split("\\n");
        assertEquals(4, lines.length);
        assertThat(lines[0], profilingFrame(0, "outer"));
        assertThat(lines[1], profilingFrame(1, "inner-0"));
        assertThat(lines[2], profilingFrame(2, "inner-0-inner"));
        assertThat(lines[3], profilingFrame(1, "inner-1"));
    }

    @Test
    public void testMemoryProfiling() {
        configuration.setMemoryProfilingEnabled(true);

        assertTrue(configuration.isMemoryProfilingEnabled());
        byte[][] memory = new byte[4][];
        try (Ticker ignored = strategy.start("outer")) {
            for (int i = 0; i < 4; ++i) {
                try (Ticker ignored2 = strategy.start("inner-" + i)) {
                    memory[i] = new byte[100 * 1024];
                    Arrays.fill(memory[i], (byte) 123);
                }
            }
        }

        verify(logger).debug(logCaptor.capture());
        String[] lines = logCaptor.getValue().split("\\n");
        assertEquals(5, lines.length);
        assertThat(lines[0], profilingFrameWithMemory(0, "outer"));
        assertThat(lines[1], profilingFrameWithMemory(1, "inner-0"));
        assertThat(lines[2], profilingFrameWithMemory(1, "inner-1"));
        assertThat(lines[3], profilingFrameWithMemory(1, "inner-2"));
        assertThat(lines[4], profilingFrameWithMemory(1, "inner-3"));
    }

    @Test
    public void testFrameMerging() throws InterruptedException {
        configuration.setMaxFramesPerTrace(10);

        try (Ticker ignored = strategy.start("outer")) {
            for (int i = 0; i < 10; ++i) {
                for (int j = 0; j < 5; ++j) {
                    Ticker inner = strategy.start("inner frame");
                    strategy.start("inner-inner-1").close();
                    try (Ticker ignored2 = strategy.start("inner-inner-2")) {
                        Thread.sleep(1L);
                    }
                    inner.close();
                }

                strategy.start("something different " + i).close();
            }
        }

        verify(logger).debug(logCaptor.capture());
        String[] lines = logCaptor.getValue().split("\\n");
        assertEquals(10, lines.length);
        assertThat(lines[0], profilingFrame(0, "outer"));
        assertThat(lines[1], profilingFrame(1, "inner frame", 5));
        assertThat(lines[2], profilingFrame(2, "inner-inner-1", 5));
        assertThat(lines[3], profilingFrame(2, "inner-inner-2", 5));
        assertThat(lines[4], profilingFrame(1, "something different 0"));
        assertThat(lines[5], profilingFrame(1, "inner frame"));
        assertThat(lines[6], profilingFrame(2, "inner-inner-1"));
        assertThat(lines[7], profilingFrame(2, "inner-inner-2"));
        assertThat(lines[8], profilingFrame(1, "inner frame"));
        assertThat(lines[9], profilingFrame(2, "inner-inner-1"));
    }

    @Test
    public void testInnerTickerClosedFromOtherThread() throws InterruptedException {
        Ticker outer = strategy.start("outer");
        Ticker inner = strategy.start("inner");

        Thread other = new Thread(inner::close, "closer-thread");
        other.start();
        // wait for the inner to be closed from the other thread
        other.join(10000L);

        outer.close();
        verify(logger).debug(logCaptor.capture());
        String[] lines = logCaptor.getValue().split("\\n");
        assertEquals(2, lines.length);
        assertThat(lines[0], profilingFrame(0, "outer"));
        assertThat(lines[1], profilingFrame(1, "inner"));
    }

    @Test(expected = NullPointerException.class)
    public void testNullFrameName() {
        //noinspection ConstantConditions
        strategy.start(null);
    }

    @Test
    public void testOnRequestEndTickerNotClosed() {
        strategy.start("outer");
        strategy.start("inner1").close();
        strategy.start("inner2");

        strategy.onRequestEnd();

        verify(logger).debug(logCaptor.capture());
        String[] lines = logCaptor.getValue().split("\\n");
        assertEquals(3, lines.length);
        assertThat(lines[0], profilingFrame(0, "outer (not closed)"));
        assertThat(lines[1], profilingFrame(1, "inner1"));
        assertThat(lines[2], profilingFrame(1, "inner2 (not closed)"));

        // verify that a next timer starts a new trace
        strategy.start("hello").close();

        verify(logger, times(2)).debug(logCaptor.capture());
        lines = logCaptor.getValue().split("\\n");
        assertEquals(1, lines.length);
        assertThat(lines[0], profilingFrame(0, "hello"));
    }

    @Test
    public void testOuterTickerClosedFromOtherThread() throws InterruptedException {
        Ticker outer = strategy.start("outer-1");

        Thread other = new Thread(outer::close, "closer-thread");
        other.start();
        // wait for the ticker to be closed from the other thread
        other.join(10000L);

        verify(logger).debug(logCaptor.capture());
        String[] lines = logCaptor.getValue().split("\\n");
        assertEquals(1, lines.length);
        assertThat(lines[0], profilingFrame(0, "outer-1"));

        // start a new ticker and verify that the old trace is not hanging around in the ThreadLocal
        strategy.start("outer-2").close();

        verify(logger, times(2)).debug(logCaptor.capture());
        lines = logCaptor.getValue().split("\\n");
        assertEquals(1, lines.length);
        assertThat(lines[0], profilingFrame(0, "outer-2"));
    }

    @Test
    public void testSuppressesFramesShorterThanMinFrameTime() throws InterruptedException {
        configuration.setMinFrameTime(10, TimeUnit.MILLISECONDS);

        try (Ticker ignored = strategy.start("outer")) {
            for (int i = 1; i < 50; ++i) {
                try (Ticker ignored2 = strategy.start("inner-" + i)) {
                    Thread.sleep(1L);
                }
            }
        }

        // verify that only the outer frame has been logged
        verify(logger).debug(logCaptor.capture());
        String[] lines = logCaptor.getValue().split("\\n");
        assertEquals(1, lines.length);
        assertThat(lines[0], profilingFrame(0, "outer"));
    }

    @Test
    public void testSuppressesRootFrameShorterThanMinFrameTime() throws InterruptedException {
        configuration.setMinFrameTime(10, TimeUnit.MILLISECONDS);

        try (Ticker ignored = strategy.start("outer")) {
            Thread.sleep(1L);
        }

        // verify that only nothing has been logged
        verify(logger, never()).debug(anyString());
    }

    @Test
    public void testSuppressesTraceIfShorterThanTotalTime() {
        configuration.setMinTraceTime(1, TimeUnit.SECONDS);
        strategy.start("top level").close();

        verify(logger, never()).debug(anyString());
    }

    @Test
    public void testTickerNotClosed() {
        Ticker outer = strategy.start("outer");
        Ticker inner = strategy.start("inner");
        outer.close();

        verify(logger).debug(logCaptor.capture());
        String[] lines = logCaptor.getValue().split("\\n");
        assertEquals(2, lines.length);
        assertThat(lines[0], profilingFrame(0, "outer"));
        assertThat(lines[1], profilingFrame(1, "inner (not closed)"));

        // close inner after the fact
        inner.close();

        // verify that no additional logging happens
        verify(logger).debug(anyString());
    }

    @Test
    public void testWithProfilingEnabled() throws ExecutionException, InterruptedException {
        configuration.setEnabled(false);
        ExecutorService executor = Executors.newSingleThreadExecutor();

        try {
            assertFalse(configuration.isEnabled());
            assertFalse(executor.submit(configuration::isEnabled).get());
            try (Ticker ignored = configuration.enableForThread()) {
                assertTrue(configuration.isEnabled());
                // verify that profiling is not enabled on other threads
                try {
                    assertFalse(executor.submit(configuration::isEnabled).get());
                } catch (InterruptedException | ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
            assertFalse(configuration.isEnabled());
            assertFalse(executor.submit(configuration::isEnabled).get());
        } finally {
            executor.shutdown();
        }
    }

    private static Matcher<String> profilingFrame(int depth, String frameName) {
        return new FrameMatcher(depth, frameName, false, 1);
    }

    private static Matcher<String> profilingFrame(int depth, String frameName, int count) {
        return new FrameMatcher(depth, frameName, false, count);
    }

    private static Matcher<String> profilingFrameWithMemory(int depth, String frameName) {
        return new FrameMatcher(depth, frameName, true, 1);
    }

    private static class FrameMatcher extends BaseMatcher<String> {
        private static final Pattern pattern = Pattern.compile(
                "(\\s*)\\[(\\d+\\.\\d)ms]( \\[count: \\d+, avg: \\d+.\\dms])?( \\[(-?\\d)+KB used] \\[(\\d)+KB free])? - (.*)");

        private final int count;
        private final String frameName;
        private final boolean hasMemory;
        private final String indent;

        private FrameMatcher(int depth, String frameName, boolean hasMemory, int count) {
            this.count = count;
            this.frameName = frameName;
            this.hasMemory = hasMemory;
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < depth; ++i) {
                builder.append(' ');
            }
            indent = builder.toString();
        }

        @Override
        public boolean matches(Object item) {
            if (item instanceof String) {
                java.util.regex.Matcher matcher = pattern.matcher((String) item);
                return matcher.matches()
                        && matcher.group(1).equals(indent)
                        && matcher.group(7).equals(frameName)
                        && hasMemory == (matcher.group(5) != null)
                        && (count > 1) == (matcher.group(3) != null);
            }
            return false;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText(indent).appendText("[<time>ms]");
            if (count > 1) {
                description.appendText(" [count: " + count + ", avg: <time>ms]");
            }
            if (hasMemory) {
                description.appendText(" [<num>KB used] [<num>KB free]");
            }
            description.appendText(" - ").appendText(frameName);
        }
    }
}
