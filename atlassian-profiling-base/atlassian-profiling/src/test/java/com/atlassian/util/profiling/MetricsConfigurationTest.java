package com.atlassian.util.profiling;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import static com.atlassian.util.profiling.ProfilingConstants.OPTIONAL_TAG_PROPERTY_PREFIX;

public class MetricsConfigurationTest {

    @Test
    public void isOptionalTagRequired_returnsTrue_whenOptionalTagIsSet() {
        final String metricName = "condition";
        final String tag = "url";
        final MetricsConfiguration metricsConfiguration = initialiseWithSystemProperty(metricName, tag);

        assertTrue(metricsConfiguration.isOptionalTagEnabled(metricName, tag));
    }

    @Test
    public void isOptionalTagRequired_returnsTrue_whenMultipleOptionalTagAreSet() {
        final String metricName = "condition";
        final String tag = "url, location ,,,somethingElse";
        final MetricsConfiguration metricsConfiguration = initialiseWithSystemProperty(metricName, tag);

        assertTrue(metricsConfiguration.isOptionalTagEnabled(metricName, "url"));
        assertTrue(metricsConfiguration.isOptionalTagEnabled(metricName, "location"));
        assertTrue(metricsConfiguration.isOptionalTagEnabled(metricName, "somethingElse"));
    }

    @Test
    public void isOptionalTagRequired_returnsFalse_whenMetricIsSetWithDifferentOptionalTags() {
        final String metricName = "fragment";
        final String tag = "url,location";
        final MetricsConfiguration metricsConfiguration = initialiseWithSystemProperty(metricName, tag);

        assertFalse(metricsConfiguration.isOptionalTagEnabled(metricName, "urlz"));
        assertFalse(metricsConfiguration.isOptionalTagEnabled(metricName, "locationzzz"));
    }

    @Test
    public void isOptionalTagRequired_returnsFalse_whenOptionalTagIsSetForDifferentMetric() {
        final String metricName = "fragment";
        final String tag = "url,location";
        final MetricsConfiguration metricsConfiguration = initialiseWithSystemProperty(metricName, tag);

        assertFalse(metricsConfiguration.isOptionalTagEnabled("condition", "url"));
    }

    @Test
    public void isOptionalTagRequired_returnsFalse_whenMetricIsIncorrectlySet() {
        final String metricName = "fragment";

        MetricsConfiguration metricsConfiguration = initialiseWithSystemProperty(metricName, "");
        assertFalse(metricsConfiguration.isOptionalTagEnabled(metricName, ""));

        metricsConfiguration = initialiseWithSystemProperty(metricName, ",,,,,");
        assertFalse(metricsConfiguration.isOptionalTagEnabled(metricName, ""));

        metricsConfiguration = initialiseWithSystemProperty(metricName, "url|url[url]url");
        assertFalse(metricsConfiguration.isOptionalTagEnabled(metricName, "url"));

        assertFalse(metricsConfiguration.isOptionalTagEnabled(metricName, null));
    }

    private MetricsConfiguration initialiseWithSystemProperty(String metricName, String val) {
        final String sysPropKey = OPTIONAL_TAG_PROPERTY_PREFIX + metricName;
        System.setProperty(sysPropKey, val);

        MetricsConfiguration metricsConfiguration = new MetricsConfiguration();

        // clear up the sys property so it doesn't affect other tests
        System.clearProperty(sysPropKey);

        return metricsConfiguration;
    }
}
