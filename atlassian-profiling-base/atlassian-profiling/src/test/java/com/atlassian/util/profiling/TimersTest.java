package com.atlassian.util.profiling;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.util.profiling.strategy.MetricStrategy;
import com.atlassian.util.profiling.strategy.ProfilerStrategy;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import static com.atlassian.util.profiling.MetricKey.metricKey;

public class TimersTest {

    private boolean metricsEnabled;
    private boolean profilingEnabled;
    private List<MetricStrategy> registeredMetricStrategies;
    private List<ProfilerStrategy> registeredProfilerStrategies;

    @Before
    public void setup() {
        // enabled profiling and metrics
        ProfilerConfiguration profilingConfig = Timers.getConfiguration();
        profilingEnabled = profilingConfig.isEnabled();
        profilingConfig.setEnabled(true);
        MetricsConfiguration metricsConfiguration = Metrics.getConfiguration();
        metricsEnabled = metricsConfiguration.isEnabled();
        metricsConfiguration.setEnabled(true);

        registeredMetricStrategies = new ArrayList<>();
        registeredProfilerStrategies = new ArrayList<>();
    }

    @After
    public void tearDown() {
        Metrics.getConfiguration().setEnabled(metricsEnabled);
        Timers.getConfiguration().setEnabled(profilingEnabled);

        registeredMetricStrategies.forEach(StrategiesRegistry::removeMetricStrategy);
        registeredProfilerStrategies.forEach(StrategiesRegistry::removeProfilerStrategy);
    }

    @Test
    public void testCannotRemoveDefaultStrategy() {
        assertFalse(StrategiesRegistry.removeProfilerStrategy(StrategiesRegistry.getDefaultProfilerStrategy()));
    }

    @Test
    public void testCallParameters() {
        ProfilerStrategy strategy = registerMockProfilerStrategy();

        Timer timer = Timers.timer("timerName");

        timer.start(null);
        verify(strategy).start("timerName");

        timer.start(1, 2, 3);
        verify(strategy).start("timerName(1, 2, 3)");

        timer.start(1, "string", null, 3, null);
        verify(strategy).start("timerName(1, string, 3)");

        Timers.timer("timer2").start(null, "", null, null);
        verify(strategy).start("timer2");
    }

    @Test
    public void testConcatEmpty() {
        Timer timer = Timers.concat();

        assertNotNull(timer);
        assertEquals(Ticker.NO_OP, timer.start());
    }

    @Test
    public void testConcatSingleTimer() {
        Timer mockTimer = mock(Timer.class);
        Ticker mockTicker = mock(Ticker.class);
        when(mockTimer.start(any(Object.class))).thenReturn(mockTicker);

        Timer concat = Timers.concat(mockTimer);

        assertNotNull(concat);
        Ticker ticker = concat.start(123);

        verify(mockTimer).start(123);
        verifyNoMoreInteractions(mockTimer, mockTicker);

        ticker.close();
        verify(mockTicker).close();
    }

    @Test
    public void testConcatMultipleTimers() {
        Timer mockTimer1 = mock(Timer.class);
        Ticker mockTicker1 = mock(Ticker.class);
        when(mockTimer1.start(any(Object.class))).thenReturn(mockTicker1);
        Timer mockTimer2 = mock(Timer.class);
        Ticker mockTicker2 = mock(Ticker.class);
        when(mockTimer2.start(any(Object.class))).thenReturn(mockTicker2);

        Timer concat = Timers.concat(mockTimer1, mockTimer2);
        Object param = "some string";
        assertNotNull(concat);
        Ticker ticker = concat.start(param);

        verify(mockTimer1).start(param);
        verify(mockTimer2).start(param);
        verifyNoMoreInteractions(mockTimer1, mockTicker1, mockTimer2, mockTicker2);

        ticker.close();
        verify(mockTicker1).close();
        verify(mockTicker2).close();
    }

    @Test
    public void testHistogramRespectsMetricsConfiguration() {
        Histogram histogram = Metrics.histogram("hist");
        MetricStrategy strategy = registerMockMetricStrategy();

        // verify that no interactions happen when metrics are disabled
        Metrics.getConfiguration().setEnabled(false);
        histogram.update(1L);

        verify(strategy, never()).updateHistogram(anyString(), anyLong());

        // verify that interactions happen when metrics are enabled
        Metrics.getConfiguration().setEnabled(true);
        histogram.update(1L);

        verify(strategy).updateHistogram(metricKey("hist"), 1L);
    }

    @Test
    public void testMetric() {
        MetricTimer metric = Metrics.timer("my-metric");
        assertNotNull(metric);

        Ticker ticker = metric.start();
        assertNotNull(ticker);

        // register a MetricStrategy and verify that it is called
        TestableStrategy metricStrategy = registerNewMetricStrategy();
        TestableStrategy profilerStrategy = registerNewProfilerStrategy();

        metric.start().close();
        assertThat(metricStrategy.startedTimers, hasItems("my-metric"));
        assertThat(metricStrategy.stoppedTimers, hasItems("my-metric"));
        assertTrue(profilerStrategy.startedTimers.isEmpty());
    }

    @Test
    public void testMetricUpdate() {
        MetricTimer metric = Metrics.timer("my-metric");
        assertNotNull(metric);

        TestableStrategy metricStrategy = registerNewMetricStrategy();

        metric.update(12L, TimeUnit.MILLISECONDS);
        metric.update(2L, TimeUnit.SECONDS);

        assertThat(metricStrategy.updatedMetricTimers, hasItems("my-metric", "my-metric"));
        assertEquals(
                TimeUnit.SECONDS.toNanos(2L) + TimeUnit.MILLISECONDS.toNanos(12L),
                metricStrategy.totalUpdatedTimeNanos);
    }

    @Test
    public void testMetricHandlesThrowingStrategy() {
        MetricStrategy strategy = registerMockMetricStrategy();
        when(strategy.startTimer(anyString())).thenThrow(new RuntimeException("testing throwing strategy"));
        TestableStrategy metricStrategy = registerNewMetricStrategy();

        Metrics.timer("my-metric").start().close();

        // verify that the non-throwing metricStrategy has been been called, even though the preceding strategy
        // threw an exception
        assertThat(metricStrategy.startedTimers, hasItems("my-metric"));
        assertThat(metricStrategy.stoppedTimers, hasItems("my-metric"));
    }

    @Test
    public void testOnRequestEnded() {
        MetricStrategy metricStrategy = registerMockMetricStrategy();
        ProfilerStrategy profilerStrategy = registerMockProfilerStrategy();

        Timers.onRequestEnd();

        verify(metricStrategy).onRequestEnd();
        verify(profilerStrategy).onRequestEnd();
    }

    @Test
    public void testProfilingEnabledRespected() {
        MetricStrategy metricStrategy = registerMockMetricStrategy();
        ProfilerStrategy profilerStrategy = registerMockProfilerStrategy();

        MetricTimer metric = Metrics.timer("metric");
        Timer timer = Timers.timer("timer");
        Timer timerWithMetric = Timers.timerWithMetric("timerWithMetric");

        // disable both profiling and metrics
        Metrics.getConfiguration().setEnabled(false);
        Timers.getConfiguration().setEnabled(false);

        metric.start().close();
        metric.update(1L, TimeUnit.SECONDS);
        timer.start().close();
        timerWithMetric.start().close();

        verify(metricStrategy, never()).startTimer(anyString());
        verify(metricStrategy, never()).updateTimer(anyString(), anyLong(), any());
        verify(profilerStrategy, never()).start(anyString());

        // enable metrics only
        Metrics.getConfiguration().setEnabled(true);

        metric.start().close();
        metric.update(1L, TimeUnit.SECONDS);
        timer.start().close();
        timerWithMetric.start().close();

        // verify that only the metricStrategy is triggered
        verify(metricStrategy, times(2)).startTimer(any(MetricKey.class));
        verify(metricStrategy, times(1)).updateTimer(any(MetricKey.class), any(Duration.class));
        verify(profilerStrategy, never()).start(anyString());

        // enable profiling only
        Metrics.getConfiguration().setEnabled(false);
        Timers.getConfiguration().setEnabled(true);

        metric.start().close();
        metric.update(1L, TimeUnit.SECONDS);
        timer.start().close();
        timerWithMetric.start().close();

        // verify that only the profilingStrategy is triggered
        verify(metricStrategy, times(2)).startTimer(any(MetricKey.class));
        verify(metricStrategy, times(1)).updateTimer(any(MetricKey.class), any(Duration.class));
        verify(profilerStrategy, times(2)).start(anyString());

        // enable both
        Metrics.getConfiguration().setEnabled(true);
        Timers.getConfiguration().setEnabled(true);

        metric.start().close();
        metric.update(1L, TimeUnit.SECONDS);
        timer.start().close();
        timerWithMetric.start().close();

        // verify that both are triggered
        verify(metricStrategy, times(4)).startTimer(any(MetricKey.class));
        verify(metricStrategy, times(2)).updateTimer(any(MetricKey.class), any(Duration.class));
        verify(profilerStrategy, times(4)).start(anyString());
    }

    @Test
    public void testStart() {
        ProfilerStrategy profilerStrategy = registerMockProfilerStrategy();

        // disable the profiling and verify that no interactions happen with the profilerStrategy
        Timers.getConfiguration().setEnabled(false);
        Timers.start("timer").close();
        verify(profilerStrategy, never()).start(anyString());

        // enable profiling and verify that profilingStrategy is called
        Timers.getConfiguration().setEnabled(true);
        Timers.start("timer").close();
        verify(profilerStrategy).start("timer");
    }

    @Test
    public void testStartWitMetric() {
        ProfilerStrategy profilerStrategy = registerMockProfilerStrategy();
        MetricStrategy metricStrategy = registerMockMetricStrategy();

        // disable metrics & profiling and verify that no interactions happen with either strategy
        Timers.getConfiguration().setEnabled(false);
        Metrics.getConfiguration().setEnabled(false);
        Timers.startWithMetric("timer1").close();
        Timers.startWithMetric("timer2", "metricTimer").close();
        verify(profilerStrategy, never()).start(anyString());
        verify(metricStrategy, never()).startTimer(anyString());

        // enable metrics only and verify that the metrics strategy is called
        Metrics.getConfiguration().setEnabled(true);
        Timers.startWithMetric("timer1").close();
        Timers.startWithMetric("timer2", "metricTimer").close();
        verifyNoMoreInteractions(profilerStrategy);
        verify(metricStrategy).startTimer(metricKey("timer1"));
        verify(metricStrategy).startTimer(metricKey("metricTimer"));

        // enable profiling only and verify that the profiling strategy is called
        Metrics.getConfiguration().setEnabled(false);
        Timers.getConfiguration().setEnabled(true);
        Timers.startWithMetric("timer1").close();
        Timers.startWithMetric("timer2", "metricTimer").close();
        verify(profilerStrategy).start("timer1");
        verify(profilerStrategy).start("timer2");
        verifyNoMoreInteractions(metricStrategy);

        // enable both and verify that both strategies are called
        Metrics.getConfiguration().setEnabled(true);
        Timers.getConfiguration().setEnabled(true);
        Timers.startWithMetric("timer3").close();
        Timers.startWithMetric("timer4", "metricTimer4").close();
        verify(profilerStrategy).start("timer3");
        verify(profilerStrategy).start("timer4");
        verify(metricStrategy).startTimer(metricKey("timer3"));
        verify(metricStrategy).startTimer(metricKey("metricTimer4"));
    }

    @Test
    public void testTimer() {
        Timer timer = Timers.timer("my-timer");
        assertNotNull(timer);

        Ticker ticker = timer.start();
        assertNotNull(ticker);

        // register a ProfilerStrategy and verify that it is called
        TestableStrategy metricStrategy = registerNewMetricStrategy();
        TestableStrategy profilerStrategy = registerNewProfilerStrategy();

        timer.start().close();
        assertThat(profilerStrategy.startedTimers, hasItems("my-timer"));
        assertThat(profilerStrategy.stoppedTimers, hasItems("my-timer"));
        assertTrue(metricStrategy.startedTimers.isEmpty());

        timer.start("arg1", "arg2").close();
        assertThat(profilerStrategy.startedTimers, hasItems("my-timer", "my-timer(arg1, arg2)"));
        assertThat(profilerStrategy.stoppedTimers, hasItems("my-timer", "my-timer(arg1, arg2)"));
        assertTrue(metricStrategy.startedTimers.isEmpty());
    }

    @Test
    public void testTimerHandlesThrowingStrategy() {
        ProfilerStrategy strategy = registerMockProfilerStrategy();
        when(strategy.start(anyString())).thenThrow(new RuntimeException("testing throwing strategy"));
        TestableStrategy profilerStrategy = registerNewProfilerStrategy();

        Timers.timer("my-timer").start().close();

        // verify that the non-throwing profilerStrategy has been been called, even though the preceding strategy
        // threw an exception
        assertThat(profilerStrategy.startedTimers, hasItems("my-timer"));
        assertThat(profilerStrategy.stoppedTimers, hasItems("my-timer"));
    }

    @Test
    public void testTimerWithMetric() {
        Timer timer = Timers.timerWithMetric("my-timer");
        assertNotNull(timer);

        Ticker ticker = timer.start();
        assertNotNull(ticker);

        // register a ProfilerStrategy and MetricStrategy and verify that they are called
        TestableStrategy metricStrategy = registerNewMetricStrategy();
        TestableStrategy profilerStrategy = registerNewProfilerStrategy();

        timer.start().close();
        assertThat(metricStrategy.startedTimers, hasItems("my-timer"));
        assertThat(metricStrategy.stoppedTimers, hasItems("my-timer"));
        assertThat(profilerStrategy.startedTimers, hasItems("my-timer"));
        assertThat(profilerStrategy.stoppedTimers, hasItems("my-timer"));
    }

    @Test
    public void testTimerWithMetricWithDifferentName() {
        Timer timer = Timers.timerWithMetric("my-timer", "my-metric");
        assertNotNull(timer);

        Ticker ticker = timer.start();
        assertNotNull(ticker);

        // register a ProfilerStrategy and MetricStrategy and verify that they are called
        TestableStrategy metricStrategy = registerNewMetricStrategy();
        TestableStrategy profilerStrategy = registerNewProfilerStrategy();

        timer.start().close();
        assertThat(metricStrategy.startedTimers, hasItems("my-metric"));
        assertThat(metricStrategy.stoppedTimers, hasItems("my-metric"));
        assertThat(profilerStrategy.startedTimers, hasItems("my-timer"));
        assertThat(profilerStrategy.stoppedTimers, hasItems("my-timer"));
    }

    private MetricStrategy registerMockMetricStrategy() {
        MetricStrategy strategy = mock(MetricStrategy.class);
        registeredMetricStrategies.add(strategy);
        StrategiesRegistry.addMetricStrategy(strategy);
        verify(strategy).setConfiguration(any());
        return strategy;
    }

    private ProfilerStrategy registerMockProfilerStrategy() {
        ProfilerStrategy strategy = mock(ProfilerStrategy.class);
        registeredProfilerStrategies.add(strategy);
        StrategiesRegistry.addProfilerStrategy(strategy);
        verify(strategy).setConfiguration(any());
        return strategy;
    }

    private TestableStrategy registerNewMetricStrategy() {
        TestableStrategy strategy = new TestableStrategy();
        registeredMetricStrategies.add(strategy);
        StrategiesRegistry.addMetricStrategy(strategy);
        return strategy;
    }

    private TestableStrategy registerNewProfilerStrategy() {
        TestableStrategy strategy = new TestableStrategy();
        registeredProfilerStrategies.add(strategy);
        StrategiesRegistry.addProfilerStrategy(strategy);
        return strategy;
    }

    private static class TestableStrategy implements MetricStrategy, ProfilerStrategy {

        private final Map<String, List<Long>> histogramValues = new HashMap<>();
        private final List<String> startedTimers = new ArrayList<>();
        private final List<String> stoppedTimers = new ArrayList<>();
        private final List<String> updatedMetricTimers = new ArrayList<>();
        private long totalUpdatedTimeNanos;

        @Override
        public void onRequestEnd() {}

        @Override
        public void setConfiguration(@Nonnull MetricsConfiguration configuration) {}

        @Override
        public void setConfiguration(@Nonnull ProfilerConfiguration configuration) {}

        @Nonnull
        @Override
        public Ticker start(@Nonnull String metricName) {
            startedTimers.add(metricName);
            return () -> stoppedTimers.add(metricName);
        }

        @Nonnull
        @Override
        public Ticker startTimer(@Nonnull String metricName) {
            startedTimers.add(metricName);
            return () -> stoppedTimers.add(metricName);
        }

        @Override
        public void updateHistogram(@Nonnull String metricName, long value) {
            histogramValues
                    .computeIfAbsent(metricName, name -> new ArrayList<>())
                    .add(value);
        }

        @Override
        public void updateTimer(@Nonnull String metricName, long time, @Nonnull TimeUnit timeUnit) {
            updatedMetricTimers.add(metricName);

            totalUpdatedTimeNanos += timeUnit.toNanos(time);
        }
    }
}
