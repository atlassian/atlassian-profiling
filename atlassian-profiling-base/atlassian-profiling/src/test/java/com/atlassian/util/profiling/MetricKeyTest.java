package com.atlassian.util.profiling;

import java.util.function.Supplier;

import org.junit.Test;

import static java.util.Collections.emptySet;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import static com.atlassian.util.profiling.MetricKey.metricKey;

public class MetricKeyTest {

    @Test
    public void testTagFormatting() {
        final MetricKey metricKey =
                metricKey("mymetric", MetricTag.of("key1", "value1"), MetricTag.of("key2", "value2"));

        assertThat(metricKey.toString(), equalTo("mymetric[key1=value1,key2=value2]"));
    }

    @Test
    public void testMetricsAreEqual() {
        final MetricKey metricKey1 =
                metricKey("mymetric", MetricTag.of("key1", "value1"), MetricTag.of("key2", "value2"));

        final MetricKey metricKey2 =
                metricKey("mymetric", MetricTag.of("key2", "value2"), MetricTag.of("key1", "value1"));

        assertEquals(metricKey1, metricKey2);
    }

    @Test
    public void testNoTagsFormatting() {
        final MetricKey metricKey = metricKey("mymetric", emptySet());

        assertThat(metricKey.toString(), equalTo("mymetric"));
    }

    @Test
    public void testMetricTagContextNotIncluded() {
        // Verifying that MetricTagContexts are not included in the current metrics until we explicitly select
        // them in the future.
        assertMetricTagContextNotIncluded(() -> metricKey("mymetric", emptySet()));
        assertMetricTagContextNotIncluded(() -> metricKey("mymetric"));
        assertMetricTagContextNotIncluded(() -> metricKey("mymetric", new MetricTag.RequiredMetricTag[] {}));
    }

    private void assertMetricTagContextNotIncluded(Supplier<MetricKey> fnToTest) {
        try (MetricTagContext.Closeable a = MetricTagContext.put(MetricTag.optionalOf("contextTagKey", 1))) {
            final MetricKey metricKey = fnToTest.get();
            assertThat(metricKey.getTags(), empty());
        }
    }
}
