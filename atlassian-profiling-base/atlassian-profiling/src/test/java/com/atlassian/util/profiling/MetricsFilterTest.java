package com.atlassian.util.profiling;

import java.time.Duration;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.util.profiling.strategy.MetricStrategy;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import static com.atlassian.util.profiling.MetricKey.metricKey;
import static com.atlassian.util.profiling.Metrics.metric;
import static com.atlassian.util.profiling.MetricsFilter.ACCEPT_ALL;
import static com.atlassian.util.profiling.MetricsFilter.DENY_ALL;
import static com.atlassian.util.profiling.MetricsFilter.deny;

public class MetricsFilterTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private static final String METRIC_NAME = "some.metric";
    private static final MetricKey METRIC_KEY = metricKey(METRIC_NAME);

    @Mock
    private MetricStrategy metricStrategy;

    @Before
    public void setup() {
        StrategiesRegistry.addMetricStrategy(metricStrategy);
        Metrics.getConfiguration().setFilter(deny(METRIC_NAME));
    }

    @After
    public void teardown() {
        StrategiesRegistry.removeMetricStrategy(metricStrategy);
        Metrics.getConfiguration().setFilter(ACCEPT_ALL);
    }

    @Test
    public void testMetricsFilterIsAppliedOnLongRunningTimer() {
        metric(METRIC_NAME).startLongRunningTimer();
        verify(metricStrategy, never()).startLongRunningTimer(METRIC_KEY);
    }

    @Test
    public void testMetricsFilterIsAppliedOnTimer() {
        metric(METRIC_NAME).startTimer();
        metric(METRIC_NAME).timer().update(Duration.ZERO);
        verify(metricStrategy, never()).startTimer(METRIC_KEY);
    }

    @Test
    public void testMetricsFilterIsAppliedOnCounter() {
        long deltaValue = 1L;
        metric(METRIC_NAME).incrementCounter(deltaValue);
        verify(metricStrategy, never()).incrementCounter(METRIC_KEY, deltaValue);
    }

    @Test
    public void testMetricsFilterIsAppliedOnGauge() {
        long deltaValue = 1L;
        metric(METRIC_NAME).incrementGauge(deltaValue);
        verify(metricStrategy, never()).incrementGauge(METRIC_KEY, deltaValue);
    }

    @Test
    public void testMetricsFilterIsAppliedOnHistogram() {
        long deltaValue = 1L;
        metric(METRIC_NAME).histogram().update(deltaValue);
        verify(metricStrategy, never()).updateHistogram(METRIC_KEY, deltaValue);
    }

    @Test
    public void testAcceptAll() {
        assertTrue(ACCEPT_ALL.accepts("foo"));
        assertTrue(ACCEPT_ALL.accepts("bar"));
    }

    @Test
    public void testDeniesAll() {
        assertFalse(DENY_ALL.accepts("foo"));
        assertFalse(DENY_ALL.accepts("bar"));
    }

    @Test
    public void testDenyNames() {
        MetricsFilter denyFoo = deny("foo");

        assertFalse(denyFoo.accepts("foo"));
        assertTrue(denyFoo.accepts("bar"));
    }
}
