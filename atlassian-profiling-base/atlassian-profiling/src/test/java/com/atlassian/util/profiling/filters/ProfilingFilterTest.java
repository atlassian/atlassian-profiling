package com.atlassian.util.profiling.filters;

import javax.servlet.FilterConfig;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import static com.atlassian.util.profiling.Timers.getConfiguration;

/**
 * @deprecated in 3.0 for removal in 5.0
 */
@Deprecated
@RunWith(MockitoJUnitRunner.class)
public class ProfilingFilterTest {
    @Mock
    private StatusUpdateStrategy statusUpdateStrategy;

    @Mock
    private FilterConfig filterConfig;

    private ProfilingFilter profilingFilter;

    @Before
    public void setUp() {
        when(filterConfig.getFilterName()).thenReturn("profiling");

        profilingFilter = new ProfilingFilter(statusUpdateStrategy);
    }

    @After
    public void tearDown() {
        getConfiguration().setEnabled(false);
    }

    @Test
    public void testAutoStartTrue() {
        when(filterConfig.getInitParameter(ProfilingFilter.AUTOSTART_PARAM)).thenReturn("true");
        profilingFilter.init(filterConfig);
        assertTrue(getConfiguration().isEnabled());
    }

    @Test
    public void testAutoStartFalse() {
        when(filterConfig.getInitParameter(ProfilingFilter.AUTOSTART_PARAM)).thenReturn("false");
        profilingFilter.init(filterConfig);
        assertFalse(getConfiguration().isEnabled());
    }

    @Test
    public void testAutoStartInvalid() {
        when(filterConfig.getInitParameter(ProfilingFilter.AUTOSTART_PARAM)).thenReturn("invalid");
        profilingFilter.init(filterConfig);
        assertFalse(getConfiguration().isEnabled());
    }
}
