package com.atlassian.util.profiling;

import org.junit.Test;

import com.atlassian.util.profiling.micrometer.analytics.AnalyticsMeterRegistry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import static com.atlassian.util.profiling.MetricTag.UNDEFINED_TAG_VALUE;

public class MetricTagTest {
    @Test
    public void testCreatingMetricTag_withANullValue_shouldConvertTheValueToAString() {
        MetricTag metricTag = MetricTag.of("key", null);

        assertEquals(metricTag.getValue(), UNDEFINED_TAG_VALUE);
    }

    @Test
    public void testEquals_shouldBeFalseForDifferentTypes() {
        final String commonTagKey = "key";
        final String commonTagValue = "value";

        MetricTag metricTag = MetricTag.of(commonTagKey, commonTagValue);
        MetricTag.OptionalMetricTag optionalMetricTag = MetricTag.optionalOf(commonTagKey, commonTagValue);

        assertNotEquals(metricTag, optionalMetricTag);
    }

    @Test
    public void shouldUseCorrectTagKeyForAnalytics() {
        assertEquals(AnalyticsMeterRegistry.SEND_ANALYTICS_TAG, MetricTag.SEND_ANALYTICS.getKey());
    }
}
