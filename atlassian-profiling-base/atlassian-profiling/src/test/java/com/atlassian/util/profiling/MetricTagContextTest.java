package com.atlassian.util.profiling;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.junit.Test;

import com.atlassian.profiling.metrics.api.tags.OptionalTag;
import com.atlassian.util.profiling.MetricTagContext.Closeable;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertThat;

import static com.atlassian.util.profiling.MetricTag.optionalOf;

public class MetricTagContextTest {

    @Test
    public void byDefault_shouldBeEmpty() {
        assertThat(MetricKey.metricKey("default").getTags(), empty());
    }

    @Test
    public void testNestedContext_singleThreaded() {
        try (Closeable a = MetricTagContext.put(optionalOf("a", 1))) {
            try (Closeable b = MetricTagContext.put(optionalOf("b", 2))) {
                assertThat(MetricTagContext.getAll(), contains(optionalOf("a", 1), optionalOf("b", 2)));
            }

            try (Closeable b = MetricTagContext.put(optionalOf("c", 3))) {
                assertThat(MetricTagContext.getAll(), contains(optionalOf("a", 1), optionalOf("c", 3)));
            }
        }
    }

    @Test
    public void testNestedContext_multiThreaded() throws InterruptedException {
        Map<String, Set<MetricTag.OptionalMetricTag>> results = new ConcurrentHashMap<>();

        try (Closeable a = MetricTagContext.put(optionalOf("a", 1))) {
            List<Thread> threads = asList(
                    new Thread(() -> {
                        try (Closeable b = MetricTagContext.put(optionalOf("b", 2))) {
                            results.put("ab", MetricTagContext.getAll());
                        }
                    }),
                    new Thread(() -> {
                        try (Closeable c = MetricTagContext.put(optionalOf("c", 3))) {
                            results.put("ac", MetricTagContext.getAll());
                        }
                    }));

            for (Thread t : threads) t.start();
            for (Thread t : threads) t.join();
        }

        assertThat(results.get("ab"), contains(optionalOf("a", 1), optionalOf("b", 2)));
        assertThat(results.get("ac"), contains(optionalOf("a", 1), optionalOf("c", 3)));
    }

    /**
     * This should only accept {@link MetricTag.OptionalMetricTag}s to avoid over collection of tags in two ways:
     * <ul>
     * <li>Re-used threads</li>
     * <li>Lack of awareness that a {@link MetricTag} is created further up the stack</li>
     * </ul>
     * which would lead to bad things:
     * <ul>
     * <li>Potentially, bad data</li>
     * <li>Generating too many metrics, thus consuming too much memory</li>
     * </ul>
     */
    @Test
    public void testMetricTagContext_shouldOnlyAcceptOptionalMetricTags() {
        List<Class<?>> metricTagParameterTypes = Arrays.stream(MetricTagContext.class.getDeclaredMethods())
                .filter(method -> !method.toString().contains("private"))
                .map(Method::getParameterTypes)
                .flatMap(Arrays::stream)
                .collect(Collectors.toList());

        assertThat(metricTagParameterTypes, hasItems(OptionalTag[].class, MetricTag.OptionalMetricTag.class));
    }
}
