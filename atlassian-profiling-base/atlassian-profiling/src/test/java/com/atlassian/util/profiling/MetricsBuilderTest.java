package com.atlassian.util.profiling;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.plugin.util.PluginKeyStack;
import com.atlassian.util.profiling.strategy.MetricStrategy;

import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import static com.atlassian.util.profiling.MetricTag.FROM_PLUGIN_KEY_TAG_KEY;
import static com.atlassian.util.profiling.MetricTag.INVOKER_PLUGIN_KEY_TAG_KEY;
import static com.atlassian.util.profiling.ProfilingConstants.OPTIONAL_TAG_PROPERTY_PREFIX;

public class MetricsBuilderTest {

    private static final String OPTIONAL_TAG_PROPERTY_KEY = OPTIONAL_TAG_PROPERTY_PREFIX + "propWithMultipleValues";

    @Mock
    private static MetricStrategy metricStrategy;

    @Rule
    public MockitoRule initRule = MockitoJUnit.rule();

    @Captor
    private ArgumentCaptor<MetricKey> metricCaptor;

    @BeforeClass
    public static void setupSuite() {
        System.setProperty(OPTIONAL_TAG_PROPERTY_KEY, "url,location,trace");
        Metrics.getConfiguration().reloadConfigs();
        Metrics.getConfiguration().setEnabled(true);
    }

    @Before
    public void setup() {
        StrategiesRegistry.addMetricStrategy(metricStrategy);
    }

    @AfterClass
    public static void afterSuite() {
        System.clearProperty(OPTIONAL_TAG_PROPERTY_KEY);
        Metrics.getConfiguration().reloadConfigs();
    }

    @After
    public void teardown() {
        StrategiesRegistry.removeMetricStrategy(metricStrategy);
    }

    @Test
    public void testMetricName() {
        final String metricName = "metricName";

        Metrics.metric(metricName) // method under test
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getMetricName(), equalTo(metricName));
    }

    @Test
    public void testAddingASingleStringTag() {
        final String tagKey = "theTagKey";
        final String tagValue = "theTagValue";

        Metrics.metric("ignored")
                .tag(tagKey, tagValue) // method under test
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getTags(), hasItem(MetricTag.of(tagKey, tagValue)));
    }

    @Test
    public void testAddingASingleBooleanTag() {
        final String tagKey = "theTagKey";
        final boolean tagValue = false;

        Metrics.metric("ignored")
                .tag(tagKey, tagValue) // method under test
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getTags(), hasItem(MetricTag.of(tagKey, tagValue)));
    }

    @Test
    public void testAddingASingleIntegerTag() {
        final String tagKey = "theTagKey";
        final int tagValue = 1;

        Metrics.metric("ignored")
                .tag(tagKey, tagValue) // method under test
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getTags(), hasItem(MetricTag.of(tagKey, tagValue)));
    }

    @Test
    public void testAddingVarArgTags() {
        final MetricTag.RequiredMetricTag[] metricTags = {MetricTag.of("key1", "val1"), MetricTag.of("key2", "val2")};

        Metrics.metric("ignored")
                .tags(metricTags) // method under test
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getTags(), hasItems(metricTags));
    }

    @Test
    public void testAddingCollectionOfTags() {
        final MetricTag.RequiredMetricTag[] metricTags = {MetricTag.of("key1", "val1"), MetricTag.of("key2", "val2")};

        Metrics.metric("ignored")
                .tags(asList(metricTags)) // method under test
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getTags(), hasItems(metricTags));
    }

    @Test
    public void testAddingTagsInEveryWay() {
        final String tagKey = "theTagKey";
        final String tagValue = "theTagValue";
        final MetricTag.RequiredMetricTag[] varArgsMetricTags = {
            MetricTag.of("varArgKey1", "varArgVal1"), MetricTag.of("varArgKey2", "varArgVal2")
        };
        final MetricTag.RequiredMetricTag[] iterableMetricTags = {
            MetricTag.of("iterableKey1", "iterableVal1"), MetricTag.of("iterableKey2", "iterableVal2")
        };

        Metrics.metric("ignored")

                // Methods under test
                .tag(tagKey, tagValue)
                .tags(varArgsMetricTags)
                .tags(asList(iterableMetricTags))
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getTags(), hasItem(MetricTag.of(tagKey, tagValue)));
        assertThat(metricCaptor.getValue().getTags(), hasItems(varArgsMetricTags));
        assertThat(metricCaptor.getValue().getTags(), hasItems(iterableMetricTags));
    }

    @Test
    public void testAddingTags_withTheSameKey_shouldOverrideExistingTags() {
        final String tagKey = "theTagKey";
        final String firstTagValue = "should-be-overriden";
        final String lastTagValue = "should-be-present";

        final MetricTag.RequiredMetricTag[] tags = {
            MetricTag.of(tagKey, firstTagValue), MetricTag.of(tagKey, lastTagValue)
        };

        Metrics.metric("ignored")
                .tags(asList(tags)) // method under test
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getTags(), not(hasItem(MetricTag.of(tagKey, firstTagValue))));
        assertThat(metricCaptor.getValue().getTags(), hasItem(MetricTag.of(tagKey, lastTagValue)));
    }

    @Test
    public void testWithAnalytics() {
        Metrics.metric("ignored")
                .withAnalytics() // method under test
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getTags(), hasItem(MetricTag.SEND_ANALYTICS));
    }

    @Test
    public void testFromPluginKey() {
        final String pluginKey = "pluginKey";

        Metrics.metric("ignored")
                .fromPluginKey(pluginKey) // method under test
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getTags(), hasItem(MetricTag.of(FROM_PLUGIN_KEY_TAG_KEY, pluginKey)));
    }

    @Test
    public void testFromPluginKey_should_notAddANullPluginKey() {
        final String nullPluginKey = null;

        Metrics.metric("ignored")
                .fromPluginKey(nullPluginKey) // method under test
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        final List<String> tagKeys = metricCaptor.getValue().getTags().stream()
                .map(MetricTag::getKey)
                .collect(toList());
        assertThat(tagKeys, not(hasItem(FROM_PLUGIN_KEY_TAG_KEY)));
    }

    @Test
    public void testFromPluginKey_should_unsetIfNullValue() {
        final String nonNullPluginKey = "pluginKey";

        Metrics.metric("ignored")
                .fromPluginKey(nonNullPluginKey) // method under test
                .fromPluginKey(null) // method under test
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(
                metricCaptor.getValue().getTags().stream()
                        .map(MetricTag::getKey)
                        .collect(toList()),
                not(hasItem(FROM_PLUGIN_KEY_TAG_KEY)));
    }

    @Test
    public void testInvokerPluginKey() {
        final String pluginKey = "pluginKey";

        Metrics.metric("ignored")
                .invokerPluginKey(pluginKey) // method under test
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getTags(), hasItem(MetricTag.of(INVOKER_PLUGIN_KEY_TAG_KEY, pluginKey)));
    }

    @Test
    public void testInvokerPluginKey_should_notAddANullPluginKey() {
        final String nullPluginKey = null;

        Metrics.metric("ignored")
                .invokerPluginKey(nullPluginKey) // method under test
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        final List<String> tagKeys = metricCaptor.getValue().getTags().stream()
                .map(MetricTag::getKey)
                .collect(toList());
        assertThat(tagKeys, not(hasItem(INVOKER_PLUGIN_KEY_TAG_KEY)));
    }

    @Test
    public void testInvokerPluginKey_should_unsetIfNullValue() {
        final String nonNullPluginKey = "pluginKey";

        Metrics.metric("ignored")
                .invokerPluginKey(nonNullPluginKey) // method under test
                .invokerPluginKey(null) // method under test
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(
                metricCaptor.getValue().getTags().stream()
                        .map(MetricTag::getKey)
                        .collect(toList()),
                not(hasItem(INVOKER_PLUGIN_KEY_TAG_KEY)));
    }

    @Test
    public void testWithInvokerPluginKey() {
        final String providedPluginKey = "providedPluginKey";
        PluginKeyStack.push(providedPluginKey);

        Metrics.metric("ignored")
                .withInvokerPluginKey() // method under test
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(
                metricCaptor.getValue().getTags(),
                hasItem(MetricTag.of(INVOKER_PLUGIN_KEY_TAG_KEY, providedPluginKey)));
    }

    @Test
    public void testStartTimer() {
        final MetricKey trackedMetricKey =
                MetricKey.metricKey("uniqueTimerMetricName", MetricTag.of("tagKey", "tagValue"));

        Metrics.metric(trackedMetricKey.getMetricName())
                .tags(trackedMetricKey.getTags())
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue(), equalTo(trackedMetricKey));
    }

    @Test
    public void testStartLongRunningTimer() {
        final MetricKey trackedMetricKey =
                MetricKey.metricKey("uniqueLongRunningTimerMetricName", MetricTag.of("tagKey", "tagValue"));

        Metrics.metric(trackedMetricKey.getMetricName())
                .tags(trackedMetricKey.getTags())
                .startLongRunningTimer()
                .close();

        verify(metricStrategy).startLongRunningTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue(), equalTo(trackedMetricKey));
    }

    @Test
    public void testIncrementCounter() {
        final MetricTag.RequiredMetricTag metricTag = MetricTag.of("tagKey", "tagValue");
        final MetricKey metricKey = MetricKey.metricKey("counter", metricTag);
        final long trackedUpdateValue = 363;

        Metrics.metric(metricKey.getMetricName()).tags(metricTag).incrementCounter(trackedUpdateValue);
        final ArgumentCaptor<Long> updateValueCaptor = ArgumentCaptor.forClass(Long.class);
        verify(metricStrategy).incrementCounter(metricCaptor.capture(), updateValueCaptor.capture());

        assertThat(metricCaptor.getValue(), equalTo(metricKey));
        assertThat(updateValueCaptor.getValue(), equalTo(trackedUpdateValue));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIncrementCounter_shouldNotAcceptNegativeValues() {
        final long negativeNumber = -1L;

        Metrics.metric("ignored").incrementCounter(negativeNumber);

        verify(metricStrategy, never()).incrementCounter(any(), any());
    }

    @Test
    public void testHistogramUpdate() {
        final MetricKey trackedMetricKey =
                MetricKey.metricKey("uniqueHistogramMetricName", MetricTag.of("tagKey", "tagValue"));
        final long trackedUpdateValue = 77777;

        Metrics.metric(trackedMetricKey.getMetricName())
                .tags(trackedMetricKey.getTags())
                .histogram()
                .update(trackedUpdateValue);

        ArgumentCaptor<Long> updateValueCaptor = ArgumentCaptor.forClass(Long.class);
        verify(metricStrategy).updateHistogram(metricCaptor.capture(), updateValueCaptor.capture());

        assertThat(metricCaptor.getValue(), equalTo(trackedMetricKey));
        assertThat(updateValueCaptor.getValue(), equalTo(trackedUpdateValue));
    }

    @Test
    public void testUpdateGauge() {
        final MetricKey metricKey = MetricKey.metricKey("counter", MetricTag.of("tagKey", "tagValue"));
        final long trackedUpdateValue = 363;

        Metrics.metric(metricKey.getMetricName()).tags(metricKey.getTags()).incrementGauge(trackedUpdateValue);
        final ArgumentCaptor<Long> updateValueCaptor = ArgumentCaptor.forClass(Long.class);
        verify(metricStrategy).incrementGauge(metricCaptor.capture(), updateValueCaptor.capture());

        assertThat(metricCaptor.getValue(), equalTo(metricKey));
        assertThat(updateValueCaptor.getValue(), equalTo(trackedUpdateValue));
    }

    @Test
    public void testNullTagValues_shouldShowInOutput() {
        final String tag1Key = "tag1";
        final String tag2Key = "tag2";
        final String tag3Key = "tag3";
        final String[] expectedTagKeys = {tag1Key, tag2Key, tag3Key};

        Metrics.metric("irrelevant")
                .tag(tag1Key, null)
                .tags(MetricTag.of(tag2Key, null))
                .tags(singleton(MetricTag.of(tag3Key, null)))
                .timer()
                .start()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        final List<String> capturedMetricTags = metricCaptor.getValue().getTags().stream()
                .map(MetricTag::getKey)
                .collect(toList());

        assertThat(capturedMetricTags, hasItems(expectedTagKeys));
    }

    @Test
    public void testCollect_collectsSpecifiedTags() {
        final String trackedMetricTagKey = "trackedMetricTagKey";

        try (MetricTagContext.Closeable ignored =
                MetricTagContext.put(MetricTag.optionalOf(trackedMetricTagKey, null))) {
            Metrics.metric("irrelevant")
                    .collect(trackedMetricTagKey) // under test
                    .startTimer()
                    .close();
        }

        verify(metricStrategy).startTimer(metricCaptor.capture());

        assertThat(
                metricCaptor.getValue().getTags().stream()
                        .map(MetricTag::getKey)
                        .collect(toList()),
                hasItem(trackedMetricTagKey));
    }

    @Test
    public void testMetricContextTags_shouldNotBeCollectedByDefault() {
        final String trackedMetricTagKey = "trackedMetricTagKey";

        // under test
        try (MetricTagContext.Closeable ignored =
                MetricTagContext.put(MetricTag.optionalOf(trackedMetricTagKey, null))) {
            Metrics.metric("irrelevant").startTimer().close();
        }

        verify(metricStrategy).startTimer(metricCaptor.capture());

        assertThat(
                metricCaptor.getValue().getTags().stream()
                        .map(MetricTag::getKey)
                        .collect(toList()),
                not(hasItem(trackedMetricTagKey)));
    }

    @Test
    public void optionalTagsShouldBeEmitted_whenOneOrMoreAreEnabled() {
        final String metricName = "propWithMultipleValues";
        final String tagKey1 = "url";
        final String tagValue1 = "http://my-url-val.lol";
        final String tagKey2 = "location";
        final String tagValue2 = "on earth";

        Metrics.metric(metricName)
                .optionalTag(tagKey1, tagValue1)
                .optionalTag(tagKey2, tagValue2)
                .optionalTag("notEnabledTag", "meh")
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getMetricName(), equalTo(metricName));
        assertThat(
                metricCaptor.getValue().getTags(),
                hasItems(MetricTag.of(tagKey1, tagValue1), MetricTag.of(tagKey2, tagValue2)));
        assertThat(metricCaptor.getValue().getTags(), hasSize(2));
    }

    @Test
    public void optionalTagsShouldNotOverrideRequiredTags_whenOptionalTagsAreEnabled() {
        final String metricName = "propWithMultipleValues";
        final String tagKey1 = "url";
        final String tagRequiredValue1 = "this one will be collected 1";
        final String tagValue1 = "should be ignored";
        final String tagKey2 = "location";
        final String tagValue2 = "should be ignored";
        final String tagRequiredValue2 = "this one will be collected 2";

        Metrics.metric(metricName)
                .tag(tagKey1, tagRequiredValue1)
                .optionalTag(tagKey1, tagValue1)
                .optionalTag(tagKey2, tagValue2)
                .tag(tagKey2, tagRequiredValue2)
                .optionalTag("notEnabledTag", "meh")
                .startTimer()
                .close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getMetricName(), equalTo(metricName));
        assertThat(
                metricCaptor.getValue().getTags(),
                hasItems(MetricTag.of(tagKey1, tagRequiredValue1), MetricTag.of(tagKey2, tagRequiredValue2)));
        assertThat(metricCaptor.getValue().getTags(), hasSize(2));
    }

    @Test
    public void metricTagContextTagsShouldNotBeEmitted_whenOptionalTagsAreNotEnabled() {
        final String metricName = "propWithMultipleValues";
        final String key = "notInPropList";
        final String val = "location";

        try (MetricTagContext.Closeable ignored = MetricTagContext.put(MetricTag.optionalOf(key, val))) {
            Metrics.metric(metricName).startTimer().close();
        }

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getMetricName(), equalTo(metricName));
        assertThat(metricCaptor.getValue().getTags(), hasSize(0));
    }

    @Test
    public void metricTagContextTagsShouldNotOverrideRequiredTag_whenOptionalTagsAreEnabled() {
        final String metricName = "propWithMultipleValues";
        final String requiredTagKey = "url";
        final String requiredTagValue = "this one will be collected 1";
        final String metricTagContextValue = "location";

        try (MetricTagContext.Closeable ignored =
                MetricTagContext.put(MetricTag.optionalOf(requiredTagKey, metricTagContextValue))) {
            Metrics.metric(metricName)
                    .tag(requiredTagKey, requiredTagValue)
                    .startTimer()
                    .close();
        }

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getMetricName(), equalTo(metricName));
        assertThat(metricCaptor.getValue().getTags(), hasItem(MetricTag.of(requiredTagKey, requiredTagValue)));
        assertThat(metricCaptor.getValue().getTags(), hasSize(1));
    }

    @Test
    public void metricTagContextTagsShouldNotOverrideOptionalTag_whenOptionalTagsAreEnabled() {
        final String metricName = "propWithMultipleValues";
        final String requiredTagKey = "url";
        final String optionalTagValue = "this one will be collected 1";
        final String metricTagContextValue = "location";

        try (MetricTagContext.Closeable ignored =
                MetricTagContext.put(MetricTag.optionalOf(requiredTagKey, metricTagContextValue))) {
            Metrics.metric(metricName)
                    .optionalTag(requiredTagKey, optionalTagValue)
                    .startTimer()
                    .close();
        }

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getMetricName(), equalTo(metricName));
        assertThat(metricCaptor.getValue().getTags(), hasItem(MetricTag.of(requiredTagKey, optionalTagValue)));
        assertThat(metricCaptor.getValue().getTags(), hasSize(1));
    }

    @Test
    public void metricTagContextShouldBeEmitted_whenEnabledAndNotOverridingOtherTags() {
        final String metricName = "propWithMultipleValues";
        final String requiredTagKey = "url";
        final String requiredTagValue = "this one will be collected 1";
        final String optionalTagKey = "location";
        final String optionalTagValue = "should be ignored";
        final String metricTagContextKey = "trace";
        final String metricTagContextValue = "yayayaya";

        try (MetricTagContext.Closeable ignored =
                MetricTagContext.put(MetricTag.optionalOf(metricTagContextKey, metricTagContextValue))) {
            Metrics.metric(metricName)
                    .tag(requiredTagKey, requiredTagValue)
                    .optionalTag(optionalTagKey, optionalTagValue)
                    .startTimer()
                    .close();
        }

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getMetricName(), equalTo(metricName));
        assertThat(
                metricCaptor.getValue().getTags(),
                hasItems(
                        MetricTag.of(requiredTagKey, requiredTagValue),
                        MetricTag.of(optionalTagKey, optionalTagValue),
                        MetricTag.of(metricTagContextKey, metricTagContextValue)));
        assertThat(metricCaptor.getValue().getTags(), hasSize(3));
    }

    @Test
    public void optionalTagsShouldNotBeEmitted_whenTagIsNotEnabled() {
        final String metricName = "someMetricNoInSysProps";
        final String tagKey = "url";

        Metrics.metric(metricName).optionalTag(tagKey, "AAA").startTimer().close();

        verify(metricStrategy).startTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getMetricName(), equalTo(metricName));
        assertTrue(metricCaptor.getValue().getTags().isEmpty());
    }
}
