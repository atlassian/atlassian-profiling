package com.atlassian.util.profiling.filters;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.atlassian.util.profiling.StrategiesRegistry;
import com.atlassian.util.profiling.strategy.impl.StackProfilerStrategy;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RequestProfilingFilterTest {

    @Mock
    private FilterChain chain;

    private RequestProfilingFilter filter;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private Logger logger;

    private Logger originalLogger;

    @Before
    public void setup() {
        filter = new RequestProfilingFilter();
        StackProfilerStrategy strategy = StrategiesRegistry.getDefaultProfilerStrategy();
        originalLogger = strategy.getLogger();
        strategy.setLogger(logger);

        when(request.getRequestURI()).thenReturn("requestURI");
        when(logger.isDebugEnabled()).thenReturn(true);
    }

    @After
    public void tearDown() {
        StrategiesRegistry.getDefaultProfilerStrategy().setLogger(originalLogger);
    }

    @Test
    public void testDisabled() throws IOException, ServletException {
        initWithActivateParameter(null);

        filter.doFilter(request, response, chain);

        verify(chain).doFilter(request, response);
        verifyZeroInteractions(logger);
    }

    @Test
    public void testEnabledAndActivatedByRequestParam() throws IOException, ServletException {
        initWithActivateParameter("activate");
        when(request.getParameter("activate")).thenReturn("do it");

        filter.doFilter(request, response, chain);

        ArgumentCaptor<String> logCaptor = ArgumentCaptor.forClass(String.class);
        InOrder inOrder = Mockito.inOrder(chain, logger);
        inOrder.verify(chain).doFilter(request, response);
        inOrder.verify(logger).debug(logCaptor.capture());

        assertThat(logCaptor.getValue(), CoreMatchers.containsString("requestURI"));
    }

    @Test
    public void testEnabledButNotActivatedByRequestParam() throws IOException, ServletException {
        initWithActivateParameter("activate");

        filter.doFilter(request, response, chain);

        verify(chain).doFilter(request, response);
        verifyZeroInteractions(logger);
    }

    private void initWithActivateParameter(String activateParamName) {
        FilterConfig config = mock(FilterConfig.class);
        when(config.getInitParameter("activate.param")).thenReturn(activateParamName);
        filter.init(config);
    }
}
