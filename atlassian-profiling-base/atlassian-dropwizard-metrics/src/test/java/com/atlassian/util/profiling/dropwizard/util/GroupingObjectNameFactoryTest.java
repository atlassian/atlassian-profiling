package com.atlassian.util.profiling.dropwizard.util;

import java.util.ArrayList;
import java.util.List;
import javax.management.ObjectName;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import static com.atlassian.util.profiling.dropwizard.util.GroupingObjectNameFactory.CATEGORY_BASE_KEY;
import static com.atlassian.util.profiling.dropwizard.util.GroupingObjectNameFactory.METRICS_PROPERTY;
import static com.atlassian.util.profiling.dropwizard.util.GroupingObjectNameFactory.NAME_KEY;
import static com.atlassian.util.profiling.dropwizard.util.GroupingObjectNameFactory.NAME_SPLIT_DELIMITER;
import static com.atlassian.util.profiling.dropwizard.util.GroupingObjectNameFactory.TYPE_KEY;
import static com.atlassian.util.profiling.dropwizard.util.GroupingObjectNameFactory.buildCategoryKey;

public class GroupingObjectNameFactoryTest {
    private static final String SIMPLE_INSTRUMENT_TYPE = "aType";
    private static final String SIMPLE_DOMAIN = "aDomain";
    private static final String SIMPLE_NAME = "aName";

    @Test
    public void createName_should_setMetricsType() {
        final ObjectName simpleObjectName = buildSimpleObjectName();

        assertEquals(METRICS_PROPERTY, simpleObjectName.getKeyProperty(TYPE_KEY));
    }

    @Test
    public void createName_should_notCareAboutInstrumentType() {
        final String[] instrumentTypes = {
            "gauges", "counters", "histograms", "meters", "timers", "mcb", "scott", "qwerty"
        };

        final GroupingObjectNameFactory objectNameFactory = new GroupingObjectNameFactory();

        final ObjectName simpleObjectName = buildSimpleObjectName();
        for (String instrumentType : instrumentTypes) {
            assertEquals(simpleObjectName, objectNameFactory.createName(instrumentType, SIMPLE_DOMAIN, SIMPLE_NAME));
        }
    }

    @Test
    public void createName_should_splitNameIntoATag() {
        final String nameWithOneCategory = joinWithNameSplitDelimiter("aCategory", SIMPLE_NAME);

        final ObjectName objectNameWithCategories = buildObjectNameWithProvidedName(nameWithOneCategory);

        assertEquals(SIMPLE_NAME, objectNameWithCategories.getKeyProperty(NAME_KEY));
    }

    @Test
    public void createName_should_splitCategoriesIntoTags() {
        final String firstCategory = "firstCategory";
        final String secondCategory = "secondCategory";
        final String nameWithTwoCategories = joinWithNameSplitDelimiter(firstCategory, secondCategory, SIMPLE_NAME);

        final String firstCategoryKey = buildCategoryKey(0);
        final String secondCategoryKey = buildCategoryKey(1);

        final ObjectName objectNameWithCategories = buildObjectNameWithProvidedName(nameWithTwoCategories);

        assertEquals(firstCategory, objectNameWithCategories.getKeyProperty(firstCategoryKey));
        assertEquals(secondCategory, objectNameWithCategories.getKeyProperty(secondCategoryKey));
    }

    @Test
    public void createName_should_NotSplitDomain() {
        final String complexDomain = joinWithNameSplitDelimiter("a", "name");

        final GroupingObjectNameFactory objectNameFactory = new GroupingObjectNameFactory();
        final ObjectName objectNameWithCategories =
                objectNameFactory.createName(SIMPLE_INSTRUMENT_TYPE, complexDomain, SIMPLE_NAME);

        assertEquals(complexDomain, objectNameWithCategories.getDomain());
    }

    @Test
    public void buildCategoryKey_should_handleUpToAHundredCategories() {
        final List<String> expectedInOrder = new ArrayList<>();
        expectedInOrder.add(CATEGORY_BASE_KEY + "00");
        expectedInOrder.add(CATEGORY_BASE_KEY + "01");
        expectedInOrder.add(CATEGORY_BASE_KEY + "10");

        final List<String> actualOrderCategoryOrder = new ArrayList<>();
        actualOrderCategoryOrder.add(buildCategoryKey(0));
        actualOrderCategoryOrder.add(buildCategoryKey(1));
        actualOrderCategoryOrder.add(buildCategoryKey(10));

        // Lexicographic sorting would put "category10" before "category1", we want to protect against that
        actualOrderCategoryOrder.sort(String::compareTo);

        assertEquals(expectedInOrder, actualOrderCategoryOrder);
    }

    //      TESTING HELPER METHODS

    private static ObjectName buildSimpleObjectName() {
        return buildObjectNameWithProvidedName(SIMPLE_NAME);
    }

    private static ObjectName buildObjectNameWithProvidedName(final String name) {
        return new GroupingObjectNameFactory().createName(SIMPLE_INSTRUMENT_TYPE, SIMPLE_DOMAIN, name);
    }

    private static String joinWithNameSplitDelimiter(final String... values) {
        return String.join(NAME_SPLIT_DELIMITER, values);
    }
}
