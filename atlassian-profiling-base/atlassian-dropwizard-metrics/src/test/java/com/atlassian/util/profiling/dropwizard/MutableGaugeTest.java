package com.atlassian.util.profiling.dropwizard;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MutableGaugeTest {

    private MutableGauge mutableGauge;

    @Before
    public void setup() {
        mutableGauge = new MutableGauge();
    }

    @Test
    public void shouldIncrementValue() {
        mutableGauge.increment(1);
        mutableGauge.increment(2);

        assertThat("should return incremented value of gauge", mutableGauge.getValue(), is(3L));
    }

    @Test
    public void shouldSetGaugeValue() {
        mutableGauge.setValue(1);
        mutableGauge.setValue(2);

        assertThat("should return set value of gauge", mutableGauge.getValue(), is(2L));
    }
}
