package com.atlassian.util.profiling.dropwizard;

import java.util.concurrent.atomic.AtomicLong;

import com.codahale.metrics.Gauge;

/**
 * Mutable version of {@link Gauge}
 *
 * @since 4.8.0
 */
public class MutableGauge implements Gauge<Long> {

    private final AtomicLong value = new AtomicLong();

    @Override
    public Long getValue() {
        return value.get();
    }

    public void setValue(final long newValue) {
        value.set(newValue);
    }

    public void increment(final long delta) {
        value.addAndGet(delta);
    }
}
