package com.atlassian.util.profiling.dropwizard.util;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.codahale.metrics.jmx.ObjectNameFactory;

import com.atlassian.annotations.VisibleForTesting;

/**
 * Custom {@link ObjectNameFactory} implementation that publishes metrics under
 * {@code com.atlassian.productname/metrics} and interprets the metric name as a path. For instance, a metric
 * 'project.Creation' is registered under the object name
 * 'com.atlassian.productname:type=metrics,category00=project,name=Creation'. This allows
 * related metrics to be grouped together visually in tools like JMC or JConsole.
 *
 * @since 3.5.0
 */
public class GroupingObjectNameFactory implements ObjectNameFactory {
    @VisibleForTesting
    static final String CATEGORY_BASE_KEY = "category";

    @VisibleForTesting
    static final String NAME_KEY = "name";

    @VisibleForTesting
    static final String TYPE_KEY = "type";

    @VisibleForTesting
    static final String METRICS_PROPERTY = "metrics";

    @VisibleForTesting
    static final String NAME_SPLIT_DELIMITER = ".";

    private static final String DOMAIN_DELIMITER = ":";

    private static final Logger log = LoggerFactory.getLogger(GroupingObjectNameFactory.class);

    @Override
    public ObjectName createName(final String type, final String domain, final String name) {
        final StringBuilder builder = new StringBuilder();

        builder.append(domain).append(DOMAIN_DELIMITER);

        builder.append(buildKeyProperty(TYPE_KEY, METRICS_PROPERTY));

        final String[] parts = name.split("\\" + NAME_SPLIT_DELIMITER);
        for (int i = 0; i < parts.length - 1; ++i) {
            builder.append(buildKeyProperty(buildCategoryKey(i), parts[i]));
        }

        builder.append(NAME_KEY).append("=").append(parts[parts.length - 1]);

        try {
            return new ObjectName(builder.toString());
        } catch (MalformedObjectNameException e) {
            log.warn("Unable to register {} {}", type, name, e);
            throw new RuntimeException(e);
        }
    }

    @VisibleForTesting
    static String buildCategoryKey(int categoryNumber) {
        return CATEGORY_BASE_KEY + twoDigitMinimumLeftPad(categoryNumber);
    }

    /**
     * Pads an integer with a zero, so it's at least a two character string.
     * <p>
     * Saves a dependency on apache commons
     *
     * @param integerToPad integer to be padded
     * @return a string with the padding
     */
    private static String twoDigitMinimumLeftPad(int integerToPad) {
        return String.format("%02d", integerToPad);
    }

    private static String buildKeyProperty(final String key, final String value) {
        return key + "=" + value + ",";
    }
}
