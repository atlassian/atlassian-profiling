package it.com.atlassian.profiling;

import org.junit.Test;
import org.junit.runner.RunWith;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;

import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.profiling.metrics.api.context.ContextFragment;
import com.atlassian.profiling.metrics.api.context.MetricContext;
import com.atlassian.profiling.metrics.api.tags.OptionalTag;
import com.atlassian.profiling.metrics.api.tags.TagFactory;
import com.atlassian.profiling.metrics.api.tags.TagFactoryFactory;
import com.atlassian.util.profiling.micrometer.MicrometerStrategy;
import com.atlassian.util.profiling.strategy.MetricStrategy;

import static io.micrometer.core.instrument.Tag.of;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;

import static com.atlassian.util.profiling.Metrics.metric;
import static com.atlassian.util.profiling.StrategiesRegistry.addMetricStrategy;
import static com.atlassian.util.profiling.StrategiesRegistry.removeMetricStrategy;

@RunWith(AtlassianPluginsTestRunner.class)
public class EndToEndWiredTest {
    private final MetricContext metricContext;
    private final TagFactoryFactory tagFactoryFactory;

    public EndToEndWiredTest(final MetricContext metricContext, final TagFactoryFactory tagFactoryFactory) {
        this.metricContext = metricContext;
        this.tagFactoryFactory = tagFactoryFactory;
    }

    @Test
    public void testAbleToStoreAndRetrieveTagsInContext() {
        final TagFactory prefixedTagFactory = tagFactoryFactory.prefixedTagFactory("prefix");

        final OptionalTag trackedTag = prefixedTagFactory.createOptionalTag("key", "value");
        try (ContextFragment ignored = metricContext.put(trackedTag)) {
            assertThat(metricContext.getAll(), hasItems(trackedTag));
        }
    }

    @Test
    public void testAbleToAddTagsToMetrics() {
        // setup
        final MeterRegistry meterRegistry = new SimpleMeterRegistry();
        final MetricStrategy metricStrategy = new MicrometerStrategy(meterRegistry);
        addMetricStrategy(metricStrategy);
        final String metricName = "metricNameUniqueTo_test_shouldBeAbleToDecorateMetricsWithTags";

        // given
        final String keyPrefix = "prefix";
        final String tagKey = "key";
        final String tagValue = "value";
        final TagFactory prefixedTagFactory = tagFactoryFactory.prefixedTagFactory(keyPrefix);
        final String expectedPrefixedTagKey = keyPrefix + tagKey;

        // when
        final OptionalTag trackedTag = prefixedTagFactory.createOptionalTag(tagKey, tagValue);
        try (ContextFragment ignored = metricContext.put(trackedTag)) {
            // some Atlassian written metric
            metric(metricName).collect(expectedPrefixedTagKey).incrementCounter(1L);
        }

        // then
        final Tag expectedTag = of(expectedPrefixedTagKey, tagValue);
        assertThat(meterRegistry.get(metricName).meter().getId().getTags(), hasItem(expectedTag));

        // teardown
        removeMetricStrategy(metricStrategy);
    }
}
