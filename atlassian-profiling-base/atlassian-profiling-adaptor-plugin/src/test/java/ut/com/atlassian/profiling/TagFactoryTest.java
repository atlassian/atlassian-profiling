package ut.com.atlassian.profiling;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.atlassian.profiling.metrics.api.tags.TagFactory;
import com.atlassian.profiling.metrics.tags.PrefixedTagFactoryAdaptor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.rules.ExpectedException.none;

public class TagFactoryTest {
    @Rule
    public ExpectedException exceptionRule = none();

    private TagFactory prefixedTagFactory;

    @Before
    public void setup() {
        prefixedTagFactory = new PrefixedTagFactoryAdaptor("");
    }

    @Test
    public void testGeneratedTagEquality_shouldBeBasedOnKeyAndValue() {
        assertEquals(
                prefixedTagFactory.createOptionalTag("sameKey", "sameValue"),
                prefixedTagFactory.createOptionalTag("sameKey", "sameValue"));

        assertNotEquals(
                prefixedTagFactory.createOptionalTag("sameKey", "sameValue"),
                prefixedTagFactory.createOptionalTag("sameKey", "differentValue"));
        assertNotEquals(
                prefixedTagFactory.createOptionalTag("sameKey", "sameValue"),
                prefixedTagFactory.createOptionalTag("differentKey", "sameValue"));
        assertNotEquals(
                prefixedTagFactory.createOptionalTag("sameKey", "sameValue"),
                prefixedTagFactory.createOptionalTag("differentKey", "differentValue"));
    }

    @Test
    public void testGetKey() {
        final String key = "sameKey";
        assertEquals(prefixedTagFactory.createOptionalTag(key, "value").getKey(), key);
    }

    @Test
    public void testGetValue() {
        final String value = "sameValue";
        assertEquals(prefixedTagFactory.createOptionalTag("key", value).getValue(), value);
    }

    @Test
    public void testOptional_shouldNotAcceptANullKey() {
        exceptionRule.expect(NullPointerException.class);
        prefixedTagFactory.createOptionalTag(null, "notNull");
    }

    @Test
    public void testOptional_shouldNotAcceptANullValue() {
        exceptionRule.expect(NullPointerException.class);
        prefixedTagFactory.createOptionalTag("notNull", null);
    }
}
