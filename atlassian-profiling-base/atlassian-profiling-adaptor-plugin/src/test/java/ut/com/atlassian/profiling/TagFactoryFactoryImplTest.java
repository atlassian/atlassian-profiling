package ut.com.atlassian.profiling;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.atlassian.profiling.metrics.api.tags.TagFactory;
import com.atlassian.profiling.metrics.tags.TagFactoryFactoryImpl;

import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;
import static org.junit.rules.ExpectedException.none;

public class TagFactoryFactoryImplTest {
    @Rule
    public ExpectedException exceptionRule = none();

    @Test
    public void testTagFactory_shouldNotAcceptANullKeyPrefix() {
        exceptionRule.expect(NullPointerException.class);
        new TagFactoryFactoryImpl().prefixedTagFactory(null);
    }

    @Test
    public void testTagFactory_shouldProduceATagFactoryThatPrefixes() {
        final String tagKeyPrefix = "tagKeyPrefix";

        final TagFactory prefixedTagFactory = new TagFactoryFactoryImpl().prefixedTagFactory(tagKeyPrefix);

        assertThat(prefixedTagFactory.createOptionalTag("someKey", "someValue").getKey(), startsWith(tagKeyPrefix));
    }
}
