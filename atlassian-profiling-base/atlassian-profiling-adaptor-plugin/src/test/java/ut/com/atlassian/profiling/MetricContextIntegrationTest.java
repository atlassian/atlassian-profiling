package ut.com.atlassian.profiling;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;

import com.atlassian.profiling.metrics.api.context.ContextFragment;
import com.atlassian.profiling.metrics.api.context.MetricContext;
import com.atlassian.profiling.metrics.api.tags.OptionalTag;
import com.atlassian.profiling.metrics.api.tags.TagFactory;
import com.atlassian.profiling.metrics.context.MetricContextAdaptor;
import com.atlassian.profiling.metrics.tags.PrefixedTagFactoryAdaptor;
import com.atlassian.util.profiling.micrometer.MicrometerStrategy;
import com.atlassian.util.profiling.strategy.MetricStrategy;

import static java.lang.System.setProperty;
import static org.junit.Assert.assertEquals;

import static com.atlassian.util.profiling.MetricTag.UNDEFINED_TAG_VALUE;
import static com.atlassian.util.profiling.Metrics.getConfiguration;
import static com.atlassian.util.profiling.Metrics.metric;
import static com.atlassian.util.profiling.ProfilingConstants.OPTIONAL_TAG_PROPERTY_PREFIX;
import static com.atlassian.util.profiling.StrategiesRegistry.addMetricStrategy;
import static com.atlassian.util.profiling.StrategiesRegistry.removeMetricStrategy;

public class MetricContextIntegrationTest {
    private MeterRegistry meterRegistry;
    private MetricStrategy metricStrategy;
    private MetricContext metricContext;
    private TagFactory prefixedTagFactory;

    @Before
    public void setup() {
        // Setup Atlassian Profiling's metrics
        meterRegistry = new SimpleMeterRegistry();
        metricStrategy = new MicrometerStrategy(meterRegistry);
        addMetricStrategy(metricStrategy);
        getConfiguration().setEnabled(true);

        metricContext = new MetricContextAdaptor();
        prefixedTagFactory = new PrefixedTagFactoryAdaptor("");
    }

    @Test
    public void testMetricContext_shouldBeAbleToBePickedUpByMetrics() {
        // given
        final String tagKey = "tagKey";
        final String tagValue = "tagValue";
        final OptionalTag tag = prefixedTagFactory.createOptionalTag(tagKey, tagValue);

        // when
        try (ContextFragment ignored = metricContext.put(tag)) {
            metric("irrelevant").collect(tagKey).incrementCounter(1L);
        }

        // then
        assertEquals(
                "tag value from context should match",
                tagValue,
                meterRegistry.getMeters().get(0).getId().getTags().get(0).getValue());
    }

    /**
     * This is a side effect of being able to use MetricTagContext for internal tagging of metrics. This also interacts
     * with shadowing of tags, i.e we may override vendor tags, and they may override our tags. The API is designed for
     * them to provide a prefix, which we'll trust they'll do unless there's a reason not to.
     */
    @Test
    public void testCollectedTags_shouldBeAbleToOverrideProvidedTags() {
        // given
        final String conflictingTagKey = "tagKey";
        final String collectedTagValue = "provided";

        // when
        try (ContextFragment ignored =
                metricContext.put(prefixedTagFactory.createOptionalTag(conflictingTagKey, collectedTagValue))) {
            metric("irrelevant")
                    .tag(conflictingTagKey, "providedTagValue")
                    .collect(conflictingTagKey)
                    .incrementCounter(1L);
        }

        // then
        assertEquals(
                "tag value from context should match",
                collectedTagValue,
                meterRegistry.getMeters().get(0).getId().getTags().get(0).getValue());
    }

    /**
     * This isn't ideal, this is resultant behaviour in wanting to be able to use MetricTagContext for internal tagging
     * of metrics. This means it's possible for a key clash with App vendors....
     */
    @Test
    public void testProvidedTags_shouldBeAbleToOverrideCollectedTags() {
        // given
        final String conflictingTagKey = "tagKey";
        final String providedTagValue = "provided";

        // when
        try (ContextFragment ignored =
                metricContext.put(prefixedTagFactory.createOptionalTag(conflictingTagKey, "collectedTagValue"))) {
            metric("irrelevant")
                    .collect(conflictingTagKey)
                    .tag(conflictingTagKey, providedTagValue)
                    .incrementCounter(1L);
        }

        // then
        assertEquals(
                "tag value from context should match",
                providedTagValue,
                meterRegistry.getMeters().get(0).getId().getTags().get(0).getValue());
    }

    /**
     * We assume the metric author knows best, we don't want to accidentally override a tag and end up with potentially
     * inconsistent or misleading information
     */
    @Test
    public void testContextTags_shouldNotOverrideOptionalTags() {
        // given
        final String tagKey = "tagKey";
        final String optionalTagValue = "optionalTagValue";

        // setup
        final String metricName = "metricName";
        setProperty(OPTIONAL_TAG_PROPERTY_PREFIX + metricName, tagKey);
        getConfiguration().reloadConfigs();

        // when
        try (ContextFragment ignored =
                metricContext.put(prefixedTagFactory.createOptionalTag(tagKey, "context tag value"))) {
            metric(metricName).optionalTag(tagKey, optionalTagValue).incrementCounter(1L);
        }

        // then
        assertEquals(
                "tag value from context should match",
                optionalTagValue,
                meterRegistry.getMeters().get(0).getId().getTags().get(0).getValue());
    }

    /**
     * We assume the metric author knows best, we don't want to accidentally override a tag and end up with potentially
     * inconsistent or misleading information
     */
    @Test
    public void testContextTags_shouldNotOverrideOptionalTagsWithNullValues() {
        // given
        final String tagKey = "tagKey";

        // setup
        final String metricName = "metricName";
        setProperty(OPTIONAL_TAG_PROPERTY_PREFIX + metricName, tagKey);
        getConfiguration().reloadConfigs();

        // when
        try (ContextFragment ignored =
                metricContext.put(prefixedTagFactory.createOptionalTag(tagKey, "context tag value"))) {
            metric(metricName).optionalTag(tagKey, null).incrementCounter(1L);
        }

        // then
        assertEquals(
                "tag value from context should match",
                UNDEFINED_TAG_VALUE,
                meterRegistry.getMeters().get(0).getId().getTags().get(0).getValue());
    }

    @After
    public void teardown() {
        removeMetricStrategy(metricStrategy);
    }
}
