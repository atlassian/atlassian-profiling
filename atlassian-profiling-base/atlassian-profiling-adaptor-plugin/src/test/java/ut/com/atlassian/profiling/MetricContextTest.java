package ut.com.atlassian.profiling;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.atlassian.profiling.metrics.api.context.ContextFragment;
import com.atlassian.profiling.metrics.api.context.MetricContext;
import com.atlassian.profiling.metrics.api.tags.OptionalTag;
import com.atlassian.profiling.metrics.api.tags.TagFactory;
import com.atlassian.profiling.metrics.context.MetricContextAdaptor;
import com.atlassian.profiling.metrics.tags.PrefixedTagFactoryAdaptor;

import static java.util.Collections.emptySet;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.rules.ExpectedException.none;

public class MetricContextTest {
    @Rule
    public ExpectedException exceptionRule = none();

    private MetricContext metricContext;
    private TagFactory tagFactory;

    @Before
    public void setup() {
        metricContext = new MetricContextAdaptor();
        tagFactory = new PrefixedTagFactoryAdaptor("");
    }

    @Test
    public void testOptional_shouldNotAcceptNullTags() {
        exceptionRule.expect(NullPointerException.class);
        metricContext.put(tagFactory.createOptionalTag("irrelevant", "irrelevant"), null);
    }

    @Test
    public void testMetricContext_shouldBeAbleToStoreAndRetrieveTags() {
        final OptionalTag trackedTag = tagFactory.createOptionalTag("irrelevant", "irrelevant");
        try (ContextFragment ignored = metricContext.put(trackedTag)) {
            assertThat(metricContext.getAll(), hasItems(trackedTag));
        }
    }

    @Test
    public void testMetricContext_shouldNotStoreTagsAfterClosure() {
        final OptionalTag trackedTag = tagFactory.createOptionalTag("irrelevant", "irrelevant");
        try (ContextFragment ignored = metricContext.put(trackedTag)) {}
        assertThat(metricContext.getAll(), is(emptySet()));
    }

    @Test
    public void testMetricContext_shouldBeNestable() {
        final OptionalTag firstTag = tagFactory.createOptionalTag("firstKey", "tag");
        final OptionalTag secondTag = tagFactory.createOptionalTag("secondKey", "tag");
        try (ContextFragment ignored = metricContext.put(firstTag)) {
            try (ContextFragment ignored1 = metricContext.put(secondTag)) {
                assertThat(
                        "context fragments should be additive when nested",
                        metricContext.getAll(),
                        hasItems(firstTag, secondTag));
            }
            assertThat("nested context fragments shouldn't leak", metricContext.getAll(), hasItems(firstTag));
        }
    }

    @Test
    public void testMetricContext_shouldShadowTags() {
        final String sharedKey = "sameKey";
        final OptionalTag firstTag = tagFactory.createOptionalTag(sharedKey, "firstTag");
        final OptionalTag secondTag = tagFactory.createOptionalTag(sharedKey, "secondTag");
        try (ContextFragment ignored = metricContext.put(firstTag)) {
            try (ContextFragment ignored1 = metricContext.put(secondTag)) {
                assertThat("tags with the same key should be shadowed", metricContext.getAll(), hasItems(secondTag));
            }
            assertThat("shadowed tags shouldn't leak", metricContext.getAll(), hasItems(firstTag));
        }
    }

    @Test
    public void testShadowing_shouldNotClearParent() {
        final String sharedTagKey = "tagKey";
        final String sharedTagValue = "tagValue";
        try (ContextFragment ignored = metricContext.put(tagFactory.createOptionalTag(sharedTagKey, sharedTagValue))) {
            try (ContextFragment ignored2 =
                    metricContext.put(tagFactory.createOptionalTag(sharedTagKey, sharedTagValue))) {}
            assertThat(metricContext.getAll(), hasItem(tagFactory.createOptionalTag(sharedTagKey, sharedTagValue)));
        }
    }
}
