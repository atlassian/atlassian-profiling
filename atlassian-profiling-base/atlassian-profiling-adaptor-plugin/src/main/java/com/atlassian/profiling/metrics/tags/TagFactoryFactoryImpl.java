package com.atlassian.profiling.metrics.tags;

import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.annotations.Internal;
import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;
import com.atlassian.profiling.metrics.api.tags.TagFactory;
import com.atlassian.profiling.metrics.api.tags.TagFactoryFactory;

import static java.util.Objects.requireNonNull;

@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
@Internal
public class TagFactoryFactoryImpl implements TagFactoryFactory {
    @Override
    public TagFactory prefixedTagFactory(final String keyPrefix) {
        requireNonNull(keyPrefix, "keyPrefix");
        return new PrefixedTagFactoryAdaptor(keyPrefix);
    }
}
