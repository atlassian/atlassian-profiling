package com.atlassian.profiling.config;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.osgi.framework.ServiceRegistration;

import com.atlassian.profiling.metrics.api.context.MetricContext;
import com.atlassian.profiling.metrics.api.tags.TagFactoryFactory;
import com.atlassian.profiling.metrics.context.MetricContextAdaptor;
import com.atlassian.profiling.metrics.tags.TagFactoryFactoryImpl;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;

@Configuration
public class ProfilingAdaptorConfig {

    @Bean
    public MetricContext metricContext() {
        return new MetricContextAdaptor();
    }

    @Bean
    public TagFactoryFactory tagFactoryFactory() {
        return new TagFactoryFactoryImpl();
    }

    @Bean
    public FactoryBean<ServiceRegistration> registerMetricContext(final MetricContext metricContext) {
        return exportOsgiService(metricContext, as(MetricContext.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> registerTagFactoryFactory(final TagFactoryFactory tagFactoryFactory) {
        return exportOsgiService(tagFactoryFactory, as(TagFactoryFactory.class));
    }
}
