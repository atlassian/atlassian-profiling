package com.atlassian.profiling.metrics.context;

import java.util.Set;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.annotations.Internal;
import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;
import com.atlassian.profiling.metrics.api.context.ContextFragment;
import com.atlassian.profiling.metrics.api.context.MetricContext;
import com.atlassian.profiling.metrics.api.tags.OptionalTag;
import com.atlassian.util.profiling.MetricTagContext;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toSet;

@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
@Internal
public class MetricContextAdaptor implements MetricContext {
    @Override
    public ContextFragment put(final OptionalTag... tags) {
        requireNonNull(tags, "tags");
        return MetricTagContext.put(tags);
    }

    @Override
    public Set<OptionalTag> getAll() {
        return MetricTagContext.getAll().stream().map(tag -> (OptionalTag) tag).collect(toSet());
    }
}
