package com.atlassian.profiling.metrics.tags;

import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.annotations.Internal;
import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;
import com.atlassian.profiling.metrics.api.tags.OptionalTag;
import com.atlassian.profiling.metrics.api.tags.TagFactory;

import static java.util.Objects.requireNonNull;

import static com.atlassian.util.profiling.MetricTag.optionalOf;

@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
@Internal
public class PrefixedTagFactoryAdaptor implements TagFactory {
    private final String keyPrefix;

    @VisibleForTesting
    public PrefixedTagFactoryAdaptor(final String keyPrefix) {
        this.keyPrefix = keyPrefix;
    }

    @Override
    public OptionalTag createOptionalTag(final String key, final String value) {
        requireNonNull(key, "key");
        requireNonNull(value, "value");
        return optionalOf(keyPrefix + key, value);
    }
}
