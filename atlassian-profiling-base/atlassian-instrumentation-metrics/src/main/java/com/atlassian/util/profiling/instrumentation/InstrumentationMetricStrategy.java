package com.atlassian.util.profiling.instrumentation;

import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;

import com.atlassian.annotations.Internal;
import com.atlassian.instrumentation.Instrument;
import com.atlassian.instrumentation.InstrumentRegistry;
import com.atlassian.instrumentation.operations.OpTimer;
import com.atlassian.util.profiling.MetricKey;
import com.atlassian.util.profiling.MetricsConfiguration;
import com.atlassian.util.profiling.Ticker;
import com.atlassian.util.profiling.strategy.MetricStrategy;

import static java.util.Objects.requireNonNull;

/**
 * Simple {@link MetricStrategy} that delegates to matching {@link OpTimer timers} from an atlassian-instrumentation
 * {@link InstrumentRegistry}
 */
@Internal
public class InstrumentationMetricStrategy implements MetricStrategy {

    private final InstrumentRegistry registry;

    public InstrumentationMetricStrategy(@Nonnull InstrumentRegistry registry) {
        this.registry = requireNonNull(registry, "registry");
    }

    @Override
    public void setConfiguration(@Nonnull MetricsConfiguration configuration) {
        // no-op
    }

    @Nonnull
    @Override
    public Ticker startTimer(@Nonnull String metricName) {
        OpTimer timer = registry.pullTimer(metricName);
        return timer::end;
    }

    @Override
    public void incrementCounter(MetricKey metricKey, long deltaValue) {
        registry.pullCounter(metricKey.getMetricName()).addAndGet(deltaValue);
    }

    @Override
    public void updateHistogram(@Nonnull String metricName, long value) {
        // no-op since atlassian-instrumentation currently does not have a histogram implementation; Gauge doesn't
        // track statistics (and does not allow us to atomically set a value), and OpCounter is purely time-based,
        // which makes it a bad fit for calculating the distribution of things like queue lengths, connections in
        // use, etc.
    }

    @Override
    public void updateTimer(@Nonnull String metricName, long time, @Nonnull TimeUnit timeUnit) {
        registry.pullTimer(metricName).endWithTime(timeUnit.toMillis(time));
    }

    @Override
    public void incrementGauge(MetricKey metricKey, long deltaValue) {
        registry.pullGauge(metricKey.getMetricName()).addAndGet(deltaValue);
    }

    @Override
    public void setGauge(MetricKey metricKey, long currentValue) {
        Instrument instrument = registry.getInstrument(metricKey.getMetricName());
        if (shouldOverrideGauge(instrument)) {
            MutableGauge mutableGauge = new MutableGauge(metricKey.getMetricName());
            registry.putInstrument(mutableGauge);
            mutableGauge.setValue(currentValue);
        } else {
            MutableGauge mutableGauge = (MutableGauge) instrument;
            mutableGauge.setValue(currentValue);
        }
    }

    private boolean shouldOverrideGauge(Instrument instrument) {
        return instrument == null || !(instrument instanceof MutableGauge);
    }
}
