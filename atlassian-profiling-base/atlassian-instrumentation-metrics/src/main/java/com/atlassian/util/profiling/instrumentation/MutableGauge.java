package com.atlassian.util.profiling.instrumentation;

import java.util.concurrent.atomic.AtomicLong;

import com.atlassian.instrumentation.AtomicGauge;

/**
 * Mutable version of {@link com.atlassian.instrumentation.Gauge Gauge}
 *
 * @since 4.8.0
 */
public class MutableGauge extends AtomicGauge {

    public MutableGauge(String name) {
        super(name);
    }

    public MutableGauge(String name, long value) {
        super(name, value);
    }

    public MutableGauge(String name, AtomicLong atomicLongRef) {
        super(name, atomicLongRef);
    }

    public void setValue(final Long valueToSet) {
        value.set(valueToSet);
    }
}
