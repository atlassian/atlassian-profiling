package com.atlassian.util.profiling.instrumentation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.instrumentation.DefaultInstrumentRegistry;
import com.atlassian.instrumentation.Gauge;
import com.atlassian.instrumentation.InstrumentRegistry;
import com.atlassian.instrumentation.operations.OpCounter;
import com.atlassian.util.profiling.MetricKey;
import com.atlassian.util.profiling.MetricTimer;
import com.atlassian.util.profiling.Metrics;
import com.atlassian.util.profiling.MetricsConfiguration;
import com.atlassian.util.profiling.StrategiesRegistry;
import com.atlassian.util.profiling.Timer;
import com.atlassian.util.profiling.Timers;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class InstrumentationMetricStrategyTest {

    private MetricsConfiguration configuration;
    private InstrumentRegistry registry;
    private InstrumentationMetricStrategy strategy;
    private boolean wasEnabled;

    @Before
    public void setup() {
        registry = new DefaultInstrumentRegistry();
        strategy = new InstrumentationMetricStrategy(registry);

        configuration = Metrics.getConfiguration();
        wasEnabled = configuration.isEnabled();
        configuration.setEnabled(true);
        StrategiesRegistry.addMetricStrategy(strategy);
    }

    @After
    public void tearDown() {
        StrategiesRegistry.removeMetricStrategy(strategy);
        configuration.setEnabled(wasEnabled);
    }

    @Test
    public void testDisabledMetricWorksWhenEnabled() {
        configuration.setEnabled(false);

        MetricTimer metric = Metrics.timer("my.metric");
        OpCounter opCounter = registry.pullOpCounter("my.metric");

        assertEquals(0L, opCounter.getInvocationCount());

        metric.start().close();

        assertEquals(0L, opCounter.getInvocationCount());

        configuration.setEnabled(true);
        metric.start().close();

        assertEquals(1L, opCounter.getInvocationCount());
    }

    @Test
    public void testEnabledMetricNoopsWhenDisabled() {
        MetricTimer metric = Metrics.timer("my.metric");
        OpCounter opCounter = registry.pullOpCounter("my.metric");

        assertEquals(0L, opCounter.getInvocationCount());

        // verify that the metric is enabled
        metric.start().close();
        assertEquals(1L, opCounter.getInvocationCount());

        // disable the strategy and verify that interacting with the metric does nothing
        configuration.setEnabled(false);
        metric.start().close();

        assertEquals(1L, opCounter.getInvocationCount());

        // re-enable and verify that the metric is enabled too
        configuration.setEnabled(true);
        metric.start().close();

        assertEquals(2L, opCounter.getInvocationCount());
    }

    @Test
    public void testCounter() {
        final String metricName = "counter";
        final long zero = 0;
        final long firstValue = 1;
        final long secondValue = 2;

        Metrics.metric(metricName).incrementCounter(zero);
        assertEquals(
                "Should accept zero", zero, registry.pullCounter(metricName).getValue());
        Metrics.metric(metricName).incrementCounter(firstValue);
        assertEquals(
                "Should start counting from 0",
                firstValue,
                registry.pullCounter(metricName).getValue());
        Metrics.metric(metricName).incrementCounter(secondValue);
        assertEquals(
                "Should increment the counter",
                firstValue + secondValue,
                registry.pullCounter(metricName).getValue());
    }

    @Test
    public void testMetric() {
        MetricTimer metric = Metrics.timer("test.metric");
        OpCounter opCounter = registry.pullOpCounter("test.metric");

        assertEquals(0L, opCounter.getInvocationCount());

        metric.start().close();
        assertEquals(1L, opCounter.getInvocationCount());

        metric.start().close();
        assertEquals(2L, opCounter.getInvocationCount());
    }

    @Test
    public void testTimerWithMetric() {
        Timer timer = Timers.timerWithMetric("test.metric");
        OpCounter opCounter = registry.pullOpCounter("test.metric");

        assertEquals(0L, opCounter.getInvocationCount());

        timer.start().close();
        assertEquals(1L, opCounter.getInvocationCount());
    }

    @Test
    public void testTimerWithMetricDifferentMetricName() {
        Timer timer = Timers.timerWithMetric("something something", "test.metric");
        OpCounter opCounter = registry.pullOpCounter("test.metric");

        assertEquals(0L, opCounter.getInvocationCount());

        timer.start().close();
        assertEquals(1L, opCounter.getInvocationCount());
    }

    @Test
    public void testUpdateGauge() {
        final MetricKey metricKey = MetricKey.metricKey("counter");
        final long zero = 0;
        final long firstValue = 50;
        final long secondValue = -20;
        final long expectedFinalValue = firstValue + secondValue;

        strategy.incrementGauge(metricKey, zero);
        assertEquals(
                "should start from 0",
                zero,
                registry.pullGauge(metricKey.getMetricName()).getValue());

        strategy.incrementGauge(metricKey, firstValue);
        assertEquals(
                "should return updated value",
                firstValue,
                registry.pullGauge(metricKey.getMetricName()).getValue());

        strategy.incrementGauge(metricKey, secondValue);
        assertEquals(
                "should update as a delta value",
                expectedFinalValue,
                registry.pullGauge(metricKey.getMetricName()).getValue());
    }

    @Test
    public void shouldCreateNewGauge() {
        final MetricKey metricKey = MetricKey.metricKey("metric");
        final long gaugeValue = 10L;
        strategy.setGauge(metricKey, gaugeValue);

        assertEquals(
                "should return set value",
                registry.pullGauge(metricKey.getMetricName()).getValue(),
                gaugeValue);
    }

    @Test
    public void shouldUpdateExistingGauge() {
        final MetricKey metricKey = MetricKey.metricKey("metric");
        final long gaugeValue = 10L;
        final long newGaugeValue = 10L;
        strategy.setGauge(metricKey, gaugeValue);
        strategy.setGauge(metricKey, newGaugeValue);

        assertEquals(
                "should return updated value",
                registry.pullGauge(metricKey.getMetricName()).getValue(),
                newGaugeValue);
    }

    @Test
    public void shouldOverwriteGaugeByMutableGauge() {
        final MetricKey metricKey = MetricKey.metricKey("metric");
        Gauge gauge = registry.pullGauge(metricKey.getMetricName());
        assertThat(gauge, not(instanceOf(MutableGauge.class)));

        strategy.setGauge(metricKey, 10L);

        Gauge overwrittenGauge = registry.pullGauge(metricKey.getMetricName());
        assertThat(overwrittenGauge, instanceOf(MutableGauge.class));
    }
}
