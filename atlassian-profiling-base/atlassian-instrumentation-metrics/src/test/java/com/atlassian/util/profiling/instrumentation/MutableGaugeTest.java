package com.atlassian.util.profiling.instrumentation;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MutableGaugeTest {
    private MutableGauge mutableGauge;

    @Before
    public void setup() {
        mutableGauge = new MutableGauge("gauge");
    }

    @Test
    public void shouldSetGaugeValue() {
        mutableGauge.setValue(1L);
        mutableGauge.setValue(2L);

        assertThat("should return set value of gauge", mutableGauge.getValue(), is(2L));
    }
}
