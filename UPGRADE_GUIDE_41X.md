If using Micrometer to emit metrics to JMX using `QualifiedCompatibleHierarchicalNameMapper` then the `ObjectName`s of
the MBeans will change, you may need to update any JMX documentation or dashboards for your product
