# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [5.0.1]
- Remove useless export declarations from `atlassian-profiling-adaptor-plugin`

## [5.0.0]
- [DCPL-614] Platform 7 migration
- Remove not used `ProfilingP6Logger` and `p6spy` dependency

## [4.9.0]
- [BIPD-694] Added MetricStrategy#resetMetric that allows remove specific metric based on MetricKey.

## [4.8.4]

- [BIPD-598] Improve performance of collecting all tags in metric builder when there are only required tags

## [4.8.3]

- Introduce additional layer of Maven modules, so `external-libraries-bom` has a parent, but not inherit its `dependencyManagement` section

## [4.8.2]

- Add libraries BOM for DC platform to consume

## [4.8.1]

### Fixed
- [BIPD-105]: Fixed Gauge metrics clearing issue.

## [4.8.0]

### Fixed
- [RAID-2927]: bumped spring to 5.3.19 to fix vulnerability.

### Added
- [ITAS-185] Add Gauge support to metrics

## [4.7.0]

### Added
- [ITASM2-338]: Add `max` attribute to LongRunningTimer analytic events.

## [4.6.1]

### Fixed
- [ITASM2-278]: Fix increment counter to check the metrics filter.

## [4.6.0]

### Added
- [ITASM3-150]: Add an external facing API for app vendors to add their own metric tags.

### Fixed
- The children in shadowing metric tag context no longer erase the exact same tags if they exist in the parent on close.
See `ut.com.atlassian.profiling.MetricContextTest#testShadowing_shouldNotClearParent` for an illustrated example.

## [4.5.0] - 2022-02-08

### Added
- [ITASM3-125]: Added the ability to enable optional tags based on `atlassian.metrics.optional.tags.{metricName}` 
system property. `metricName` is the name of the metric to enable the tags for, value is a comma-delimited list of
optional tags to be enabled

### Fixed
- [VIB-70]: bumped log4j to `1.2.17-atlassian-15` to fix vulnerability

## [4.4.0] - 2022-01-04

### Added
- [ITASM3-99]: Add metric builder tag and optionalTag methods that take `int` and `boolean` as values to complement the 
existing methods taking `String` values. This brings the metrics builder to feature parity with the `MetricTag` et al. 
methods.

## [4.3.1] - 2022-01-04

### Fixed
- [ITASM3-92]: Remove accidental deprecations

## [4.3.0] - 2021-12-31

### Changed
- [ITASM3-92]: Add deprecations for redundant methods
- chore: Simplified `@Nonnull` annotations in `Timers.java`

### Fixed
- [ITASM3-91]: Update deprecations that were initially slated for 4.0 to 5.0

## [4.2.1]

### Fixed
- [ITASM3-87] Fix implicit behaviour of Micrometer leaking into Profiling API by taking responsibility for it and making
it explicit. Specifically the ordering of tags in output, and the overriding of existing tags with the same key.
- chore: Make `Metrics.Builder#Builder` package-private instead of protected.

## [4.2.0] - 2021-12-22

### Fixed
- [ITASM3-90] Fix `MetricFilter` to the correct package

## [4.1.2] - 2021-12-22

### Fixed
- [ITASM3-89] Fix `QualifiedCompatibleHierarchicalNameMapper` to escape special characters in JMX object names

## [4.1.1] - 2021-12-20

### Fixed
- [ITASM3-85] Prepend all tag keys to prevent collisions with key properties used for the metric name
- chore: Reduce visibility of internal `TagComparator` utility.
- chore: Reduce visibility of internal `QualifiedCompatibleHierarchicalNameMapper` utility method.

## [4.1.0] - 2021-12-17

### Changed
- [ITAS-232] Simplify JMX naming for tags.
- [ITASM3-45] Introduce MetricsFilter
- [ITAS-196] decouple Micrometer analytics registry from atlassian-profiling

## [4.0.1] - 2021-11-17

## Fixed

- [ITAS-213] QualifiedCompatibleHierarchicalNameMapper puts the `type=metrics` part of the names at the start.

## [4.0.0] - 2021-11-17

## Added

- [ITAS-191] Add OptionalMetricTag to collect data that could be useful for a metric
- [ITAS-191] Add #collect() method to metrics builder to collect a metric tag if it exists on a context

## Changed

- [ITAS-191] MetricTagContext only takes OptionalMetricTag to prevent over-collection
- [ITAS-191] All existing method parameters of type MetricTag have been migrated to MetricTag.RequiredMetricTag

## [3.7.0]

## Added

- [ITAS-191] Added counter metric support

## Fixed

- [ITAS-190] Colon characters that appear in the metric name (or metric tags) causing an exception when emitted via JMX 
  by replacing them with an underscore in `QualifiedCompatibleHierarchicalNameMapper`

## [3.6.2]

## Fixed

- [ITAS-88] Excluded MetricTagContexts from metrics until we have a way of including each tag context explicitly.

## [3.6.1]

## Fixed

- [ITAS-152] Metric Tags can now be created with a `null` value.

## [3.6.0]

### Added

- [ITAS-122] Add Long-running metrics timers to the builder API
- [ITAS-150] Add intentful and descriptive methods to attribute plugin-keys to a metric in the builder API

## Fixed

- [ITAS-149] Add history to long-running timers
- [PROF-5] Starting a regular timer instead of a long-running timer

## [3.5.0]

### Added

- [ITAS-122] Long-running metrics timers
- [ITAS-119] Provide GroupingObjectNameFactory to be imported and used with dropwizard
- [ITAS-143] Provide QualifiedCompatibleHierarchicalNameMapper and UnescapedObjectNameFactory to be used with micrometer
- [ITAS-116] Introduce MetricTagContext
- [ITAS-117] Support for publishing Atlassian Analytics via Micrometer

[Unreleased]: https://bitbucket.org/atlassian/atlassian-profiling/compare/master%0Datlassian-profiling-parent-5.0.1

[5.0.1]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-5.0.1%0Datlassian-profiling-parent-5.0.0
[5.0.0]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-5.0.0%0Datlassian-profiling-parent-4.9.0
[4.9.0]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.9.0%0Datlassian-profiling-parent-4.8.4
[4.8.4]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.8.4%0Datlassian-profiling-parent-4.8.3
[4.8.3]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.8.3%0Datlassian-profiling-parent-4.8.2
[4.8.2]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.8.2%0Datlassian-profiling-parent-4.8.1
[4.8.1]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.8.1%0Datlassian-profiling-parent-4.8.0
[4.8.0]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.8.0%0Datlassian-profiling-parent-4.7.0
[4.7.0]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.7.0%0Datlassian-profiling-parent-4.6.1
[4.6.1]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.6.1%0Datlassian-profiling-parent-4.6.0
[4.6.0]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.6.0%0Datlassian-profiling-parent-4.5.0
[4.5.0]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.5.0%0Datlassian-profiling-parent-4.4.0
[4.4.0]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.4.0%0Datlassian-profiling-parent-4.3.1
[4.3.1]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.3.1%0Datlassian-profiling-parent-4.3.0
[4.3.0]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.3.0%0Datlassian-profiling-parent-4.2.1
[4.2.1]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.2.1%0Datlassian-profiling-parent-4.2.0
[4.2.0]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.2.0%0Datlassian-profiling-parent-4.1.2
[4.1.2]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.1.2%0Datlassian-profiling-parent-4.1.1
[4.1.1]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.1.1%0Datlassian-profiling-parent-4.1.0
[4.1.0]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.1.0%0Datlassian-profiling-parent-4.0.1
[4.0.1]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.0.1%0Datlassian-profiling-parent-4.0.0
[4.0.0]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-4.0.0%0Datlassian-profiling-parent-3.7.0
[3.7.0]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-3.7.0%0Datlassian-profiling-parent-3.6.2
[3.6.2]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-3.6.2%0Datlassian-profiling-parent-3.6.1
[3.6.1]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-3.6.1%0Datlassian-profiling-parent-3.6.0
[3.6.0]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-3.6.0%0Datlassian-profiling-parent-3.5.0
[3.5.0]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-3.5.0%0Datlassian-profiling-parent-3.4.3
[3.4.3]: https://bitbucket.org/atlassian/atlassian-profiling/compare/atlassian-profiling-parent-3.4.3%0Datlassian-profiling-parent-3.4.2

[BIPD-694]: https://bulldog.internal.atlassian.com/browse/BIPD-694
[BIPD-598]: https://bulldog.internal.atlassian.com/browse/BIPD-598
[ITAS-88]: https://bulldog.internal.atlassian.com/browse/ITAS-88
[ITAS-122]: https://bulldog.internal.atlassian.com/browse/ITAS-122
[ITAS-119]: https://bulldog.internal.atlassian.com/browse/ITAS-119
[ITAS-116]: https://bulldog.internal.atlassian.com/browse/ITAS-116
[ITAS-117]: https://bulldog.internal.atlassian.com/browse/ITAS-117
[ITAS-143]: https://bulldog.internal.atlassian.com/browse/ITAS-143
[ITAS-149]: https://bulldog.internal.atlassian.com/browse/ITAS-149
[ITAS-150]: https://bulldog.internal.atlassian.com/browse/ITAS-150
[ITAS-152]: https://bulldog.internal.atlassian.com/browse/ITAS-152
[ITAS-185]: https://bulldog.internal.atlassian.com/browse/ITAS-185
[ITAS-190]: https://bulldog.internal.atlassian.com/browse/ITAS-190
[ITAS-191]: https://bulldog.internal.atlassian.com/browse/ITAS-191
[ITAS-196]: https://bulldog.internal.atlassian.com/browse/ITAS-196
[ITAS-213]: https://bulldog.internal.atlassian.com/browse/ITAS-213
[ITAS-232]: https://bulldog.internal.atlassian.com/browse/ITAS-232
[ITASM3-45]: https://bulldog.internal.atlassian.com/browse/ITASM3-45
[ITASM3-85]: https://bulldog.internal.atlassian.com/browse/ITASM3-85
[ITASM3-87]: https://bulldog.internal.atlassian.com/browse/ITASM3-87
[ITASM3-89]: https://bulldog.internal.atlassian.com/browse/ITASM3-89
[ITASM3-90]: https://bulldog.internal.atlassian.com/browse/ITASM3-90
[ITASM3-91]: https://bulldog.internal.atlassian.com/browse/ITASM3-91
[ITASM3-92]: https://bulldog.internal.atlassian.com/browse/ITASM3-92
[ITASM3-99]: https://bulldog.internal.atlassian.com/browse/ITASM3-99
[ITASM3-125]: https://bulldog.internal.atlassian.com/browse/ITAS-125
[ITASM3-150]: https://bulldog.internal.atlassian.com/browse/ITASM3-150
[ITASM3-338]: https://bulldog.internal.atlassian.com/browse/ITASM3-338
[PROF-5]: https://product-fabric.atlassian.net/browse/PROF-5
[VIB-70]: https://bulldog.internal.atlassian.com/browse/VIB-70
