1. If there is a GroupingObjectNameFactory in your product, feel free to remove it and instead import a tested copy from
   the dropwizard metrics module if you're still using dropwizard. If you're using micrometer, use
   UnescapedObjectNameFactory and QualifiedCompatibleHierarchicalNameMapper instead.
2. If the Micrometer strategy or any other strategy exists that supports metrics tags, it's recommend to filter out the
   tags. In the case of micrometer this can be done by 
   `<strategy>.config().meterFilter(ignoreTags(SEND_ANALYTICS.getKey()));` on each registry that isn't the provided
   one for sending analytics: `AnalyticsMeterRegistry`
3. To send metrics to analytics the `MicrometerStrategy` metrics strategy will need to be used, `AnalyticsMeterRegistry`
   will need to be setup, and the [global analytics allowlist] will need to updated. Analytic events will be fired
   with the name `profiling.metric`.

[global analytics allowlist](https://bitbucket.org/atlassian/analytics-whitelist/)
