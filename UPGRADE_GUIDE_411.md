Ensure that the `QualifiedCompatibleHierarchicalNameMapper` is only used on the micrometer registry used for JMX. It
will now prefix all metric tag keys with `tag.` to prevent collisions. 
