1. If there are any references to `MetricTag` as a type, please replace them with `MetricTag.RequiredMetricTag`. Product
   and marketplace Apps were first checked, and there were no usages at time of change.
2. If there are any usages of `MetricTagContext`, then any tags that are added will need to be changed to
   `MetricTag.OptionalMetricTag`. Product and marketplace Apps were first checked, and there were no usages at time of
   change.
